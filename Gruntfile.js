module.exports = function(grunt) {
    grunt.initConfig({
        "bower-install-simple": {
            options: {
                color: true,
                directory: "bower_components"
            },
            "prod": {
                options: {
                    production: true
                }
            },
            "dev": {
                options: {
                    production: false
                }
            }
        },
        "concat": {
            phaser: {
                files: {
                    'src/js/lib/phaser/phaser.js': ['bower_components/phaser-official/build/phaser.js'],
                    'src/js/lib/phaser/phaser.map': ['bower_components/phaser-official/build/phaser.map']
                }
            },
            requirejs: {
                files: {
                    'src/js/lib/requirejs/require.js': ['bower_components/requirejs/require.js']
                }
            },
            underscore: {
                files: {
                    'src/js/lib/underscore/underscore.js': ['bower_components/underscore/underscore.js'],
                    'src/js/lib/underscore/underscore-min.map': ['bower_components/underscore/underscore-min.map']
                }
            },
            jquery: {
                files: {
                    'src/js/lib/jquery/jquery.js': ['bower_components/jquery/dist/jquery.js']
                }
            }
        },
        "clean": ["deploy"],
        "jshint": {
            files: ['Gruntfile.js', 'src/js/**/*.js', '!src/js/lib/**'],
            options: {
                globals: {
                    jQuery: false
                }
            }
        },
        "requirejs": {
            compile: {
                options: {
                    dir: "deploy",
                    appDir: "src",
                    optimize: "uglify",
                    fileExclusionRegExp: /^assets$/ // ignore assets directory, imagemin will handle this instead
                }
            }
        },
        "regex-replace": {
            config: {
                src: 'deploy/js/main.js',
                actions: [{
                    name: 'url',
                    search: 'url:\s*".*?",',
                    replace: 'url:"http://dev\.funatomic\.com/Anmuinteoir/multiple-choice/server/",',
                    flags: 'gi'
                }]
            }
        },
        "imagemin": {
            png: {
                options: {
                    optimizationLevel: 3
                },
                files: [
                        {
                        expand: true,
                        cwd: 'src/assets/',
                        src: ['**/*.png'],
                        dest: 'deploy/assets/',
                        ext: '.png'
                    }
                ]
            },
            jpg: {
                options: {
                    progressive: true
                },
                files: [
                    {
                        expand: true,
                        cwd: 'src/assets/',
                        src: ['**/*.jpg'],
                        dest: 'deploy/assets/',
                        ext: '.jpg'
                    }
                ]
            }
        },
        "copy": {
            spritesheets: {
                files: [
                    { expand: true, cwd: 'src/assets/', src: ['**/*.json'], dest: 'deploy/assets/' }
                ]
            },
            audio: {
                files: [
                    { expand: true, cwd: 'src/assets/', src: ['**/*.mp3', '**/*.ogg'], dest: 'deploy/assets/' }
                ]
            }
        }
    });

    grunt.loadNpmTasks("grunt-bower-install-simple");
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-regex-replace');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Bower integration
    grunt.registerTask('bower', ['bower-install-simple', 'concat']);

    grunt.registerTask('localdeploy', ['jshint', 'clean', 'bower', 'requirejs', 'copy', 'imagemin']);
    grunt.registerTask('deploy', ['jshint', 'clean', 'bower', 'requirejs', 'regex-replace', 'copy', 'imagemin']);
    grunt.registerTask('default', ['jshint']);
};