var SB = SB || {};
var _ = require("underscore");

SB.Server = function() {
    this.port = process.env.OPENSHIFT_NODEJS_PORT || 8000;
    this.ip = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
    this.app = require("express")();
    this.app.set("port", this.port);
    this.app.set("ipaddr", this.ip);
    this.http = require("http").Server(this.app);
    this.io = require("socket.io")(this.http);
}

SB.Server.prototype = {
    start: function() {
        this.http.listen(this.port, function(){
            console.log("listening on *:8000");
        });

        this.io.on("connection", function(socket){
            socket.on(SB.events.CLIENT_READY, function(tableNo){
                socket.join("table" + tableNo);
                var table = tables[tableNo];
                console.log("Player connected " + tableNo)

                if (table.playerCount >= 6) {
                    return;
                }

                // Finding seat for player
                for (var pos = 0; pos < 6; pos++) {
                    if (!table.players[pos]) {
                        break;
                    }
                }

                table.players[pos] = new SB.Player(false, pos, "Player" + (pos + 1), socket, table.buyin);
                table.playerCount++;
                table.playersWaiting++;

                var playerData = [];

                for (var i = 0; i < 6; i++) {
                    var player = table.players[i];
                    if (player) {
                        playerData.push({
                            name: player.name,
                            position: player.position,
                            money: player.money,
                            betAmount: player.betAmount,
                            interferenceCards: player.interferenceCards,
                            cards: player.cards.length
                        });
                    } else {
                        playerData.push(null);
                    }
                }

                socket.emit(SB.events.START_GAME, { position: pos, players: playerData, count: table.playerCount, dealer: table.dealer, ante: table.ante, blind: table.blind });

                player = table.players[pos];

                for (var i = 0; i < 6; i++) {
                    if (table.players[i] && table.players[i].socket !== socket) {
                        table.players[i].socket.emit(SB.events.PLAYER_CONNECTED, { position: pos, player: { name: player.name, position: player.position, money: player.money} });
                    }
                }

                if (table.playerCount === 2)
                    table.startGame();

            }.bind(this));

            socket.on(SB.events.RECIEVE_ACTION, function(data) {
                var tableNo;
                for (var j = 0; j < tables.length; j++) {
                    for (var i = 0; i < 6; i++) {
                        if (tables[j].players[i] && tables[j].players[i].socket == socket) {
                            tableNo = j;
                        }
                    }
                }

                console.log(tableNo);

                var table = tables[tableNo];
                table.executeAction(data);
            });

            socket.on("disconnect", function() {
                socket.leave("table" + tableNo);
                var tableNo, playerNo;
                // Get Player
                for (var j = 0; j < tables.length; j++) {
                    for (var i = 0; i < 6; i++) {
                        if (tables[j].players[i] && tables[j].players[i].socket == socket) {
                            tableNo = j;
                            playerNo = i;
                        }
                    }
                }

                var table = tables[tableNo];
                if (table) {
                    var player = table.players[playerNo];

                    if (player) {
                        if (player.position == table.playerTurn) {
                            table.executeAction({ name: "fold" });
                        }

                        table.mainPot += player.betAmount;

                        table.players[playerNo] = null;
                        table.playerCount--;

                        // only gets called if non player turn disconnects
                        if (table.foldedPlayers === (table.playerCount - table.playersWaiting) - 1) {
                            table.calculateWinner();
                        }

                        for (var j = 0; j < 6; j++) {
                            if (table.players[j] && table.players[j].socket !== socket) {
                                table.players[j].socket.emit(SB.events.PLAYER_DISCONNECTED, { position: playerNo });
                            }
                        }
                    }
                }
            });
        }.bind(this));
    },

    setEvents: function() {
        // Outgoing Events
    }
}

var server = new SB.Server();

server.start();

SB.Events = function() {

}

SB.Events.prototype = {
    CLIENT_READY: "clientReady",
    PLAYER_CONNECTED: "playerConnected",
    PLAYER_DISCONNECTED: "playerDisconnected",
    START_GAME: "startGame",
    START_ROUND: "startRound",
    TAKE_ANTE: "takeAnte",
    TAKE_BET: "takeBet",
    ROUND_DEAL: "roundDeal",
    TAKE_TURN: "takeTurn",
    TAKE_ACTION: "takeAction",
    RECIEVE_ACTION: "recieveAction",
    COLLECT_BETS: "collectBets",
    SHOW_WINNER: "showWinner",
    RESET_PLAYERS: "resetPlayers",
    GAME_WAITING: "gameWaiting",
    SABAAC_SHIFT: "sabaacShift"
}

SB.events = new SB.Events();

SB.Constants = {
    WAITING: 1,
    STARTED: 2,
};

SB.Table = function (buyin, room) {
    this.buyin = buyin;
    this.room = room
    this.blind = Math.ceil(buyin / 100);
    this.ante = Math.ceil(buyin / 400);
    this.players = new Array();
    this.currentDeck = null;
    this.gamePhase = SB.Constants.WAITING ;
    this.mainPot = 0 ;
    this.sabbacPot = 0 ;
    this.dealer = 0 ;
    this.playerTurn = null ;
    this.phasesCount = 1 ;
    this.maximumCurrentBet = 0 ;
    this.raises = 0 ;
    this.lastTurn = 0 ;
    this.alderaaned = false ;
    this.alderaanedPlayer = -1 ;
    this.foldedPlayers = 0 ;
    this.turnTimer = null ;
    this.shiftTimer = null ;
    this.playerCount = 0 ;
    this.playersWaiting = 0 ;
}

SB.Table.prototype = {
    startGame: function() {
        this.currentDeck = null;
        this.gamePhase = SB.Constants.WAITING;
        this.mainPot = 0;
        this.sabbacPot = 0;
        this.dealer = 0;
        this.playerTurn = null;
        this.phasesCount = 1;
        this.maximumCurrentBet = 0;
        this.raises = 0;
        this.lastTurn = 0;
        this.alderaaned = false;
        this.alderaanedPlayer = -1;
        this.foldedPlayers = 0;
        this.currentDeck = new SB.Deck();
        this.startRound();
    },

    startRound: function() {
        if (this.playerCount === 1) {
            clearTimeout(this.turnTimer);
            clearTimeout(this.shiftTimer);

            for (var j = 0; j < 6; j++) {
                if (this.players[j]) {
                    this.players[j].socket.emit(SB.events.GAME_WAITING);
                }
            }
            return;
        }

        console.log("Starting new round....");

        this.playersWaiting = 0;

        // Changing dealer in clockwise direction and setting player turn
        do {
            this.dealer ++;
            this.dealer %= 6;
        } while (!this.players[this.dealer]);

        for (var i = 0; i < 6; i++) {
            if (this.players[i]) {
                this.players[i].isWaiting = false;
            }
        }

        // Starting player would be the 3rd player from dealer since 1st and 2nd do blind bets
        this.playerTurn = this.dealer + 3;
        this.playerTurn %= 6;

        // Start timer for sabaac shift
        this.startSabaacShiftTimer();
        server.io.to(this.room).emit(SB.events.START_ROUND, this.dealer);
        this.turnTimer = setTimeout(this.takeAnte.bind(this), 100);
    },

    takeAnte: function () {
        console.log("Taking Ante....");

        this.gamePhase = "Ante";

        for (var i = 0; i < 6; i++) {
            if (this.players[i] && !this.players[i].isWaiting) {
                this.players [i].takeAnte(this.ante);
                this.sabbacPot += this.ante;
            }
        }

        server.io.to(this.room).emit(SB.events.TAKE_ANTE, this.ante);
        this.turnTimer = setTimeout(this.takeBlinds.bind(this), 1000);
    },


    takeBlinds: function () {
        // Take Blind Bets from 2 players next to Dealer and put them as counters on table

        console.log("Taking Blinds");

        for (var i = 1, j = 1; i <= 6 && j <= 2; i++) {
            if (this.players[(this.dealer + i) % 6] && !this.players[(this.dealer + i) % 6].isWaiting) {
                this.recieveBet((this.dealer + i) % 6, Math.ceil((j * this.blind) / 2));
                j++;
            }
        }

        this.maximumCurrentBet = this.blind;
        this.lastTurn = this.dealer + 2;
        this.lastTurn %= 6;

        this.dealCards();
    },

    dealCards: function () {
        console.log("Dealing Cards....");

        this.gamePhase = "Dealing";

        this.currentDeck.makeDeck();
        this.currentDeck.shuffle();

        var i = 0, j = 0;

        for (var i = 0; i < 2; i++) {
            for (var j = 0; j < 6; j++) {
                var playerPosition = (j + this.dealer + 1) % 6;
                if (this.players[playerPosition] && !this.players[playerPosition].isWaiting) {
                    this.players [playerPosition].getCard(this.currentDeck.dealCard());
                }
            }
        }

        for (var i = 0; i < 6; i++) {
            if (this.players[i])
                this.players[i].socket.emit(SB.events.ROUND_DEAL, [
                    { value: this.players[i].cards[0].value, suit: this.players[i].cards[0].suit, points: this.players[i].cards[0].points },
                    { value: this.players[i].cards[1].value, suit: this.players[i].cards[1].suit, points: this.players[i].cards[1].points }
                ]);
        }

        this.gamePhase = "Betting";

        this.turnTimer = setTimeout(this.takeTurn.bind(this), 500 * (this.playerCount + 1));
    },

    takeTurn: function() {
        console.log(this.playerTurn + " " + this.lastTurn);
        if (!this.players[this.playerTurn] || this.players[this.playerTurn].folded || this.players[this.playerTurn].isWaiting) {
            this.endTurn();
            return;
        }

        server.io.to(this.room).emit(SB.events.TAKE_TURN, { playerTurn: this.playerTurn, gamePhase: this.gamePhase, maximumCurrentBet: this.maximumCurrentBet, phasesCount: this.phasesCount, alderaaned: this.alderaaned });
        if (this.players[this.playerTurn].socket) {
            this.turnTimer = setTimeout(this.playerTimeOver.bind(this), 20000);
        } else {
            if (this.gamePhase === "Betting") {
                this.turnTimer = setTimeout(this.aiBetting.bind(this), 3000 + Math.random() * 5000);
            } else {
                this.turnTimer = setTimeout(this.aiTrading.bind(this), 3000 + Math.random() * 5000);
            }
        }
    },

    aiBetting: function () {
        var player = this.players [this.playerTurn];
        var points = player.totalPoints;

        if (this.phasesCount === 1) {
            switch (true) {
                case (points >= -17 && points <= 13):
                case (points > 23):
                case (points < -27):
                    switch (this.raises) {
                        case 0:
                            this.executeAction({ name: "call" });
                            break;
                        case 1:
                            if (Math.random() < 0.5) {
                                this.executeAction({ name: "call" });
                            } else {
                                this.executeAction({ name: "checkFold" });
                            }
                            break;
                        default :
                            this.executeAction({ name: "checkFold" });
                            break;
                    }
                    break;
                case (points >= 14 && points <= 19):
                case (points >= -27 && points <= 24):
                case (points >= -19 && points <= -18):
                    switch (this.raises) {
                        case 0:
                            if (Math.random() < 0.8) {
                                this.executeAction({ name: "call" });
                            } else {
                                this.executeAction({ name: "confirm", amount: 2 * this.maximumCurrentBet - player.betAmount });
                            }
                            break;
                        case 1:
                            if (Math.random() < 0.75) {
                                this.executeAction({ name: "call" });
                            } else {
                                this.executeAction({ name: "checkFold" });
                            }
                            break;
                        default :
                            this.executeAction({ name: "checkFold" });
                            break;
                    }
                    break;
                case (points >= 20 && points <= 22):
                case (points >= -22 && points <= -20):
                    switch (this.raises) {
                        case 0:
                            this.executeAction({ name: "confirm", amount: 3 * this.maximumCurrentBet - player.betAmount });
                            break;
                        default :
                            if (Math.random() < 0.9) {
                                this.executeAction({ name: "call" });
                            } else {
                                this.executeAction({ name: "confirm", amount: 2 * this.maximumCurrentBet - player.betAmount });
                            }
                            break;
                    }
                    break;
                case (points === 23 || points === -23):
                    switch (this.raises) {
                        case 0:
                        case 1:
                            this.executeAction({ name: "confirm", amount: 3 * this.maximumCurrentBet - player.betAmount });
                            break;
                        default :
                            this.executeAction({ name: "call" });
                    }
                    break;
            }
        } else {
            switch (true) {
                case (points > 23):
                case (points < -23):
                    this.executeAction({ name: "checkFold" });
                    break;
                case (points >= -17 && points <=17):
                    var random = Math.random();
                    switch (this.raises) {
                        case 0:
                            if (random < 0.5) {
                                this.executeAction({ name: "call" });
                            } else if (random > 0.7) {
                                this.executeAction({ name: "confirm", amount: Math.floor((.33 + (Math.random() *.17)) * this.mainPot) - player.betAmount });
                            } else {
                                this.executeAction({ name: "confirm", amount: Math.floor((.5 + (Math.random() *.16)) * this.mainPot) - player.betAmount });
                            }
                            break;
                        case 1:
                            if (this.maximumCurrentBet < 0.5 * this.mainPot) {
                                if (random < 0.7) {
                                    this.executeAction({ name: "call" });
                                } else {
                                    this.executeAction({ name: "checkFold" });
                                }
                            } else if (this.maximumCurrentBet > this.mainPot) {
                                this.executeAction({ name: "checkFold" });
                            } else {
                                if (random < 0.5) {
                                    this.executeAction({ name: "call" });
                                } else {
                                    this.executeAction({ name: "checkFold" });
                                }
                            }
                            break;
                        default :
                            this.executeAction({ name: "checkFold" });
                    }
                    break;
                case (points >= 18 && points <= 19):
                case (points >= -19 && points <= -18):
                    switch (this.raises) {
                        case 0:
                            this.executeAction({ name: "confirm", amount: Math.floor((.33 + (Math.random() *.17)) * this.mainPot) - player.betAmount });
                            break;
                        case 1:
                            if (this.maximumCurrentBet < 0.66 * this.mainPot) {
                                this.executeAction({ name: "call" });
                            } else {
                                this.executeAction({ name: "checkFold" });
                            }
                            break;
                        default :
                            this.executeAction({ name: "checkFold" });
                            break;
                    }
                    break;
                case (points >= 20 && points <= 22):
                case (points >= -22 && points <= -20):
                    switch (this.raises) {
                        case 0:
                            this.executeAction({ name: "confirm", amount: Math.floor((.5 + (Math.random() *.16)) * this.mainPot) - player.betAmount });
                            break;
                        default :
                            this.executeAction({ name: "call" });
                            break;
                    }
                    break;
                case (points === 23 || points === -23):
                    switch (this.raises) {
                        case 0:
                            this.executeAction({ name: "confirm", amount: this.mainPot - player.betAmount });
                            break;
                        case 1:
                            this.executeAction({ name: "confirm", amount: 3 * this.maximumCurrentBet - player.betAmount });
                            break;
                        case  2:
                            this.executeAction({ name: "confirm", amount: 2 * this.maximumCurrentBet - player.betAmount });
                            break;
                        default :
                            this.executeAction({ name: "call" });
                    }
                    break;
            }
        }
    },

    aiTrading: function () {
        var player = this.players [this.playerTurn];
        var points = player.totalPoints;
        var diffPoints = 0;
        if (points > 0) {
            diffPoints = points - 19;
        } else {
            diffPoints = points + 19;
        }

        var tradePosition = null;

        switch (true) {
            case (points > 23):
            case (points < -28):
            case (points >= -18 && points <= 15):
                tradePosition = 0;
                for (var i = 1; i < player.cards.length; i++) {
                    if (Math.abs(diffPoints - player.cards[tradePosition].points) > Math.abs(diffPoints - player.cards[i].points)) {
                        tradePosition = i;
                    }
                }
                break;
            case (points >= -28 && points <= -24):
                if (player.cards.length >= 5) {
                    tradePosition = 0;
                    for (var i = 1; i < player.cards.length; i++) {
                        if (Math.abs(diffPoints - player.cards[tradePosition].points) > Math.abs(diffPoints - player.cards[i].points)) {
                            tradePosition = i;
                        }
                    }
                }
                break;
            case (points >= 16 && points <= 18):
                if (player.cards.length >= 5) {
                    tradePosition = 0;
                    for (var i = 1; i < player.cards.length; i++) {
                        if (Math.abs(player.cards[tradePosition].points) > Math.abs(player.cards[i].points)) {
                            tradePosition = i;
                        }
                    }
                }
                break;
            case (points >= 19 && points <= 23):
            case (points >= -23 && points <= -19):
                if (Math.random() < 0.5) {
                    tradePosition = "aldreaaned";
                } else {
                    tradePosition = "stand";
                }
                break;
        }

        if (tradePosition == "aldreaaned") {
            if (!this.alderaaned && this.phasesCount > 1) {
                this.executeAction({ name: "alderaan" });
                console.log("AI Alderaaned");
            } else {
                this.executeAction({ name: "stand" });
                console.log("AI Stand");
            }
        } else if (tradePosition == "stand") {
            this.executeAction({ name: "stand" });
            console.log("AI Stand");
        } else if (tradePosition === null) {
            this.executeAction({ name: "draw" });
        } else {
            this.executeAction({ name: "trade", cardPosition: tradePosition });
        }
    },

    switchPhase: function() {
        if (this.alderaaned) {
            this.gamePhase = "Alderaan";
            this.calculateWinner();
            return;
        }
        if (this.gamePhase == "Betting") {
            this.gamePhase = "Trading";

            // Take all bets and put them in main pot
            for (var i = 0; i < 6; i++) {
                if (this.players[i]) {
                    this.mainPot += this.players[i].betAmount;
                    this.players[i].resetForPhase();
                }
            }
            server.io.to(this.room).emit(SB.events.COLLECT_BETS, { gamePhase: this.gamePhase, mainPot: this.mainPot });

            console.log("Start Trading Phase.....");
        } else {
            this.gamePhase = "Betting";
            this.phasesCount++;
            this.maximumCurrentBet = 0;
            console.log("Start Betting Phase.....");
        }

        this.playerTurn = this.dealer;
        this.playerTurn %= 6;
        this.lastTurn = this.dealer;
        this.lastTurn %= 6;
    },

    playerTimeOver: function() {
        if (this.gamePhase == "Betting") {
            if (this.players[this.playerTurn].betAmount == this.maximumCurrentBet) {
                this.executeAction({name: "check"});
            } else {
                this.executeAction({name: "fold"});
            }
        } else if(this.gamePhase == "Trading") {
            this.executeAction({name: "stand"});
        }
    },

    executeAction: function(action) {
        var table = tables[0];
        if (action.name !== "protect")
            clearTimeout(this.turnTimer);
        var obj = { playerTurn: this.playerTurn, action: action.name };
        var player = this.players[this.playerTurn];
        switch (action.name) {
            case "checkFold":
                this.playerTimeOver();
                return;
            case "fold" :
                player.folded = true;
                this.foldedPlayers++;
                server.io.to(this.room).emit(SB.events.TAKE_ACTION, obj);
                if (this.foldedPlayers === (this.playerCount - this.playersWaiting) - 1) {
                    this.calculateWinner();
                    return;
                }
                break;
            case "check":
            case "stand":
                server.io.to(this.room).emit(SB.events.TAKE_ACTION, obj);
                break;
            case "call" :
                var diff = this.maximumCurrentBet - player.betAmount;
                console.log(this.playerTurn);
                this.recieveBet(this.playerTurn, diff);
                server.io.to(this.room).emit(SB.events.TAKE_ACTION, obj);
                break;
            case  "confirm":
                var diff = action.amount - player.betAmount;
                this.maximumCurrentBet = action.amount;
                this.recieveBet(this.playerTurn, diff);
                this.lastTurn = this.playerTurn - 1;
                this.lastTurn += 6;
                this.lastTurn %= 6;
                this.raises++;
                server.io.to(this.room).emit(SB.events.TAKE_ACTION, obj);
                break;
            case "trade" :
                player.giveCard(action.cardPosition);
                obj.positionInHand = action.cardPosition;
            case "draw":
                var card = this.currentDeck.dealCard();
                player.getCard(card);
                for (var i = 0; i < 6; i++) {
                    if (this.players[i] && this.players[i].socket) {
                        if (i === this.playerTurn) {
                            obj.card = { value: card.value, suit: card.suit, points: card.points };
                        } else {
                            obj.card = { value: "0", suit: "0", points: 0 };
                        }
                        this.players[i].socket.emit(SB.events.TAKE_ACTION, obj);
                    }
                }
                break;
            case "protect":
                var card = player.cards[action.cardPosition];
                player.protectCard(action.cardPosition);
                obj.card = { value: card.value, suit: card.suit, points: card.points };
                obj.positionInHand = action.cardPosition;
                server.io.to(this.room).emit(SB.events.TAKE_ACTION, obj);
                return;
            case "alderaan":
                this.alderaaned = true;
                this.alderaanedPlayer = this.playerTurn;
                server.io.to(this.room).emit(SB.events.TAKE_ACTION, obj);
                break;
        }
        this.endTurn();
    },

    endTurn: function() {
        if (this.playerTurn === this.lastTurn) {
            this.switchPhase();
            if (this.alderaaned) {
                return;
            }
        }
        this.playerTurn++;
        this.playerTurn %= 6;
        this.takeTurn();
    },

    calculateWinner: function(afterDemise) {
        console.log("Calculating Winner.....");

        clearTimeout(this.shiftTimer);
        var winners = [];
        var bombouts = [];
        var bestValue = 0;

        // Take all bets and put them in main pot
        for (var i = 0; i < 6; i++) {
            if (this.players[i]) {
                this.mainPot += this.players[i].betAmount;
                this.players[i].resetForPhase();
            }
        }

        var foldWin = false;
        // In case win cause of every1 folding
        if (this.foldedPlayers === (this.playerCount - this.playersWaiting) - 1) {
            foldWin = true;
            for (var k = 0; k < 6; k++) {
                var player = this.players [k];
                if (player && !player.isWaiting && !player.folded) {
                    winners[0] = player;
                }
            }
        } else {
            for (var i = 0; i < 6; i++) {
                var playerPosition = (i + this.dealer + 1) % 6;
                var player = this.players [playerPosition];

                if (player && !player.isWaiting) {
                    // Checking Folded players
                    if (player.folded) {
                        continue;
                    }

                    // Checking Bombout
                    if (Math.abs(player.totalPoints) > 23 || player.totalPoints === 0) {
                        bombouts.push(player);
                        continue;
                    }

                    // Calculating Points in case of special hands
                    var specialPoints = this.checkForSpecialCards(player.cards);
                    if (specialPoints !== 0) { player.totalPoints = specialPoints }

                    // Checking if better score

                    if (Math.abs(bestValue) < Math.abs(player.totalPoints)) {
                        winners = [];
                        winners.push(player);
                        bestValue = player.totalPoints;

                        // Non Muck players
                        //this.playerPanels[playerPosition].faceUpCards();
                        continue;
                    }

                    // Checking if same score

                    if (Math.abs(bestValue) === Math.abs(player.totalPoints)) {
                        if (player.totalPoints < 0 && bestValue > 0) {
                            winners = [];
                            winners.push(player);
                            bestValue = player.totalPoints;

                            //    this.playerPanels[playerPosition].faceUpCards();
                        } else {
                            winners.push(player);
                            bestValue = player.totalPoints;

                            //  this.playerPanels[playerPosition].faceUpCards();
                        }
                    }
                }
            }
        }

        // Checking if sudden demise can happen

        if (winners.length > 1 && !afterDemise) {
            for (var j = 0; j < winners.length; j++) {
                if (winners[j].cards.length === 5) {
                    // Sudden Demise Cant Happen
                    break;
                }
            }
            if (j === winners.length) {
                this.suddenDemise(winners);
                return;
            }
        }

        // Check if alderaaned player didnt win
        for (var j = 0; j < winners.length; j++) {
            if (winners[j].position === this.alderaanedPlayer) {
                // Found Winner
                break;
            }
        }

        if (j === winners.length && this.alderaanedPlayer !== -1) {
            bombouts.push(this.players[this.alderaanedPlayer]);
        }

        for (var j = 0; j < bombouts.length; j++) {
            if (bombouts[j]) {
                bombouts[j].takeAnte(Math.ceil(this.mainPot / 10));
            }
            this.sabbacPot += Math.ceil(this.mainPot / 10);
        }

        var prizeMoney = this.mainPot;

        if (winners.length === 0) {
            this.sabbacPot += prizeMoney;
        } else {
            if (Math.abs(bestValue) >= 23) {
                prizeMoney += this.sabbacPot;
                this.sabbacPot = 0;
            }
            prizeMoney = Math.floor(prizeMoney / winners.length);

            for (var i = 0; i < winners.length; i++) {
                winners[i].getMoney(prizeMoney);
            }
        }

        this.mainPot = 0;

        var dataWinners = [], dataBombouts = [];

        for (var i = 0; i < winners.length; i++) {
            dataWinners.push({name: winners[i].name, position: winners[i].position});
        }

        for (var i = 0; i < bombouts.length; i++) {
            dataBombouts.push({position: bombouts[i].position});
        }

        server.io.to(this.room).emit(SB.events.SHOW_WINNER, {
            winners: dataWinners,
            bombouts: dataBombouts,
            bestValue: bestValue,
            prizeMoney: prizeMoney,
            sabbacPot: this.sabbacPot
        });

        setTimeout(this.resetPlayers.bind(this), 7000);
    },

    resetPlayers: function() {
        server.io.to(this.room).emit(SB.events.RESET_PLAYERS);
        for (var i = 0; i < 6; i++) {
            if (this.players[i])
                this.players[i].resetForRound();
        }

        this.playerTurn = null;
        this.phasesCount = 1;

        this.maximumCurrentBet = 0;
        this.raises = 0;
        this.lastTurn = 0;
        this.alderaaned = false;
        this.foldedPlayers = 0;
        this.alderaanedPlayer = -1;
        this.startRound();
    },

    checkForSpecialCards: function(cards) {
        var cardList = {};
        var idiot = 0, two = 0, three = 0, queen = 0, end = 0, master = 0, mistress = 0, evil = 0, commander = 0;
        if (cards.length > 3) {
            return 0;
        }
        for (var i = 0; i < cards.length; i++) {
            switch (cards[i].value) {
                case "I": idiot++; break;
                case "2": two++; break;
                case "3": three++; break;
                case "Q": queen++; break;
                case "E": end++; break;
                case "MA": master++; break;
                case "MI": mistress++; break;
                case "Ev": evil++; break;
                case "C": commander++; break;
                default : return 0;
            }
        }

        if (idiot === 1 && two === 1 && three === 1) {
            // Idiot's Array
            return 23.5;
        } else if (queen === 2 && cards.length === 2) {
            // Fairy Empress
            return 22.5;
        } else if (end === 1 && master === 1 && mistress === 1) {
            // Longing Hearts
            return 21.5;
        } else if (commander === 1 && evil === 1 && mistress === 1) {
            // Temptation
            return 20.5;
        } else {
            return 0;
        }
    },

    suddenDemise: function (players) {
        for (var i = 0; i < players.length; i++) {
            players[i].getCard(this.currentDeck.dealCard());
        }

        this.calculateWinner(true);
    },

    startSabaacShiftTimer: function() {
        if (this.shiftTimer) {
            clearTimeout(this.shiftTimer);
            this.shiftTimer = null;
        }

        this.shiftTimer = setTimeout(this.sabaacShift.bind(this), 30000 + Math.random() * 90000);
    },

    sabaacShift: function() {
        console.log("Sabaac Shift Happened...");
        this.startSabaacShiftTimer();

        var cardsToShift = Math.ceil(Math.random() * 4) * 2;
        var playerCardsToShift = Math.ceil(Math.random() * cardsToShift / 2);
        var playerCards = [], playerCardPositions = [];
        for (var i = 0; i < this.players.length; i++) {
            var player = this.players[i];
            if (!player || player.isWaiting || player.folded) {
                continue;
            }
            for (var j = 0; j < this.players[i].cards.length; j++) {
                playerCards.push(this.players[i].cards[j]);
                playerCardPositions.push({ player: i, position: j, list: "cards" });
            }
            for (var k = 0; k < this.players[i].interferenceCards.length; k++) {
                playerCards.push(this.players[i].interferenceCards[k]);
                playerCardPositions.push({ player: i, position: k, list: "interferenceCards" });
            }
        }

        var selectedCards = [], selectedPositions = [];

        for (var i = 0; i < playerCards.length; i++) {
            selectedPositions.push(i);
        }

        selectedPositions = _.sample(selectedPositions, playerCardsToShift);

        for (var i = 0; i < selectedPositions.length; i++) {
            var position = selectedPositions[i];
            var card1 = playerCards[position];
            if (card1.isInterferenced) {
                continue;
            }
            if (Math.random() < 0.5 && i < selectedPositions.length - 1) {
                var position2 = selectedPositions[i + 1];
                var card2 = playerCards[position2];
                this.players[playerCardPositions[position].player].tradeCard(card2, playerCardPositions[position].position);
                this.players[playerCardPositions[position2].player].tradeCard(card1, playerCardPositions[position2].position);
                i++;
            } else {
                var card2 = this.currentDeck.dealCard();
                this.players[playerCardPositions[position].player].tradeCard(card2, playerCardPositions[position].position);
            }
        }

        for (var i = 0; i < 6; i++) {
            if (this.players[i])
                this.players[i].socket.emit(SB.events.SABAAC_SHIFT, this.players[i].cards);
        }
    },

    recieveBet: function (playerPosition, amount) {
        this.players[playerPosition].putBet(amount);
        this.maximumCurrentBet = this.players[playerPosition].betAmount;
        server.io.to(this.room).emit(SB.events.TAKE_BET, { position: playerPosition, amount: amount, maxBet: this.maximumCurrentBet });
    },
}

var tables = [];
tables.push(new SB.Table(200, "table0"));
tables.push(new SB.Table(300, "table1"));
tables.push(new SB.Table(600, "table2"));
tables.push(new SB.Table(1000, "table3"));
tables.push(new SB.Table(2000, "table4"));
tables.push(new SB.Table(5000, "table5"));

SB.Player = function (isAI, position, name, socket, money) {
    this.isWaiting = true;
    this.name = name;
    this.position = position;
    this.isAI = isAI || false;
    this.cards = new Array ();
    this.interferenceCards = [];
    this.totalPoints = 0;
    this.betAmount = 0;
    this.money = money;
    this.folded = false;
    this.cardSelectWaitingFor = "";
    this.socket = socket;
};

SB.Player.prototype = {
    takeAnte: function (amount) {
        this.money = this.money - amount;
    },

    putBet: function (amount) {
        this.money -= amount;
        this.betAmount += amount;
    },

    getCard: function(card) {
        this.cards.push(card);
        this.totalPoints += card.points;
    },

    tradeCard: function(newCard, oldCardPosition) {
        this.totalPoints -= this.cards[oldCardPosition].points;
        this.cards[oldCardPosition] = newCard;
        this.totalPoints += newCard.points;
    },

    giveCard: function(oldCardPosition) {
        this.totalPoints -= this.cards[oldCardPosition].points;
        this.cards.splice(oldCardPosition, 1);
    },

    resetForPhase: function() {
        this.betAmount = 0;
    },

    resetForRound: function() {
        this.totalPoints = 0;
        this.betAmount = 0;
        this.folded = false;
        this.cards = [];
        this.interferenceCards = [];
        this.cardSelectWaitingFor = "";
    },

    getMoney: function (amount) {
        this.money += amount;
    },

    protectCard: function (oldCardPosition) {
        var card = this.cards.splice(oldCardPosition, 1)[0];
        card.isInterferenced = true;
        this.interferenceCards.push(card);
    }
}

/**
 * Deck Object containing 76 cards deck.
 * Card Type Reference:
 *          Suits: St (Staves), Fl (Flasks), Co (Coins), Sb (Sabres)
 *          Suit Card Values : 1-11, C (Commander), MI (Mistress), MA (Master), A (Ace)
 *          Ranked Cards: I (Idiot), Q (Queen of Air and Darkness), E (Endurance), B (Balance), D (Demise), M (Moderation), Ev (The Evil One), S (The Star)
 */

SB.Deck = function () {
    this.cards = new Array ();
};

SB.Deck.prototype = {
    makeDeck: function () {
        var values = new Array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "C", "MI", "MA", "A");
        var points = new Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        var suits = new Array("Sb", "St", "Fl", "Co");
        var rankCards = new Array("I", "Q", "E", "B", "D", "M", "Ev", "S");
        var rankValues = new Array(0, -2, -8, -11, -13, -14, -15, -17);
        var i, j;
        var n;

        n = values.length * suits.length;

        // Set array of cards.

        this.cards = new Array();

        // Fill the array with 'n' packs of cards.

        // Fill in suit cards
        for (i = 0; i < suits.length; i++) {
            for (j = 0; j < values.length; j++) {
                var card = new SB.Card(values[j], suits[i], points[j]);
                this.cards.push(card);
            }
        }

        // Fill in ranked cards
        for (i = 0; i < rankCards.length; i++) {
            for (j = 0; j < 2; j++) {
                var card = new SB.Card(rankCards[i], "Rk", rankValues[i]);
                this.cards.push(card);
            }
        }
    },

    shuffle: function () {
        var i, j, k;
        var temp;

        // Shuffle the stack '3' times.

        for (i = 0; i < 3; i++) {
            for (j = 0; j < this.cards.length; j++) {
                k = Math.floor(Math.random() * this.cards.length);
                temp = this.cards[j];
                this.cards[j] = this.cards[k];
                this.cards[k] = temp;
            }
        }
    },

    dealCard: function () {
        return this.cards.shift();
    }
}


/**
 * Card Object to represent each card in the playing deck
 */

SB.Card = function (value, suit, points) {
    this.name = "";
    this.value = value;
    this.suit = suit;
    this.points = points;
    this.isInterferenced = false;
}
