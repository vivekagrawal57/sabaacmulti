define([
    'phaser',
    'underscore',
    'js/objects/DealerToken.js',
    'js/objects/FeedbackObject.js',
    'js/objects/PlayerPanel.js',
    'js/objects/ActionPanel.js',
    'js/objects/BettingPanel.js',
    'js/objects/WinnerMessage.js',

], function (
    Phaser,
    UnderScore,
    DealerToken,
    PlayerPanel,
    BettingPanel,
    ActionPanel,
    WinnerMessage
) {
    'use strict';

    SB.Tutorial = function() {};

    SB.Tutorial.prototype = {
        currentDeck: null,
        players: [],
        playerPanels: [],
        gamePhase: SB.Constants.ANTE,
        mainPot: 0,
        sabbacPot: 0,
        callAll: false,
        checkFold: false,
        dealer: 2,
        playerTurn: null,
        phasesCount: 1,
        folderPlayers: 0,
        maximumCurrentBet: 0,
        raises: 0,
        lastTurn: 0,
        alderaaned: false,
        playerCount: 6,
        physicalDeck: [],
        alderaanedPlayer: -1,
        foldedPlayers: 0,
        turnTimer: null,
        shiftTimer: null,
        aiAlderaan: -2,
        allinedPlayers: 0,
        currentStep: 1,

        preload: function() {

        },

        create: function() {
            this.NO_OF_PLAYERS = 6;
            this.setup();

            this.createEnvironment();

            this.tutorialMsg = SB.game.add.group();
            var bg = SB.game.add.sprite(0, 0, "sprites2", "tutorial-bg");
            bg.anchor.set(0.5, 0.5);

            this.tutorialMsg.position.set(1470, 230);
            this.tutorialText = SB.game.add.text(0, -50, " Greetings, I am C-8D0, player-cyborg relations. Please allow me to give you an overview of the rules of Sabacc.");
            this.tutorialText.fill = "white";
            this.tutorialText.wordWrap = true;
            this.tutorialText.wordWrapWidth = 800;
            this.tutorialText.fontSize = 32;
            this.tutorialText.anchor.set(0.5, 0.5);
            this.tutorialText.align = "center";

            this.nextBtn = SB.game.add.button(0, 120, "sprites2", this.nextStep, this, "next-normal", "next-normal", "next-down");
            this.nextBtn.anchor.set(0.5, 0.5);

            this.tutorialMsg.add(bg);
            this.tutorialMsg.add(this.tutorialText);
            this.tutorialMsg.add(this.nextBtn);

            document.addEventListener("backbutton", this.returnToMainMenu.bind(this), false);

            // Creat a Card Deck and Shuffle it

            this.currentDeck = new SB.DeckTutorial();

            this.chart = SB.game.add.sprite(30, 35, "sprites2", "tutorial-cards");

            this.chart3 = SB.game.add.group();

            var suits = ["Sb", "St", "Fl", "Co"];
            var values = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "C", "MI", "MA", "A"];

            for (var i = 0; i < 4; i++) {
                for (var j = 0; j < 11; j++) {
                    var sprite = SB.game.add.sprite(j * 82, i * 130, "sprites", suits[i] + "/" + values[j]);
                    sprite.scale.set(0.75, 0.75);
                    this.chart3.add(sprite);
                }
            }

            this.chart3.position.set(65, 68);
            this.chart3.visible = false;

            this.chart4 = SB.game.add.group();

            for (var i = 0; i < 4; i++) {
                for (var j = 11; j < 15; j++) {
                    var sprite = SB.game.add.sprite((j - 7.5) * 82, i * 130, "sprites", suits[i] + "/" + values[j]);
                    sprite.scale.set(0.75, 0.75);
                    this.chart4.add(sprite);
                }
            }

            this.chart4.position.set(65, 68);
            this.chart4.visible = false;

            this.chart5 = SB.game.add.group();

            var rankCards = ["I", "Q", "E", "B", "D", "M", "Ev", "S"];

            for (var i = 0; i < 2; i++) {
                for (var j = 0; j < 4; j++) {
                    var sprite = SB.game.add.sprite((j + 1.5) * 110, i * 170, "sprites", "Rk/" + rankCards[j + 4 * i]);
                    this.chart5.add(sprite);
                }
            }

            this.chart5.position.set(130, 150);
            this.chart5.visible = false;

            this.chart6 = SB.game.add.group();

            var specialHands = [
                {"cards": [{s: "Rk", v: "I"}, {s: "St", v: "2"}, {s: "Fl", v: "3"}], "text": "Idiot's Array (-23)"},
                {"cards": [{s: "Rk", v: "Q"}, {s: "Rk", v: "Q"}], "text": "Fairy Empress (-22)"},
                {"cards": [{s: "Rk", v: "E"}, {s: "Co", v: "MI"}, {s: "Sb", v: "MA"}], "text": "Longing Hearts (-21)"},
                {"cards": [{s: "Rk", v: "Ev"}, {s: "Sb", v: "C"}, {s: "Fl", v: "MI"}], "text": "Temptation (-20)"}
            ];

            for (var i = 0; i < 4; i++) {
                for (var j = 0; j < specialHands[i].cards.length; j++) {
                    var sprite = SB.game.add.sprite(((i % 2) * 400) + (j * 110) + ((3 - specialHands[i].cards.length) * 50),
                        Math.floor(i / 2) * 245, "sprites", specialHands[i].cards[j].s + "/" + specialHands[i].cards[j].v);
                    this.chart6.add(sprite);
                }
                var handName = SB.game.add.text(160 + ((i % 2) * 400), 190 + Math.floor(i / 2) * 245, specialHands[i].text);
                handName.fill = "white";
                handName.anchor.set(0.5, 0.5);
                handName.align = "center";
                this.chart6.add(handName);
            }

            this.chart6.position.set(150, 100);
            this.chart6.visible = false;
            this.chart.visible = false;
        },

        nextStep: function() {
            this.currentStep++;
            switch (this.currentStep) {
                case 2:
                    this.tutorialText.text = "The goal in Sabacc is to win the chips in the Hand and Sabacc pots, respectively, by acquiring a hand with a value not exceeding positive or negative 23. The Sabacc pot can only be won with a hand value of exactly positive or negative 23.";
                    break;
                case 3:
                    this.chart.visible = true;
                    this.chart3.visible = true;
                    this.tutorialText.text = "There are four suits in a Sabacc deck: Staves, Flasks, Coins and Sabres. Each suit has regular cards numbered 1 through 11. Each of these card's value corresponds with its number.";
                    break;
                case 4:
                    this.chart3.visible = false;
                    this.chart4.visible = true;
                    this.tutorialText.text = "Additionally, each suit has four 'Face' cards: the Commander (value 12), the Mistress (13), the Master (14) and the Ace (15).";
                    break;
                case 5:
                    this.chart4.visible = false;
                    this.chart5.visible = true;
                    this.tutorialText.text = "The Sabacc deck also contains 8 pairs of cards with a neutral or negative value, which you can see on the left hand side of the screen. Thus, it is possible to have a hand of negative value in Sabacc, and these trump positive hands of the same or lesser value.";
                    break;
                case 6:
                    this.chart5.visible = false;
                    this.chart6.visible = true;
                    this.tutorialText.text = "Lastly, there are 4 'special' hands in Sabacc, composed of a special combination of cards. The hands trump all other hands of equal or lesser value. Now, let's get to the action!";
                    break;
                case 7:
                    this.chart6.visible = false;
                    this.chart.visible = false;
                    this.tutorialText.text = "You will be seated at the bottom of your screen. Up to 5 additional players are possible at each table.";
                    this.addPlayer(0, "Player");
                    this.addPlayer(1, "Luke");
                    this.addPlayer(5, "Ben");
                    this.addPlayer(2, "Rey");
                    this.startRound();
                    break;
                case 8:
                    this.nextBtn.visible = false;
                    this.tutorialText.text = "Each hand, all the players wishing to participate in the hand must pay an ante into the Sabacc pot. Additionally, the 2 players sitting directly clockwise from the dealer pay small and big blinds to begin the hand.";
                    this.takeAnte();
                    break;
                case 9:
                    this.nextBtn.visible = false;
                    this.tutorialText.text = "Each player is dealt two cards. We have received a 3 and a 4 for a total of 7, which is indicated in the 'Hand' bar in our Avatar. Ben has called. Our hand gives us options, so let's call also.";
                    this.dealCards();
                    break;
                case 10:
                    this.nextBtn.visible = false;
                    this.tutorialMsg.visible = false;
                    this.executeAction({name: "call"});
                    this.takeTurn();
                    setTimeout(function() {
                        this.executeAction({name: "call"});
                        this.takeTurn();
                    }.bind(this), 1500);
                    setTimeout(function() {
                        this.executeAction({name: "call"});
                        this.tutorialMsg.visible = true;
                        this.tutorialText.text = "We are now in the trading round. Here we have the option to add a card, trade one of our existing cards for a new card, or stand. Our hand value is quite low at the moment, so let's add a card.";
                        this.takeTurn(true);
                        this.nextBtn.visible = true;
                    }.bind(this), 3000);
                    break;
                case 11:
                    this.nextBtn.visible = false;
                    this.tutorialMsg.visible = false;
                    this.executeAction({name: "draw"});
                    this.takeTurn();
                    setTimeout(function() {
                        this.players[2].giveCard(0);
                        this.playerPanels[2].updateCards();
                        this.players[2].getCard(this.currentDeck.dealCard());
                        this.tradeCardAnimation(function() {}, this.playerTurn);
                        this.endTurn();
                        if (this.turnTimer)
                            this.turnTimer.stop();

                        if (this.tween) {
                            this.tween.stop();
                            SB.game.tweens.remove(this.tween);
                            this.tween = null;
                            this.playerPanels[2].timer.drawTimer(0);
                        }
                        this.takeTurn();
                        this.takeTurn();
                        this.takeTurn();
                    }.bind(this), 1500);
                    setTimeout(function() {
                        this.executeAction({name: "draw"});
                        this.takeTurn(true, true);
                    }.bind(this), 3000);
                    setTimeout(function() {
                        this.executeAction({name: "draw"});
                        this.tutorialMsg.visible = true;
                        this.tutorialText.text = "We have received a Mistress, giving us hand total of 20. This is a solid early hand! Now, on to a new betting hand.";
                        this.takeTurn(true);
                        this.nextBtn.visible = true;
                    }.bind(this), 4500);
                    break;
                case 12:
                    this.players[1].protectCard(0);
                    this.playerPanels[1].updateCards();
                    this.tutorialText.text = "It looks as though Luke has dropped one of his cards into the interference field! This protects the card from being affected by the random shifting of cards known as a Sabacc Shift. However, the card must be shown face up for all to see.";

                    SB.SFX.interference.play();
                    break;
                case 13:
                    this.nextBtn.visible = false;
                    this.tutorialMsg.visible = false;
                    this.makeBet(this.players[1], this.playerPanels[1], 100);
                    this.endTurn();
                    this.takeTurn();
                    setTimeout(function() {
                        this.executeAction({name: "fold"});
                        this.takeTurn();
                        this.takeTurn();
                        this.takeTurn();
                    }.bind(this), 1500);

                    setTimeout(function() {
                        this.executeAction({name: "call"});
                        this.takeTurn(true, true);
                        this.tutorialMsg.visible = true;
                        this.tutorialText.text = "Luke has bet 100, it looks like he has a good hand. Rey has folded to Luke's bet, but Ben has called. We have a good hand, so let's call also.";
                        this.nextBtn.visible = true;
                    }.bind(this), 3000);
                    break;
                case 14:
                    this.nextBtn.visible = false;
                    this.tutorialMsg.visible = false;
                    this.executeAction({name: "call"});
                    this.takeTurn(true);
                    setTimeout(function() {
                        this.sabaacShift();
                    }.bind(this), 1500);
                    setTimeout(function() {
                        this.tutorialText.text = "What luck! A Sabacc Shift has taken place, turning our Mistress into an Ace and giving us a hand of 22! This is a great hand, so when the action comes to us, we should consider an Alderaan, that is, calling the hand.";
                        this.tutorialMsg.visible = true;
                        this.nextBtn.visible = true;
                    }.bind(this), 3500);
                    break;
                case 15:
                    this.nextBtn.visible = false;
                    this.tutorialMsg.visible = false;
                    this.takeTurn();
                    setTimeout(function() {
                        this.executeAction({name: "stand"});
                        this.takeTurn();
                        this.takeTurn();
                        this.takeTurn();
                        this.takeTurn();
                    }.bind(this), 1500);
                    setTimeout(function() {
                        this.executeAction({name: "stand"});
                        this.takeTurn(false, true);
                    }.bind(this), 3000);
                    setTimeout(function() {
                        this.executeAction({name: "alderaan"});
                        this.tutorialText.text = "We have Alderaaned, and it looks like we are tied with Luke while Ben has Bombed Out! A bombout occurs when a player's hand is greater than positive or negative 23, and requires payment of a penalty equal to 10% of the Hand pot into the Sabacc pot. It looks like we will have to go to a Sudden Demise to determine the winner.";
                        this.tutorialText.fontSize = 28;
                        this.tutorialMsg.visible = true;
                        this.nextBtn.visible = true;
                        this.suddenDemiseMsg.visible = true;
                        this.diamond.visible = false;
                        this.playerPanels[1].faceUpCards();
                        this.playerPanels[5].faceUpCards();
                        this.playerPanels[5].playAnimation("bomb-out");
                        this.playerPanels [5].bomboutPay(Math.min(this.players [5].money, Math.ceil(this.mainPot / 10)));
                        this.players [5].takeAnte(Math.min(this.players [5].money, Math.ceil(this.mainPot / 10)));
                        this.playerPanels [5].updateMoney();
                        this.sabbacPot += Math.min(this.players [5].money, Math.ceil(this.mainPot / 10));
                        this.updateSabbacPot();
                    }.bind(this), 4500);
                    break;
                case 16:
                    this.tutorialText.text = "A Sudden Demise requires the dealer to deal each tied player an additional card from the deck, which is then added to each player's original hand. The best modified hand wins the pot.";
                    this.tutorialText.fontSize = 32;
                    break;
                case 17:
                    this.nextBtn.visible = false;
                    this.tutorialMsg.visible = false;
                    this.players[0].getCard(this.currentDeck.dealCard());
                    this.animateCard(function() {}, 0);
                    setTimeout(function() {
                        this.players[1].getCard(this.currentDeck.dealCard());
                        this.animateCard(function() {}, 1);
                        this.playerPanels[1].faceUpCards();
                    }.bind(this), 1500);
                    setTimeout(function() {
                        this.tutorialText.text = "Luke has received an Endurance card, lowering his total to 14, while we received a 1, giving us 23. We win the hand, and what's more, with a total of 23 we have Sabacc! This allows us to claim both the Hand and Sabacc pots!";
                        this.tutorialMsg.visible = true;
                        this.nextBtn.visible = true;
                    }.bind(this), 2000);
                    break;
                case 18:
                    this.nextBtn.visible = false;
                    this.tutorialMsg.visible = false;
                    this.suddenDemiseMsg.visible = false;
                    this.sabaacMsg.visible = true;
                    SB.SFX.sabaac.play();
                    this.playerPanels[0].mainToPlayer(this.mainPot);
                    this.playerPanels[0].showWinnerStatus();
                    this.playerPanels[0].sabaacToPlayer(this.sabbacPot);
                    this.players[0].money += this.mainPot + this.sabbacPot;
                    this.playerPanels[0].updateMoney();
                    this.sabbacPot = 0;
                    this.mainPot = 0;
                    this.updateMainPot();
                    this.updateSabbacPot();
                    setTimeout(function() {
                        this.tutorialText.text = "You have now received an overview of how to play Sabacc. Best of luck at the tables.";
                        this.tutorialMsg.visible = true;
                        this.nextBtn.visible = true;
                    }.bind(this), 1000);
                    break;
                case 19:
                    this.returnToMainMenu();
                    break;
            }
        },

        setup: function() {
            this.currentDeck = null;
            this.players = [];
            this.playerPanels = [];
            this.gamePhase = SB.Constants.ANTE;
            this.mainPot = 0;
            this.sabbacPot = 0;
            this.callAll = false;
            this.checkFold = false;
            this.dealer = 2;
            this.playerTurn = null;
            this.phasesCount = 1;
            this.folderPlayers = 0;
            this.maximumCurrentBet = 0;
            this.raises = 0;
            this.lastTurn = 0;
            this.alderaaned = false;
            this.playerCount = 6;
            this.physicalDeck = [];
            this.alderaanedPlayer = -1;
            this.foldedPlayers = 0;
            this.turnTimer = null;
            this.shiftTimer = null;
            this.allinedPlayers = 0;
            this.subPots = [];
            SB.Globals.buyin = 10000;
            SB.Globals.blind = 100;
            SB.Globals.ante = 25;
            this.currentStep = 1;
        },

        addPlayer: function(position, name) {
            var i = position;
            this.players[position] = new SB.Player(position, name, 10000);
            var player = this.players[i];
            var panelPosition = i;
            this.playerPanels [panelPosition].assignPlayer(this.players [i]);
            this.playerPanels [panelPosition].updateMoney();
        },

        /*
         Game Logic Code starts here
         */

        startRound: function () {
            console.log("Starting new round....");

            this.dealer = 0;
            this.dealerToken.shift(this.dealer);

            // Starting player would be the 3rd player from dealer since 1st and 2nd do blind bets
            this.playerTurn = this.dealer + 3;
            this.playerTurn %= 6;
        },

        takeAnte: function () {
            console.log("Taking Ante....");

            this.gamePhase = "Ante";

            for (var i = 0; i < 6; i++) {
                if (this.players[i]) {
                    this.players [i].takeAnte(SB.Globals.ante);
                    this.playerPanels [i].updateMoney();
                    this.playerPanels [i].chipToSabacc(SB.Globals.ante);
                    this.sabbacPot += SB.Globals.ante;
                }
            }

            this.updateSabbacPot();
            setTimeout(this.takeBlinds.bind(this), 1000);
        },

        takeBlinds: function () {
            // Take Blind Bets from 2 players next to Dealer and put them as counters on table

            for (var i = 1, j = 1; i <= 6 && j <= 2; i++) {
                if (this.players[(this.dealer + i) % 6]) {
                    this.makeBet(this.players [(this.dealer + i) % 6], this.playerPanels [(this.dealer + i) % 6], Math.ceil((j * SB.Globals.blind) / 2));
                    j++;
                }
            }

            this.maximumCurrentBet = SB.Globals.blind;
            this.lastTurn = this.dealer + 2;
            this.lastTurn %= 6;

            this.nextBtn.visible = true;
        },

        dealCards: function () {
            console.log("Dealing Cards....");

            this.gamePhase = "Dealing";

            this.currentDeck.makeDeck();
            SB.SFX.shuffle.play();
            this.createPhysicalDeck();

            var i = 0, j = 0, k = 0;

            var callback = function() {
                if (j < 6) {
                    var playerPosition = (j + this.dealer + 1) % 6;

                    j++;
                    if (this.players[playerPosition]) {
                        this.players [playerPosition].getCard(this.currentDeck.dealCard());
                        this.animateCard(callback, playerPosition);
                    } else {
                        callback();
                    }
                } else {
                    i++;
                    if (i < 2) {
                        j = 0;
                        callback();
                    } else {
                        this.startBettingPhase();
                        this.takeTurn();
                        this.takeTurn();
                        this.executeAction({name: "call"});
                        this.takeTurn(true, true);
                        this.nextBtn.visible = true;
                    }
                }

            }.bind(this);

            callback();
        },

        startBettingPhase: function () {
            console.log("Starting Betting Phase....")

            this.gamePhase = "Betting";

            this.actionPanel.hidePanel();
            this.callAll = false;
            this.checkFold = false;

            this.takeTurn();
        },

        takeTurn: function (dontShowTimer, makeSound) {
            console.log(this.playerTurn + " " + this.lastTurn);
            if (!this.players[this.playerTurn] || this.players[this.playerTurn].folded) {
                this.endTurn();
                return;
            }
            if (!dontShowTimer)
                this.startTurnTime(function() { }, 20000);
            if (makeSound) {
                SB.SFX.turn.play();
            }
        },

        endTurn: function() {
            if (this.playerTurn === this.lastTurn) {
                this.switchPhase();
                if (this.alderaaned) {
                    return;
                }
            }
            this.playerTurn++;
            this.playerTurn %= 6;
            SB.SFX.timer.stop();
            console.log("hrtr");
        },

        switchPhase: function () {
            if (this.alderaaned) {
                this.gamePhase = "Alderaan";
                return;
            }
            if (this.gamePhase == "Betting") {
                this.gamePhase = "Trading";
                this.actionPanel.hidePanel();

                var bets = [].concat(this.players);
                bets.sort(function(a, b) {
                    if (!a) {
                        return -1;
                    }
                    if (!b) {
                        return 1;
                    }
                    return a.betAmount - b.betAmount;
                });

                var betAmounts = [];
                for (var i = 0; i < 6; i++) {
                    if (bets[i]) {
                        betAmounts.push(bets[i].betAmount);
                    } else {
                        betAmounts.push(0);
                    }
                }

                console.log(betAmounts);

                var isFirstPot = true;

                for (var i = 0; i < 6; i++) {
                    if (bets[i]) {
                        var totalAmount = 0;
                        var winners = [];

                        if (bets[i].folded || betAmounts[i] === 0)
                            continue;

                        var amountToTake = betAmounts[i];

                        for (var j = 0; j < 6; j++) {
                            var amount = Math.min(betAmounts[j], amountToTake);
                            totalAmount += amount;
                            betAmounts[j] -= amount;
                            if (bets[j] && !bets[j].folded && amount !== 0) {
                                winners.push(bets[j].position);
                            }
                        }

                        if (isFirstPot && this.subPots.length > 0 && this.subPots[this.subPots.length - 1].players.length === (winners.length + this.foldedPlayers)) {
                            this.subPots[this.subPots.length - 1].amount += totalAmount;
                        } else {
                            this.subPots.push({amount: totalAmount, players: winners});
                        }

                        isFirstPot = false;

                        console.log("Pot: " + totalAmount + " Winners: " + winners);
                    }
                }

                // Take all bets and put them in main pot
                for (var i = 0; i < 6; i++) {
                    if (this.players[i]) {
                        this.mainPot += this.players[i].betAmount;
                    }
                }
                this.updateMainPot();

                for (var i = 0; i < 6; i++) {
                    if (this.players[i]) {
                        this.players[i].resetForPhase();
                        this.playerPanels[i].showBet();
                    }
                }

                if (this.players[0].folded) {
                    // If player folded increment counter to wait for 2nd trading and then assign the player to alderaan
                    this.aiAlderaan++;
                    if (this.aiAlderaan === 0) {
                        var bestValue = 0, bestPlayer = 0;
                        for (var i = 0; i < 6; i++) {
                            var player = this.players[i];
                            if (player && !player.folded && Math.abs(player.totalPoints) > bestValue  && Math.abs(player.totalPoints) <= 23) {
                                bestValue = Math.abs(player.totalPoints);
                                bestPlayer = i;
                            }
                        }
                        this.aiAlderaan = bestPlayer;
                        console.log("best player " + this.aiAlderaan + " " + bestValue);
                    }
                }

                console.log("Start Trading Phase.....");
            } else {
                this.gamePhase = "Betting";
                this.phasesCount++;
                this.maximumCurrentBet = 0;
                console.log("Start Betting Phase.....");
            }

            this.playerTurn = this.dealer;
            this.playerTurn %= 6;
            this.lastTurn = this.dealer;
            this.lastTurn %= 6;

        },

        startTurnTime: function(callback, time) {
            var playerPanel = this.playerPanels[this.playerTurn];
            if (this.turnTimer) {
                this.turnTimer.destroy();
                this.turnTimer = null;
            }
            this.turnTimer = SB.game.time.create(true);
            this.turnTimer.add(time, function() {
                SB.SFX.timer.stop();
                if (this.tween) {
                    this.tween.stop();
                    SB.game.tweens.remove(this.tween);
                    this.tween = null;
                    playerPanel.timer.drawTimer(0);
                }
                callback();
            }, this);
            this.turnTimer.start();

            var temp = { value: 4 };
            this.tween = SB.game.add.tween(temp).to({ value: 0}, 15500, "Linear", true);
            this.tween.onUpdateCallback(function(){
                playerPanel.timer.drawTimer(temp.value);
            }.bind(this));
            this.tween.onComplete.add(function() {
                this.tween.stop();
                SB.game.tweens.remove(this.tween);
                this.tween = null;
                playerPanel.timer.drawTimer(0);
                SB.SFX.timer.play();
            }.bind(this));
        },

        executeAction: function (btn, callback) {
            var player = this.players[this.playerTurn];
            var playerPanel = this.playerPanels[this.playerTurn];
            switch (btn.name) {
                case "callall":
                    this.callAll = !this.actionPanel.callallBtn.state;
                    if (this.checkFold) {
                        this.checkFold = false;
                        this.actionPanel.checkFoldBtn.toggle();
                    }
                    return;
                case "check-fold":
                    this.checkFold = !this.actionPanel.checkFoldBtn.state;
                    if (this.callAll) {
                        this.callAll = false;
                        this.actionPanel.callallBtn.toggle();
                    }
                    return;
                case "fold" :
                    playerPanel.fold();
                    this.foldedPlayers++;
                    break;
                case "allin":
                case "call" :
                    var diff = this.maximumCurrentBet - player.betAmount;
                    this.makeBet(player, playerPanel, diff);
                    break;
                case "check":
                case "stand":
                    SB.SFX.check.play();
                    break;
                case "bet":
                case "raise":
                    if (this.maximumCurrentBet === 0) {
                        this.bettingPanel.showPanel(Math.min(SB.Globals.blind, player.money), this.findMaxValueTobet(player));
                    } else {
                        this.bettingPanel.showPanel(2 * this.maximumCurrentBet, this.findMaxValueTobet(player));
                    }
                    var btnsToShow = this.findBetBtnsToShow();
                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    return;
                case  "confirm":
                    var diff = this.bettingPanel.value - player.betAmount;
                    this.makeBet(player, playerPanel, diff);
                    this.lastTurn = this.playerTurn - 1;
                    this.lastTurn += 6;
                    this.lastTurn %= 6;
                    this.raises++;
                    break;
                case "protect":
                    player.cardSelectWaitingFor = "protect";
                    var btnsToShow = ["backBtn"];
                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    this.helpMsg.visible = true;
                    this.helpMsg.text = "Select a card to Protect";
                    return;
                case "trade":
                    player.cardSelectWaitingFor = "trade";
                    var btnsToShow = ["backBtn"];
                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    this.helpMsg.visible = true;
                    this.helpMsg.text = "Select a card to Trade";
                    return;
                case "back":
                    if (this.gamePhase == "Betting") {
                        var btnsToShow = this.findBetBtnsToShow();
                    } else {
                        var btnsToShow = this.findTradeBtnsToShow();
                    }
                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    this.helpMsg.visible = false;
                    return;
                case "draw":
                    player.cardSelectWaitingFor = "";
                    player.getCard(this.currentDeck.dealCard());
                    this.animateCard(function() {}, this.playerTurn);
                    break;
                case "alderaan":
                    SB.SFX.alderaan.play();
                    this.alderaaned = true;
                    this.alderaanedPlayer = 0;
                    this.switchOffGameLights();
                    break;
            }

            if (this.turnTimer)
                this.turnTimer.stop();

            if (this.tween) {
                this.tween.stop();
                SB.game.tweens.remove(this.tween);
                this.tween = null;
                playerPanel.timer.drawTimer(0);
            }

            this.bettingPanel.hidePanel();
            this.actionPanel.hidePanel();

            if (callback) {
                callback();
            }
            this.endTurn();
        },

        cardSelectCallback: function(card) {

        },

        foldPlayer: function(player, playerPanel) {
            if (player.betAmount !== this.maximumCurrentBet) {
                playerPanel.fold();
                this.foldedPlayers++;
            }
        },

        makeBet: function (player, playerPanel, amount) {
            console.log(amount + "   " + this.maximumCurrentBet);
            if (amount < player.money) {
                player.putBet(amount);
                this.maximumCurrentBet = player.betAmount;
            } else {
                amount = player.money;
                player.putBet(amount);
                player.allIn = true;
                if (this.maximumCurrentBet <= player.betAmount) {
                    this.maximumCurrentBet = player.betAmount;
                }
                this.allinedPlayers++;
            }
            console.log(amount + "   " + this.maximumCurrentBet);
            playerPanel.updateMoney();
            if (amount !== 0) {
                playerPanel.chipToPile(amount, (this.maximumCurrentBet !== player.betAmount) ? SB.SFX.raise : SB.SFX.bet);
            } else {
                SB.SFX.check.play();
            }
        },

        sabaacShift: function() {
            SB.SFX.shift.play();

            var card2 = this.currentDeck.dealCard();
            this.players[0].tradeCard(card2, 2);
            this.playerPanels[0].updateCards();
            this.playerPanels[0].dissolveAndAppear(2);
        },

        /*
         Design Code starts here
         */

        animateCard: function (callback, playerPosition) {
            var playerPanel = this.playerPanels[playerPosition];
            var pos = {x: 0, y: 0};
            pos.x = playerPanel.handCardsArray[2].worldPosition.x * SB.xScale;
            pos.y = playerPanel.handCardsArray[2].worldPosition.y * SB.yScale;

            var angle = playerPanel.handCards.rotation;

            var card = this.physicalDeck.pop();
            var tween = SB.game.add.tween(card);

            tween.to({ x: pos.x, y: pos.y, rotation: angle }, 250, "Linear", true);

            tween.onComplete.add(function() {
                playerPanel.updateCards();
                if (playerPosition === 0) {
                    playerPanel.faceUpCards();
                }
                callback();
                SB.game.tweens.remove(tween);
                tween = null;
                card.destroy();
            });

            SB.SFX.deal.play();
        },

        tradeCardAnimation: function (callback, playerPosition) {
            var playerPanel = this.playerPanels[playerPosition];
            var pos = {x: 0, y: 0};
            pos.x = playerPanel.handCardsArray[2].worldPosition.x * SB.xScale;
            pos.y = playerPanel.handCardsArray[2].worldPosition.y * SB.yScale;

            var angle = playerPanel.handCards.rotation;

            var card = this.physicalDeck.pop();
            var tween = SB.game.add.tween(card);

            tween.from({ x: pos.x, y: pos.y, rotation: angle }, 190, "Linear", true);

            tween.onComplete.add(function() {
                tween.to({ x: pos.x, y: pos.y, rotation: angle }, 190, "Linear", true); {}
                tween.onComplete.add(function() {
                    playerPanel.updateCards();
                    if (playerPosition === 0) {
                        playerPanel.faceUpCards();
                    }
                    callback();
                    SB.game.tweens.remove(tween);
                    tween = null;
                    card.destroy();
                });
            });
            SB.SFX.deal.play();
        },

        createPhysicalDeck: function () {
            // Create Physical Deck
            for (var i = this.physicalDeck.length; i < 72; i++) {
                var card = SB.game.add.sprite(1640 - (i * 0.2), 495 - (i * 0.1), "sprites", "card-back");
                card.scale.set(0.4, 0.4);
                this.physicalDeck.push(card);
            }
        },

        switchOffGameLights: function () {
            if (this.lightTween1) {
                SB.game.tweens.remove(this.lightTween1);
                this.lightTween1 = null;
            }
            if (this.lightTween2) {
                SB.game.tweens.remove(this.lightTween2);
                this.lightTween2 = null;
            }
            this.lightTween1 = SB.game.add.tween(this.light1Crop).to({height: 0}, 1000, "Linear", true);
            this.lightTween2 = SB.game.add.tween(this.light2Crop).to({y: 455}, 1000, "Linear", true);

            this.lightTween1.onUpdateCallback(function(){
                this.gameLights.updateCrop();
                this.gameLights2.updateCrop();
            }.bind(this));
            this.lightTween2.onComplete.add(function() {
                SB.game.tweens.remove(this.lightTween1);
                this.lightTween1 = null;
                SB.game.tweens.remove(this.lightTween2);
                this.lightTween2 = null;
            }.bind(this));
        },

        switchOnGameLights: function () {
            this.light1Crop.height = 456;
            this.light2Crop.y = 0;
            this.gameLights.updateCrop();
            this.gameLights2.updateCrop();
        },

        createEnvironment: function () {
            this.background = SB.game.add.sprite(0, 0, "background");
            this.gameLights = SB.game.add.sprite(17, 70, "game-lights");
            this.gameLights2 = SB.game.add.sprite(17, 981, "game-lights2");
            this.gameLights2.anchor.set(0, 1);

            this.light1Crop = new Phaser.Rectangle(0, 0, 1827, 456);
            this.gameLights.crop(this.light1Crop);

            this.light2Crop = new Phaser.Rectangle(0, 0, 1827, 455);
            this.gameLights2.crop(this.light2Crop);

            this.dealerContainer = SB.game.add.group();

            SB.game.add.button(0, 940, "sprites", this.returnToMainMenu, this, "rtlobby-normal", "rtlobby-normal", "rtlobby-over");
            new SB.MenuButton("table-menu", "restart-tutorial", this.restartTutorial.bind(this), { x: 1780, y: 992 }, this.dealerContainer);

            this.createPhysicalDeck();

            this.sabaacMsg = SB.game.add.sprite(825, 360, "sprites", "sabbac");
            this.diamond = SB.game.add.sprite(835, 380, "sprites", "sabacc-logo");
            this.sabaacMsg.scale.set(0.75, 0.75);
            this.sabaacMsg.visible = false;
            this.suddenDemiseMsg = SB.game.add.sprite(770, 305, "sprites", "sudden-demise");
            this.suddenDemiseMsg.visible = false;

            SB.game.add.sprite(1625, 415, "sprites", "droid");

            this.playerPanels.push (new SB.PlayerPanel(1, this.cardSelectCallback.bind(this)));

            var aiNames = ["R5-C7", "R9-P3", "R6-G9", "R3-S6", "R7-X4"]

            for (var i = 0; i < this.NO_OF_PLAYERS - 1; i++) {
                this.playerPanels.push (new SB.PlayerPanel(i + 2, this.cardSelectCallback.bind(this)));
            }

            this.dealerToken = new SB.DealerToken(this.dealer + 1, this.dealerContainer);

            this.mainPotImage = SB.game.add.group();
            this.mainPotImage.create(0, 0, "sprites", "main-pot");
            this.mainPotText = SB.game.add.text(145, 52, "0");
            this.mainPotChip = SB.game.add.sprite(0, 0, "sprites", "chip1");
            this.mainPotChip.scale.set(0.72, 0.72);

            this.mainPotImage.add(this.mainPotText);
            this.mainPotImage.add(this.mainPotChip);
            this.mainPotImage.position.set(600, 420);

            this.sabbacPotImage = SB.game.add.group();
            this.sabbacPotImage.create(0, 0, "sprites", "sabbac-pot-chip");
            this.sabbacPotText = SB.game.add.text(160, 63, "0");

            this.sabbacPotImage.add(this.sabbacPotText);
            this.sabbacPotImage.position.set(1050, 420);

            this.mainPotText.anchor.set(0.5, 0.5);
            this.mainPotText.font = 'MyFont';
            this.mainPotText.fontWeight = 'bold';
            this.mainPotText.fontSize = 32;
            this.mainPotText.fill = '#FFFFFF';

            this.sabbacPotText.anchor.set(0.5, 0.5);
            this.sabbacPotText.font = 'MyFont';
            this.sabbacPotText.fontWeight = 'bold';
            this.sabbacPotText.fontSize = 32;
            this.sabbacPotText.fill = '#FFFFFF';

            this.bettingPanel = new SB.BettingPanel();
            this.actionPanel = new SB.ActionPanel(this.executeAction.bind(this));
            this.actionPanel.hidePanel();
            this.bettingPanel.hidePanel();

            this.helpMsg = SB.game.add.text(1920 / 2, 800, "Please select a card to protect");
            this.helpMsg.anchor.set(0.5, 0.5);
            this.helpMsg.font = "Arial";
            this.helpMsg.stroke = "#00FFFF";
            this.helpMsg.strokeThickness = 6;
            this.helpMsg.visible = false;

            this.messageBox = new SB.FeedbackObject();
            this.winnerMsg = new SB.WinnerMessage(this.replayGame.bind(this), this.returnToMainMenu.bind(this));

            // Creating SFX
            SB.SFX = {};
            SB.SFX.bet = this.game.add.sound("bet");
            SB.SFX.deal = this.game.add.sound("deal");
            SB.SFX.interference = this.game.add.sound("interference");
            SB.SFX.movechip = this.game.add.sound("movechip");
            SB.SFX.shuffle = this.game.add.sound("shuffle");
            SB.SFX.shift = this.game.add.sound("shift");
            SB.SFX.turn = this.game.add.sound("turn");
            SB.SFX.winchip = this.game.add.sound("winchip");
            SB.SFX.sabaac = this.game.add.sound("sabaac");
            SB.SFX.bombout = this.game.add.sound("bombout");

            SB.SFX.alderaan = this.game.add.sound("alderaan");
            SB.SFX.check = this.game.add.sound("check");
            SB.SFX.fold = this.game.add.sound("fold");
            SB.SFX.raise = this.game.add.sound("raise");
            SB.SFX.timer = this.game.add.sound("timer");
        },

        updateSabbacPot: function () {
            this.sabbacPotText.text = this.sabbacPot;
        },

        updateMainPot: function () {
            this.mainPotText.text = this.mainPot;
            for (var i = 0; i < 6; i++) {
                if (this.players[i] && this.players[i].betAmount !== 0) {
                    this.playerPanels[i].pileToMainPot(this.players[i].betAmount);
                }
            }

            setTimeout(function() {
                this.mainPotChip.frameName = "chip" + SB.utils.getChipColor (this.mainPot) + "";
            }.bind(this), 500);
        },

        returnToMainMenu: function() {
            document.removeEventListener("backbutton", this.returnToMainMenu.bind(this), false);

            var id = setTimeout(function() {}, 0);

            while (id--) {
                clearTimeout(id); // will do nothing if no timeout with id is present
            }
            this.state.start('Menu');
        },

        restartTutorial: function() {
            document.removeEventListener("backbutton", this.returnToMainMenu.bind(this), false);

            var id = setTimeout(function() {}, 0);

            while (id--) {
                clearTimeout(id); // will do nothing if no timeout with id is present
            }
            this.state.start('Tutorial');
        },

        replayGame: function() {
            var id = setTimeout(function() {}, 0);

            while (id--) {
                clearTimeout(id); // will do nothing if no timeout with id is present
            }
            this.state.start('SingleGame');
        }
    };

    return SB.Tutorial;
});

/**
 * Deck Object containing 76 cards deck.
 * Card Type Reference:
 *          Suits: St (Staves), Fl (Flasks), Co (Coins), Sb (Sabres)
 *          Suit Card Values : 1-11, C (Commander), MI (Mistress), MA (Master), A (Ace)
 *          Ranked Cards: I (Idiot), Q (Queen of Air and Darkness), E (Endurance), B (Balance), D (Demise), M (Moderation), Ev (The Evil One), S (The Star)
 */

SB.PlayerTutorial = function (isAI, position, name) {
    this.name = name;
    this.position = position;
    this.isAI = isAI || false;
    this.cards = new Array ();
    this.interferenceCards = [];
    this.totalPoints = 0;
    this.betAmount = 0;
    this.money = 10000;
    this.folded = false;
    this.cardSelectWaitingFor = "";
    this.allIn = false;
};

SB.PlayerTutorial.prototype = {
    takeAnte: function (amount) {
        this.money = this.money - amount;
    },

    putBet: function (amount) {
        this.money -= amount;
        this.betAmount += amount;
    },

    getCard: function(card) {
        this.cards.push(card);
        this.totalPoints += card.points;
    },

    removeCard: function() {
        var card = this.cards.pop();
        this.totalPoints -= card.points;

        return card;
    },

    tradeCard: function(newCard, oldCardPosition) {
        this.totalPoints -= this.cards[oldCardPosition].points;
        this.cards[oldCardPosition] = newCard;
        this.totalPoints += newCard.points;
    },

    giveCard: function(oldCardPosition) {
        this.totalPoints -= this.cards[oldCardPosition].points;
        this.cards.splice(oldCardPosition, 1);
    },

    resetForPhase: function() {
        this.betAmount = 0;
    },

    resetForRound: function() {
        this.totalPoints = 0;
        this.betAmount = 0;
        this.folded = false;
        this.cards = [];
        this.interferenceCards = [];
        this.cardSelectWaitingFor = "";
    },

    getMoney: function (amount) {
        this.money += amount;
    },

    protectCard: function (oldCardPosition) {
        var card = this.cards.splice(oldCardPosition, 1)[0];
        card.isInterferenced = true;
        this.interferenceCards.push(card);
    }
}

SB.DeckTutorial = function () {
    this.cards = new Array ();
};

SB.DeckTutorial.prototype = {
    makeDeck: function () {
        var values = new Array("MA", "9", "8", "4", "6", "5", "7", "3", "2", "10", "11", "MI", "A", "1");
        var points = new Array(14, 9, 8, 4, 6, 5, 7, 3, 2, 10, 11, 13, 15, 1);
        var suits = new Array("Sb");
        var rankCards = new Array("E", "B", "D", "M", "Ev", "S");
        var rankValues = new Array(-8, -11, -13, -14, -15, -17);
        var i, j;
        var n;

        n = values.length * suits.length;

        // Set array of cards.

        this.cards = new Array();

        // Fill the array with 'n' packs of cards.

        // Fill in suit cards
        for (i = 0; i < suits.length; i++) {
            for (j = 0; j < values.length; j++) {
                var card = new SB.Card(values[j], suits[i], points[j]);
                this.cards.push(card);
                card.cardToString();
            }
        }

        // Fill in ranked cards
        for (i = 0; i < rankCards.length; i++) {
            for (j = 0; j < 2; j++) {
                var card = new SB.Card(rankCards[i], "Rk", rankValues[i]);
                this.cards.push(card);
                card.cardToString();
            }
        }
    },

    shuffle: function () {
        var i, j, k;
        var temp;

        // Shuffle the stack '3' times.

        for (i = 0; i < 3; i++) {
            for (j = 0; j < this.cards.length; j++) {
                k = Math.floor(Math.random() * this.cards.length);
                temp = this.cards[j];
                this.cards[j] = this.cards[k];
                this.cards[k] = temp;
            }
        }
    },

    dealCard: function () {
        return this.cards.shift();
    }
}



