define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.PreloadState = function() {};

    SB.PreloadState.prototype = {
        preload: function() {
            this.add.sprite(0, 0, 'preloaderBg');
            this.preloadBar = this.add.sprite(960, 920, 'preloaderBar');
            this.preloadBar.anchor.setTo(0,0.5);
            this.preloadBar.x = 960 - this.preloadBar.width/2;

            this.preloadBarBg = this.add.sprite(960, 920, 'preloaderBarBg');
            this.preloadBarBg.anchor.setTo(0,0.5);
            this.preloadBarBg.x = 960 - this.preloadBarBg.width/2;

            //this statement sets the blue bar to represent the actual percentage of data loaded
            this.load.setPreloadSprite(this.preloadBar);

            this.load.image("menuBG", "././assets/menuBG.png");
            this.load.image("lobbyBG", "././assets/lobbyBG.png");
            this.load.image("background", "././assets/background.png");
            this.load.image("game-lights", "././assets/game-lights.png");
            this.load.image("game-lights2", "././assets/game-lights2.png");
            this.load.image("login-bg", "././assets/login-bg.png");
            this.load.image("shop-bg", "././assets/shop-bg.png");
            this.load.atlas("sprites", "././assets/sprites.png", "././assets/sprites.json");
            this.load.atlas("sprites2", "././assets/sprites2.png", "././assets/sprites2.json");

            // Audio
            this.load.audio("bet", ["assets/sfx/bet.mp3", "assets/sfx/bet.ogg"]);
            this.load.audio("deal", ["assets/sfx/deal.mp3", "assets/sfx/deal.ogg"]);
            this.load.audio("interference", ["assets/sfx/interference.mp3", "assets/sfx/interference.ogg"]);
            this.load.audio("movechip", ["assets/sfx/movechip.mp3", "assets/sfx/movechip.ogg"]);
            this.load.audio("shuffle", ["assets/sfx/shuffle.mp3", "assets/sfx/shuffle.ogg"]);
            this.load.audio("check", ["assets/sfx/check.mp3", "assets/sfx/check.ogg"]);
            this.load.audio("alderaan", ["assets/sfx/alderaan.mp3", "assets/sfx/alderaan.ogg"]);
            this.load.audio("bombout", ["assets/sfx/bombout.mp3", "assets/sfx/bombout.ogg"]);
            this.load.audio("fold", ["assets/sfx/fold.mp3", "assets/sfx/fold.ogg"]);
            this.load.audio("raise", ["assets/sfx/raise.mp3", "assets/sfx/raise.ogg"]);
            this.load.audio("sabaac", ["assets/sfx/sabaac.mp3", "assets/sfx/sabaac.ogg"]);
            this.load.audio("shift", ["assets/sfx/shift.mp3", "assets/sfx/shift.ogg"]);
            this.load.audio("turn", ["assets/sfx/turn.mp3", "assets/sfx/turn.ogg"]);
            this.load.audio("winchip", ["assets/sfx/winchip.mp3", "assets/sfx/winchip.ogg"]);
            this.load.audio("timer", ["assets/sfx/timer.mp3", "assets/sfx/timer.ogg"]);
        },

        create: function() {
            this.game.sound.setDecodedCallback([
                'bet', 'deal', 'interference', 'movechip', 'shuffle'
            ], this.startGame, this);
        },

        startGame: function() {
            var username = localStorage.getItem("username");
            var password = localStorage.getItem("password");
            var name = localStorage.getItem("player_name");

            if (username && username != "") {
                if (password == "facebook") {
                    this.loginFB(username, name);
                } else if (password == "device") {
                    this.loginGuest(username);
                } else {
                    this.login(username, password);
                }
            } else {
                this.state.start('Menu');
            }
        },

        login: function(username, password) {
            var data = {username: username, password: password, oauth_provider: "normal"};
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SB.API_URL + "login.api.php",
                data: {'data': JSON.stringify(data)},
                success: function(data) {
                    if (data.responseStatus === "true") {
                        SB.CurrentUser = {
                            id: data["data"]["user_id"],
                            username: data["data"]["username"],
                            credits: parseInt(data["data"]["credits"]),
                            fbLogin: false,
                            guestLogin: false
                        }
                    }

                    this.state.start('Menu');
                }.bind(this),
                error: function() {
                    this.state.start('Menu');
                }.bind(this)
            });
        },

        loginFB: function(username, playerName) {
            var data = {username: username, oauth_provider: "facebook"};
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SB.API_URL + "login.api.php",
                data: {'data': JSON.stringify(data)},
                success: function(data) {
                    if (data.responseStatus === "true") {
                        SB.CurrentUser = {
                            id: data["data"]["user_id"],
                            username: playerName,
                            credits: parseInt(data["data"]["credits"]),
                            fbLogin: true,
                            guestLogin: false
                        }
                    }

                    this.state.start('Menu');
                }.bind(this),
                error: function() {
                    this.state.start('Menu');
                }.bind(this)
            });
        },

        loginGuest: function(username) {
            var data = {username: username, oauth_provider: "device"};
            console.log(username);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SB.API_URL + "login.api.php",
                data: {'data': JSON.stringify(data)},
                success: function(data) {
                    console.log("here "+ JSON.stringify(data));
                    if (data.responseStatus === "true") {
                        SB.CurrentUser = {
                            id: data["data"]["user_id"],
                            username: "Guest" + _.random(10000, 99999),
                            credits: parseInt(data["data"]["credits"]),
                            fbLogin: false,
                            guestLogin: true
                        };
                    }

                    this.state.start('Menu');
                }.bind(this),
                error: function() {
                    this.state.start('Menu');
                }.bind(this)
            });
        }
    };

    return SB.PreloadState;
});