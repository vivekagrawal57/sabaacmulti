define([
    'phaser',
    'underscore',
    'js/objects/DealerToken.js',
    'js/objects/FeedbackObject.js',
    'js/objects/PlayerPanel.js',
    'js/objects/ActionPanel.js',
    'js/objects/BettingPanel.js',
    'js/objects/WinnerMessage.js',

], function (
    Phaser,
    UnderScore,
    DealerToken,
    PlayerPanel,
    BettingPanel,
    ActionPanel,
    WinnerMessage
) {
    'use strict';

    SB.SingleState = function() {};

    SB.SingleState.prototype = {
        currentDeck: null,
        players: [],
        playerPanels: [],
        gamePhase: SB.Constants.ANTE,
        mainPot: 0,
        sabbacPot: 0,
        callAll: false,
        checkFold: false,
        dealer: 2,
        playerTurn: null,
        phasesCount: 1,
        folderPlayers: 0,
        maximumCurrentBet: 0,
        raises: 0,
        lastTurn: 0,
        alderaaned: false,
        playerCount: 6,
        physicalDeck: [],
        alderaanedPlayer: -1,
        foldedPlayers: 0,
        turnTimer: null,
        shiftTimer: null,
        aiAlderaan: -2,
        allinedPlayers: 0,

        preload: function() {

        },

        create: function() {
            this.NO_OF_PLAYERS = 6;
            this.setup();

            this.createEnvironment();

            document.addEventListener("backbutton", this.returnToMainMenu.bind(this), false);

            // Creat a Card Deck and Shuffle it

            this.currentDeck = new SB.Deck();

            // Create Player and AI opponents
            this.players.push(new SB.PlayerSingle (false, 0, "Player"));
            this.playerPanels [0].assignPlayer(this.players [0]);

            var aiNames = ["R5-C7", "R9-P3", "R6-G9", "R3-S6", "R7-X4"];

            for (var i = 0; i < this.NO_OF_PLAYERS - 1; i++) {
                this.players.push (new SB.PlayerSingle(true, i + 1, aiNames[i]));
                this.playerPanels [i + 1].assignPlayer(this.players [i + 1]);
            }

            // Game Start

            this.startRound();
        },

        setup: function() {
            this.currentDeck = null;
            this.players = [];
            this.playerPanels = [];
            this.gamePhase = SB.Constants.ANTE;
            this.mainPot = 0;
            this.sabbacPot = 0;
            this.callAll = false;
            this.checkFold = false;
            this.dealer = 2;
            this.playerTurn = null;
            this.phasesCount = 1;
            this.folderPlayers = 0;
            this.maximumCurrentBet = 0;
            this.raises = 0;
            this.lastTurn = 0;
            this.alderaaned = false;
            this.playerCount = 6;
            this.physicalDeck = [];
            this.alderaanedPlayer = -1;
            this.foldedPlayers = 0;
            this.turnTimer = null;
            this.shiftTimer = null;
            this.allinedPlayers = 0;
            this.subPots = [];
            SB.Globals.buyin = 10000;
            SB.Globals.blind = 100;
            SB.Globals.ante = 25;
        },

        /*
         Game Logic Code starts here
         */

        startRound: function () {
            console.log("Starting new round....");

            // Changing dealer in clockwise direction and setting player turn
            do {
                this.dealer ++;
                this.dealer %= 6;
            } while (!this.players[this.dealer]);
            this.dealerToken.shift(this.dealer);

            for (var i = 0; i < 6; i++) {
                if (this.players[i]) {
                    this.players[i].allIn = false;
                }
            }


            // Starting player would be the 3rd player from dealer since 1st and 2nd do blind bets
            this.playerTurn = this.dealer + 3;
            this.playerTurn %= 6;

            // Start timer for sabaac shift
            this.startSabaacShiftTimer();
            setTimeout(this.takeAnte.bind(this), 100);
        },

        takeAnte: function () {
            console.log("Taking Ante....");

            this.gamePhase = "Ante";

            for (var i = 0; i < 6; i++) {
                if (this.players[i]) {
                    this.players [i].takeAnte(SB.Globals.ante);
                    this.playerPanels [i].updateMoney();
                    this.playerPanels [i].chipToSabacc(SB.Globals.ante);
                    this.sabbacPot += SB.Globals.ante;
                }
            }

            this.updateSabbacPot();
            setTimeout(this.takeBlinds.bind(this), 1000);
        },

        takeBlinds: function () {
            // Take Blind Bets from 2 players next to Dealer and put them as counters on table

            for (var i = 1, j = 1; i <= 6 && j <= 2; i++) {
                if (this.players[(this.dealer + i) % 6]) {
                    this.makeBet(this.players [(this.dealer + i) % 6], this.playerPanels [(this.dealer + i) % 6], Math.ceil((j * SB.Globals.blind) / 2));
                    j++;
                }
            }

            this.maximumCurrentBet = SB.Globals.blind;
            this.lastTurn = this.dealer + 2;
            this.lastTurn %= 6;

            this.dealCards();
        },

        dealCards: function () {
            console.log("Dealing Cards....");

            this.gamePhase = "Dealing";

            this.currentDeck.makeDeck();
            this.currentDeck.shuffle();
            SB.SFX.shuffle.play();
            this.createPhysicalDeck();

            var i = 0, j = 0;

            var callback = function() {
                if (j < 6) {
                    var playerPosition = (j + this.dealer + 1) % 6;

                    j++;
                    if (this.players[playerPosition]) {
                        this.players [playerPosition].getCard(this.currentDeck.dealCard());
                        this.animateCard(callback, playerPosition);
                    } else {
                        callback();
                    }
                } else {
                    i++;
                    if (i < 2) {
                        j = 0;
                        callback();
                    } else {
                        this.startBettingPhase();
                    }
                }

            }.bind(this);

            callback();
        },

        startBettingPhase: function () {
            console.log("Starting Betting Phase....")

            this.gamePhase = "Betting";

            this.actionPanel.hidePanel();
            this.callAll = false;
            this.checkFold = false;

            this.takeTurn();
        },

        takeTurn: function () {
            console.log(this.playerTurn + " " + this.lastTurn);
            if (!this.players[this.playerTurn] || this.players[this.playerTurn].folded) {
                this.endTurn();
                return;
            }
            if (this.gamePhase == "Betting") {
                if (this.players[this.playerTurn].allIn) {
                    this.endTurn();
                    return;
                }
            }
            if (this.gamePhase == "Betting") {
                if (this.players[this.playerTurn].isAI) {
                    this.actionPanel.showPanel(["callallBtn", "checkFoldBtn"]);
                    this.startTurnTime(this.aiBetting.bind(this), 1000 + Math.random() * 6000);
                } else {
                    SB.SFX.turn.play();
                    this.startTurnTime(function() {
                        if (player.betAmount == this.maximumCurrentBet) {
                            this.executeAction({name: "check"});
                        } else {
                            this.executeAction({name: "fold"});
                        }
                    }.bind(this), 20000);

                    var player = this.players[this.playerTurn];

                    if (this.callAll) {
                        this.executeAction({name: "call"});
                        return;
                    }

                    if (this.checkFold) {
                        if (player.betAmount == this.maximumCurrentBet) {
                            this.executeAction({name: "check"});
                        } else {
                            this.executeAction({name: "fold"});
                        }
                        return;
                    }

                    var btnsToShow = this.findBetBtnsToShow();

                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                }
            } else if (this.gamePhase == "Trading") {
                if (this.players[this.playerTurn].isAI) {
                    this.startTurnTime(this.aiTrading.bind(this), 1000 + Math.random() * 6000);
                } else {
                    SB.SFX.turn.play();
                    this.startTurnTime(function() {
                        this.executeAction({name: "stand"});
                    }.bind(this), 20000);

                    var btnsToShow = this.findTradeBtnsToShow();

                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                }
            }
        },

        endTurn: function() {
            if (this.playerTurn === this.lastTurn) {
                this.switchPhase();
                if (this.alderaaned) {
                    return;
                }
            }
            this.playerTurn++;
            this.playerTurn %= 6;
            SB.SFX.timer.stop();
            this.takeTurn();
        },

        findMaxValueTobet: function(player) {
            var highestMoney = 0, secondHighest = 0;

            for (var i = 0; i < 6; i++) {
                if (this.players[i] && (this.players[i].money + this.players[i].betAmount) > secondHighest) {
                    if ((this.players[i].money + this.players[i].betAmount) > highestMoney) {
                        secondHighest = highestMoney;
                        highestMoney = (this.players[i].money + this.players[i].betAmount);
                    } else {
                        secondHighest = (this.players[i].money + this.players[i].betAmount);
                    }
                }
            }
            return Math.min((player.money + player.betAmount), secondHighest);
        },

        findBetBtnsToShow: function(dontProtect) {
            // Show Action Panel with actions player can take (note check and call cant come together, all in is always avaliable and raise only if player has more money than current max)
            var btnsToShow = new Array("foldBtn");
            var player = this.players[this.playerTurn];
            if (player.betAmount == this.maximumCurrentBet) {
                btnsToShow.push("checkBtn");
            } else {
                if (player.money >= (this.maximumCurrentBet - player.betAmount)) {
                    btnsToShow.push("callBtn");
                } else {
                    btnsToShow.push("allinBtn");
                }
            }

            if (this.bettingPanel.container.visible) {
                btnsToShow.push("confirmBtn");
            } else {
                if (this.maximumCurrentBet === 0) {
                    btnsToShow.push("betBtn");
                } else if (player.money >= (2 * this.maximumCurrentBet - player.betAmount)) {
                    btnsToShow.push("raiseBtn");
                }
            }

            if (player.interferenceCards.length === 0 && !dontProtect) {
                btnsToShow.push("protectBtn");
            }

            return btnsToShow;
        },

        findTradeBtnsToShow: function (dontProtect) {
            // Show Action Panel with actions player can take during trading phaser, alderaan only avaliable after 2nd betting
            var player = this.players[this.playerTurn];
            var btnsToShow = new Array();
            if (this.phasesCount > 1 && !this.alderaaned) {
                btnsToShow.push("alderaanBtn");
            }
            btnsToShow.push("standBtn", "tradeBtn");

            if (player.interferenceCards.length < 2 && !dontProtect) {
                btnsToShow.push("protectBtn");
            }

            if (player.cards.length + player.interferenceCards.length < 5) {
                btnsToShow.push("drawBtn");
            }

            return btnsToShow;
        },

        switchPhase: function () {
            if (this.alderaaned) {
                this.gamePhase = "Alderaan";
                this.calculateWinner();
                return;
            }
            if (this.gamePhase == "Betting") {
                this.gamePhase = "Trading";
                this.actionPanel.hidePanel();

                var bets = [].concat(this.players);
                bets.sort(function(a, b) {
                    if (!a) {
                        return -1;
                    }
                    if (!b) {
                        return 1;
                    }
                    return a.betAmount - b.betAmount;
                });

                var betAmounts = [];
                for (var i = 0; i < 6; i++) {
                    if (bets[i]) {
                        betAmounts.push(bets[i].betAmount);
                    } else {
                        betAmounts.push(0);
                    }
                }

                console.log(betAmounts);

                var isFirstPot = true;

                for (var i = 0; i < 6; i++) {
                    if (bets[i]) {
                        var totalAmount = 0;
                        var winners = [];

                        if (bets[i].folded || betAmounts[i] === 0)
                            continue;

                        var amountToTake = betAmounts[i];

                        for (var j = 0; j < 6; j++) {
                            var amount = Math.min(betAmounts[j], amountToTake);
                            totalAmount += amount;
                            betAmounts[j] -= amount;
                            if (bets[j] && !bets[j].folded && amount !== 0) {
                                winners.push(bets[j].position);
                            }
                        }

                        if (isFirstPot && this.subPots.length > 0 && this.subPots[this.subPots.length - 1].players.length === (winners.length + this.foldedPlayers)) {
                            this.subPots[this.subPots.length - 1].amount += totalAmount;
                        } else {
                            this.subPots.push({amount: totalAmount, players: winners});
                        }

                        isFirstPot = false;

                        console.log("Pot: " + totalAmount + " Winners: " + winners);
                    }
                }

                // Take all bets and put them in main pot
                for (var i = 0; i < 6; i++) {
                    if (this.players[i]) {
                        this.mainPot += this.players[i].betAmount;
                    }
                }
                this.updateMainPot();

                for (var i = 0; i < 6; i++) {
                    if (this.players[i]) {
                        this.players[i].resetForPhase();
                        this.playerPanels[i].showBet();
                    }
                }

                if (this.players[0].folded) {
                    // If player folded increment counter to wait for 2nd trading and then assign the player to alderaan
                    this.aiAlderaan++;
                    if (this.aiAlderaan === 0) {
                        var bestValue = 0, bestPlayer = 0;
                        for (var i = 0; i < 6; i++) {
                            var player = this.players[i];
                            if (player && !player.folded && Math.abs(player.totalPoints) > bestValue  && Math.abs(player.totalPoints) <= 23) {
                                bestValue = Math.abs(player.totalPoints);
                                bestPlayer = i;
                            }
                        }
                        this.aiAlderaan = bestPlayer;
                        console.log("best player " + this.aiAlderaan + " " + bestValue);
                    }
                }

                console.log("Start Trading Phase.....");
            } else {
                if (this.allinedPlayers === (this.playerCount - this.foldedPlayers) - 1) {
                    this.alderaaned = true;
                    SB.SFX.alderaan.play();
                    this.switchOffGameLights();
                    this.calculateWinner();
                    return;
                }
                this.gamePhase = "Betting";
                this.phasesCount++;
                this.maximumCurrentBet = 0;
                console.log("Start Betting Phase.....");
            }

            this.playerTurn = this.dealer;
            this.playerTurn %= 6;
            this.lastTurn = this.dealer;
            this.lastTurn %= 6;

        },

        startTurnTime: function(callback, time) {
            var playerPanel = this.playerPanels[this.playerTurn];
            if (this.turnTimer) {
                this.turnTimer.destroy();
                this.turnTimer = null;
            }
            this.turnTimer = SB.game.time.create(true);
            this.turnTimer.add(time, function() {
                SB.SFX.timer.stop();
                if (this.tween) {
                    this.tween.stop();
                    SB.game.tweens.remove(this.tween);
                    this.tween = null;
                    playerPanel.timer.drawTimer(0);
                }
                callback();
            }, this);
            this.turnTimer.start();

            var temp = { value: 4 };
            this.tween = SB.game.add.tween(temp).to({ value: 0}, 15500, "Linear", true);
            this.tween.onUpdateCallback(function(){
                playerPanel.timer.drawTimer(temp.value);
            }.bind(this));
            this.tween.onComplete.add(function() {
                this.tween.stop();
                SB.game.tweens.remove(this.tween);
                this.tween = null;
                playerPanel.timer.drawTimer(0);
                SB.SFX.timer.play();
            }.bind(this));
        },

        executeAction: function (btn) {
            var player = this.players[this.playerTurn];
            var playerPanel = this.playerPanels[this.playerTurn];
            switch (btn.name) {
                case "callall":
                    this.callAll = !this.actionPanel.callallBtn.state;
                    if (this.checkFold) {
                        this.checkFold = false;
                        this.actionPanel.checkFoldBtn.toggle();
                    }
                    return;
                case "check-fold":
                    this.checkFold = !this.actionPanel.checkFoldBtn.state;
                    if (this.callAll) {
                        this.callAll = false;
                        this.actionPanel.callallBtn.toggle();
                    }
                    return;
                case "fold" :
                    playerPanel.fold();
                    this.foldedPlayers++;
                    if (this.foldedPlayers === this.playerCount - 1) {
                        this.calculateWinner();
                        return;
                    }
                    break;
                case "allin":
                case "call" :
                    var diff = this.maximumCurrentBet - player.betAmount;
                    this.makeBet(player, playerPanel, diff);
                    break;
                case "check":
                case "stand":
                    SB.SFX.check.play();
                    break;
                case "bet":
                case "raise":
                    if (this.maximumCurrentBet === 0) {
                        this.bettingPanel.showPanel(Math.min(SB.Globals.blind, player.money), this.findMaxValueTobet(player));
                    } else {
                        this.bettingPanel.showPanel(2 * this.maximumCurrentBet, this.findMaxValueTobet(player));
                    }
                    var btnsToShow = this.findBetBtnsToShow();
                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    return;
                case  "confirm":
                    var diff = this.bettingPanel.value - player.betAmount;
                    this.makeBet(player, playerPanel, diff);
                    this.lastTurn = this.playerTurn - 1;
                    this.lastTurn += 6;
                    this.lastTurn %= 6;
                    this.raises++;
                    break;
                case "protect":
                    player.cardSelectWaitingFor = "protect";
                    var btnsToShow = ["backBtn"];
                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    this.helpMsg.visible = true;
                    this.helpMsg.text = "Select a card to Protect";
                    return;
                case "trade":
                    player.cardSelectWaitingFor = "trade";
                    var btnsToShow = ["backBtn"];
                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    this.helpMsg.visible = true;
                    this.helpMsg.text = "Select a card to Trade";
                    return;
                case "back":
                    if (this.gamePhase == "Betting") {
                        var btnsToShow = this.findBetBtnsToShow();
                    } else {
                        var btnsToShow = this.findTradeBtnsToShow();
                    }
                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    this.helpMsg.visible = false;
                    return;
                case "draw":
                    player.cardSelectWaitingFor = "";
                    player.getCard(this.currentDeck.dealCard());
                    this.animateCard(function() {}, this.playerTurn);
                    break;
                case "alderaan":
                    SB.SFX.alderaan.play();
                    this.alderaaned = true;
                    this.alderaanedPlayer = 0;
                    this.switchOffGameLights();
                    break;
            }

            if (this.turnTimer)
                this.turnTimer.stop();

            if (this.tween) {
                this.tween.stop();
                SB.game.tweens.remove(this.tween);
                this.tween = null;
                playerPanel.timer.drawTimer(0);
            }

            this.bettingPanel.hidePanel();
            this.actionPanel.hidePanel();

            if (this.gamePhase == "Betting" && !this.players[0].folded) {
                this.callAll = false;
                this.checkFold = false;
                this.actionPanel.showPanel(["callallBtn", "checkFoldBtn"]);
            }

            this.endTurn();
        },

        cardSelectCallback: function(card) {
            var player = this.players[this.playerTurn];
            var playerPanel = this.playerPanels[this.playerTurn];

            switch (player.cardSelectWaitingFor) {
                case "trade" :
                    player.giveCard(card.positionInHand);
                    playerPanel.updateCards();
                    if (this.playerTurn === 0) {
                        playerPanel.faceUpCards();
                    }

                    player.getCard(this.currentDeck.dealCard());
                    this.tradeCardAnimation(function() {}, this.playerTurn);

                    player.cardSelectWaitingFor = "";
                    if (this.playerTurn === this.lastTurn) {
                        this.switchPhase();
                    }
                    if (this.tween) {
                        this.tween.stop();
                        SB.game.tweens.remove(this.tween);
                        this.tween = null;
                        playerPanel.timer.drawTimer(0);
                    }
                    this.playerTurn++;
                    this.playerTurn %= 6;
                    this.actionPanel.hidePanel();
                    this.takeTurn();
                    break;
                case "protect":
                    player.protectCard(card.positionInHand);
                    player.cardSelectWaitingFor = "";
                    playerPanel.updateCards();

                    if (this.playerTurn === 0) {
                        playerPanel.faceUpCards();
                    }

                    SB.SFX.interference.play();

                    if (this.gamePhase == "Betting") {
                        var btnsToShow = this.findBetBtnsToShow(true);
                    } else {
                        var btnsToShow = this.findTradeBtnsToShow(true);
                    }

                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    break;
            }
            this.helpMsg.visible = false;
        },

        foldPlayer: function(player, playerPanel) {
            if (player.betAmount !== this.maximumCurrentBet) {
                playerPanel.fold();
                this.foldedPlayers++;
            }
        },

        aiBetting: function () {
            var player = this.players [this.playerTurn];
            var playerPanel = this.playerPanels[this.playerTurn];
            var hasRaised = false;
            var points = player.totalPoints;

            if (this.phasesCount === 1) {
                switch (true) {
                    case (points >= -17 && points <= 13):
                    case (points > 23):
                    case (points < -27):
                        switch (this.raises) {
                            case 0:
                                this.makeBet(player, playerPanel, this.maximumCurrentBet - player.betAmount);
                                break;
                            case 1:
                                if (Math.random() < 0.5) {
                                    this.makeBet(player, playerPanel, this.maximumCurrentBet - player.betAmount);
                                } else {
                                    this.foldPlayer(player, playerPanel);
                                }
                                break;
                            default :
                                this.foldPlayer(player, playerPanel);
                                break;
                        }
                        break;
                    case (points >= 14 && points <= 19):
                    case (points >= -27 && points <= 24):
                    case (points >= -19 && points <= -18):
                        switch (this.raises) {
                            case 0:
                                if (Math.random() < 0.8) {
                                    this.makeBet(player, playerPanel, this.maximumCurrentBet - player.betAmount);
                                } else {
                                    this.makeBet(player, playerPanel, 2 * this.maximumCurrentBet - player.betAmount);
                                    hasRaised = true;
                                }
                                break;
                            case 1:
                                if (Math.random() < 0.75) {
                                    this.makeBet(player, playerPanel, this.maximumCurrentBet - player.betAmount);
                                } else {
                                    this.foldPlayer(player, playerPanel);
                                }
                                break;
                            default :
                                this.foldPlayer(player, playerPanel);
                                break;
                        }
                        break;
                    case (points >= 20 && points <= 22):
                    case (points >= -22 && points <= -20):
                        switch (this.raises) {
                            case 0:
                                this.makeBet(player, playerPanel, 3 * this.maximumCurrentBet - player.betAmount);
                                hasRaised = true;
                                break;
                            default :
                                if (Math.random() < 1) {
                                    this.makeBet(player, playerPanel, this.maximumCurrentBet - player.betAmount);
                                } else {
                                    this.makeBet(player, playerPanel, 2 * this.maximumCurrentBet - player.betAmount);
                                    hasRaised = true;
                                }
                                break;
                        }
                        break;
                    case (points === 23 || points === -23):
                        switch (this.raises) {
                            case 0:
                            case 1:
                                this.makeBet(player, playerPanel, 3 * this.maximumCurrentBet - player.betAmount);
                                hasRaised = true;
                                break;
                            default :
                                this.makeBet(player, playerPanel, this.maximumCurrentBet - player.betAmount);
                        }
                        break;
                }
            } else {
                switch (true) {
                    case (points > 23):
                    case (points < -23):
                        this.foldPlayer(player, playerPanel);
                        break;
                    case (points >= -17 && points <=17):
                        var random = Math.random();
                        switch (this.raises) {
                            case 0:
                                if (random < 0.5) {
                                    this.makeBet(player, playerPanel, this.maximumCurrentBet - player.betAmount);
                                } else if (random > 0.7) {
                                    this.makeBet(player, playerPanel, Math.floor((.33 + (Math.random() *.17)) * this.mainPot) - player.betAmount);
                                    hasRaised = true;
                                } else {
                                    this.makeBet(player, playerPanel, Math.floor((.5 + (Math.random() *.16)) * this.mainPot) - player.betAmount);
                                    hasRaised = true;
                                }
                                break;
                            case 1:
                                if (this.maximumCurrentBet < 0.5 * this.mainPot) {
                                    if (random < 0.7) {
                                        this.makeBet(player, playerPanel, this.maximumCurrentBet - player.betAmount);
                                    } else {
                                        this.foldPlayer(player, playerPanel);
                                    }
                                } else if (this.maximumCurrentBet > this.mainPot) {
                                    this.foldPlayer(player, playerPanel);
                                } else {
                                    if (random < 0.5) {
                                        this.makeBet(player, playerPanel, this.maximumCurrentBet - player.betAmount);
                                    } else {
                                        this.foldPlayer(player, playerPanel);
                                    }
                                }
                                break;
                            default :
                                this.foldPlayer(player, playerPanel);
                        }
                        break;
                    case (points >= 18 && points <= 19):
                    case (points >= -19 && points <= -18):
                        switch (this.raises) {
                            case 0:
                                this.makeBet(player, playerPanel, Math.floor((.33 + (Math.random() *.17)) * this.mainPot) - player.betAmount);
                                hasRaised = true;
                                break;
                            case 1:
                                if (this.maximumCurrentBet < 0.66 * this.mainPot) {
                                    this.makeBet(player, playerPanel, this.maximumCurrentBet - player.betAmount);
                                } else {
                                    this.foldPlayer(player, playerPanel);
                                }
                                break;
                            default :
                                this.foldPlayer(player, playerPanel);
                                break;
                        }
                        break;
                    case (points >= 20 && points <= 22):
                    case (points >= -22 && points <= -20):
                        switch (this.raises) {
                            case 0:
                                this.makeBet(player, playerPanel, Math.floor((.5 + (Math.random() *.16)) * this.mainPot) - player.betAmount);
                                hasRaised = true;
                                break;
                            default :
                                this.makeBet(player, playerPanel, this.maximumCurrentBet - player.betAmount);
                                break;
                        }
                        break;
                    case (points === 23 || points === -23):
                        switch (this.raises) {
                            case 0:
                                this.makeBet(player, playerPanel, this.mainPot - player.betAmount);
                                hasRaised = true;
                                break;
                            case 1:
                                this.makeBet(player, playerPanel, 3 * this.maximumCurrentBet - player.betAmount);
                                hasRaised = true;
                                break;
                            case  2:
                                this.makeBet(player, playerPanel, 2 * this.maximumCurrentBet - player.betAmount);
                                hasRaised = true;
                            default :
                                this.makeBet(player, playerPanel, this.maximumCurrentBet - player.betAmount);
                        }
                        break;
                }
            }

            if (hasRaised) {
                this.lastTurn = this.playerTurn - 1;
                this.lastTurn += 6;
                this.lastTurn %= 6;
                this.raises++;
            }

            if (this.foldedPlayers === this.playerCount -1) {
                this.calculateWinner();
                return;
            }

            this.endTurn();
        },

        aiTrading: function () {
            var player = this.players [this.playerTurn];
            var playerPanel = this.playerPanels[this.playerTurn];
            var points = player.totalPoints;
            var diffPoints = 0;
            if (points > 0) {
                diffPoints = points - 19;
            } else {
                diffPoints = points + 19;
            }

            var tradePosition = null;

            switch (true) {
                case (points > 23):
                case (points < -28):
                case (points >= -18 && points <= 15):
                    tradePosition = 0;
                    for (var i = 1; i < player.cards.length; i++) {
                        if (Math.abs(diffPoints - player.cards[tradePosition].points) > Math.abs(diffPoints - player.cards[i].points)) {
                            tradePosition = i;
                        }
                    }
                    break;
                case (points >= -28 && points <= -24):
                    if (player.cards.length >= 5) {
                        tradePosition = 0;
                        for (var i = 1; i < player.cards.length; i++) {
                            if (Math.abs(diffPoints - player.cards[tradePosition].points) > Math.abs(diffPoints - player.cards[i].points)) {
                                tradePosition = i;
                            }
                        }
                    }
                    break;
                case (points >= 16 && points <= 18):
                    if (player.cards.length >= 5) {
                        tradePosition = 0;
                        for (var i = 1; i < player.cards.length; i++) {
                            if (Math.abs(player.cards[tradePosition].points) > Math.abs(player.cards[i].points)) {
                                tradePosition = i;
                            }
                        }
                    }
                    break;
                case (points >= 19 && points <= 21):
                case (points >= -21 && points <= -19):
                    if (Math.random() < 0.5) {
                        tradePosition = "aldreaaned";
                    } else {
                        tradePosition = "stand";
                    }
                    break;
                case (points >= 22 && points <= 23):
                case (points >= -23 && points <= -22):
                    tradePosition = "aldreaaned";
                    break;
            }

            if (tradePosition == "aldreaaned" || this.aiAlderaan === this.playerTurn) {
                if (!this.alderaaned && this.phasesCount > 1) {
                    this.alderaaned = true;
                    this.switchOffGameLights();
                    this.alderaanedPlayer = this.playerTurn;
                    console.log("AI Alderaaned");
                    SB.SFX.alderaan.play();
                } else {
                    console.log("AI Stand");
                    SB.SFX.check.play();
                }
            } else if (tradePosition == "stand") {
                console.log("AI Stand");
                SB.SFX.check.play();
            } else if (tradePosition === null) {
                player.getCard(this.currentDeck.dealCard());
                this.animateCard(function() {}, this.playerTurn);
            } else {
                player.giveCard(tradePosition);
                playerPanel.updateCards();
                player.getCard(this.currentDeck.dealCard());
                this.tradeCardAnimation(function() {}, this.playerTurn);
            }

           this.endTurn();
        },

        makeBet: function (player, playerPanel, amount) {
            console.log(amount + "   " + this.maximumCurrentBet);
            if (amount < player.money) {
                player.putBet(amount);
                this.maximumCurrentBet = player.betAmount;
            } else {
                amount = player.money;
                player.putBet(amount);
                player.allIn = true;
                if (this.maximumCurrentBet <= player.betAmount) {
                    this.maximumCurrentBet = player.betAmount;
                }
                this.allinedPlayers++;
            }
            console.log(amount + "   " + this.maximumCurrentBet);
            playerPanel.updateMoney();
            if (amount !== 0) {
                playerPanel.chipToPile(amount, (this.maximumCurrentBet !== player.betAmount) ? SB.SFX.raise : SB.SFX.bet);
            } else {
                SB.SFX.check.play();
            }
        },

        checkForSpecialCards: function(cards) {
            var cardList = {};
            var idiot = 0, two = 0, three = 0, queen = 0, end = 0, master = 0, mistress = 0, evil = 0, commander = 0;
            if (cards.length > 3) {
                return 0;
            }
            for (var i = 0; i < cards.length; i++) {
                switch (cards[i].value) {
                    case "I": idiot++; break;
                    case "2": two++; break;
                    case "3": three++; break;
                    case "Q": queen++; break;
                    case "E": end++; break;
                    case "MA": master++; break;
                    case "MI": mistress++; break;
                    case "Ev": evil++; break;
                    case "C": commander++; break;
                    default : return 0;
                }
            }

            if (idiot === 1 && two === 1 && three === 1) {
                // Idiot's Array
                return 23.5;
            } else if (queen === 2 && cards.length === 2) {
                // Fairy Empress
                return 22.5;
            } else if (end === 1 && master === 1 && mistress === 1) {
                // Longing Hearts
                return 21.5;
            } else if (commander === 1 && evil === 1 && mistress === 1) {
                // Temptation
                return 20.5;
            } else {
                return 0;
            }
        },

        checkForSpecialCardsNames: function(cards) {
            var idiot = 0, two = 0, three = 0, queen = 0, end = 0, master = 0, mistress = 0, evil = 0, commander = 0;
            if (cards.length > 3) {
                return 0;
            }
            for (var i = 0; i < cards.length; i++) {
                switch (cards[i].value) {
                    case "I": idiot++; break;
                    case "2": two++; break;
                    case "3": three++; break;
                    case "Q": queen++; break;
                    case "E": end++; break;
                    case "MA": master++; break;
                    case "MI": mistress++; break;
                    case "Ev": evil++; break;
                    case "C": commander++; break;
                    default : return 0;
                }
            }

            if (idiot === 1 && two === 1 && three === 1) {
                return "Idiot's Array";
            } else if (queen === 2 && cards.length === 2) {
                return "Fairy Empress";
            } else if (end === 1 && master === 1 && mistress === 1) {
                return "Longing Hearts";
            } else if (commander === 1 && evil === 1 && mistress === 1) {
                return "Temptation";
            } else {
                return 0;
            }
        },

        calculateWinner: function() {
            console.log("Calculating Winner.....");

            this.shiftTimer.stop();
            var winners = [];
            var bombouts = [];
            var openHands = [];

            // Take all bets and put them in main pot
            var foldedChips = 0;
            for (var i = 0; i < 6; i++) {
                if (this.players[i]) {
                    this.mainPot += this.players[i].betAmount;
                    foldedChips += this.players[i].betAmount;
                    this.players[i].resetForPhase();
                }
            }

            var foldWin = false;

            if (this.foldedPlayers === this.playerCount - 1) {
                foldWin = true;
                for (var k = 0; k < 6; k++) {
                    var player = this.players [k];
                    if (player && !player.folded) {
                        if(this.subPots.length !== 0) {
                            this.subPots[this.subPots.length - 1].amount += foldedChips;
                            this.subPots[this.subPots.length - 1].winners = [player.position];
                        }
                        else
                            this.subPots.push({amount: foldedChips, players: [player.position], winners: [player.position]});
                        break;
                    }
                }
            } else
                this.subPots[this.subPots.length - 1].amount += foldedChips;


            console.log(this.subPots);

            if (!foldWin) {
                for (var i = 0; i < 6; i++) {
                    var player = this.players[i];
                    if (player) {
                        if (player.folded)
                            player.totalPoints = 0;
                        else {
                            if (Math.abs(player.totalPoints) > 23 || player.totalPoints === 0) {
                                player.totalPoints = 0;
                                player.folded = true;
                                bombouts.push(player);
                            } else {
                                var specialPoints = this.checkForSpecialCards(player.cards.concat(player.interferenceCards));
                                if (specialPoints !== 0) { player.totalPoints = specialPoints }
                            }
                        }
                    }
                }

                var playerRanks = [].concat(this.players);

                playerRanks.sort(function(a,b) {
                    if (!a) {
                        return 1;
                    }
                    if (!b) {
                        return -1;
                    }
                    if (Math.abs(a.totalPoints) < Math.abs(b.totalPoints)) {
                        return 1;
                    } else if (Math.abs(a.totalPoints) > Math.abs(b.totalPoints)) {
                        return -1;
                    } else if (a.totalPoints < b.totalPoints) {
                        return -1;
                    } else {
                        return 1;
                    }
                });

                // Check for sudden demise and do it
                var highestPoints = playerRanks[0].totalPoints;
                var demisePlayers = [];
                for (var i = 0; i < 6; i++) {
                    if (playerRanks[i] && playerRanks[i].totalPoints === highestPoints) {
                        demisePlayers.push(playerRanks[i]);
                    }
                }

                var afterDemise = false;

                if (demisePlayers.length > 1 && highestPoints !== 0) {
                    for (var j = 0; j < demisePlayers.length; j++) {
                        demisePlayers[j].getCard(this.currentDeck.dealCard());
                        if (Math.abs(demisePlayers[j].totalPoints) > 23 || demisePlayers[j].totalPoints === 0) {
                            demisePlayers[j].totalPoints = 0;
                        } else {
                            var specialPoints = this.checkForSpecialCards(demisePlayers[j].cards.concat(demisePlayers[j].interferenceCards));
                            if (specialPoints !== 0) { demisePlayers[j].totalPoints = specialPoints }
                        }
                        openHands.push({playerPosition: demisePlayers[j].position, playerCards: demisePlayers[j].cards});
                    }
                    afterDemise = true;
                    playerRanks.sort(function(a,b) {
                        if (!a) {
                            return 1;
                        }
                        if (!b) {
                            return -1;
                        }
                        if (Math.abs(a.totalPoints) < Math.abs(b.totalPoints)) {
                            return 1;
                        } else if (Math.abs(a.totalPoints) > Math.abs(b.totalPoints)) {
                            return -1;
                        } else if (a.totalPoints < b.totalPoints) {
                            return -1;
                        } else {
                            return 1;
                        }
                    });
                }

                for (var i = 0; i < 6; i++) {
                    if (playerRanks[i]) {
                        console.log("Player at: " + playerRanks[i].position);
                    } else {
                        console.log("Empty position");
                    }
                }

                var alderaanBombout = true;

                for (var i = 0; i < 6; i++) {
                    if (playerRanks[i] && playerRanks[i].totalPoints !== 0) {
                        for (var j = 0; j < this.subPots.length; j++) {
                            if (!this.subPots[j].winners) {
                                this.subPots[j].winners = [];
                            }
                            if (this.subPots[j].players.indexOf(playerRanks[i].position) !== -1) {
                                if (this.subPots[j].winners.length === 0) {
                                    this.subPots[j].points = playerRanks[i].totalPoints;
                                    this.subPots[j].winners.push(playerRanks[i].position);
                                    if (playerRanks[i].position === this.alderaanedPlayer) {
                                        alderaanBombout = false;
                                    }
                                } else if (this.players[this.subPots[j].winners[0]].totalPoints === playerRanks[i].totalPoints) {
                                    this.subPots[j].winners.push(playerRanks[i].position);
                                    if (playerRanks[i].position === this.alderaanedPlayer) {
                                        alderaanBombout = false;
                                    }
                                }
                            }
                        }
                    }
                }

                if (alderaanBombout && this.alderaanedPlayer !== -1) {
                    if (bombouts.indexOf(this.players[this.alderaanedPlayer]) === -1)
                        bombouts.push(this.players[this.alderaanedPlayer]);
                }

                for (var j = 0; j < bombouts.length; j++) {
                    if (bombouts[j]) {
                        this.sabbacPot += Math.ceil(Math.min(bombouts[j].money, Math.ceil(this.mainPot / 10)));
                        bombouts[j].takeAnte(Math.min(bombouts[j].money, Math.ceil(this.mainPot / 10)));
                    }
                }

                for (var j = 0; j < this.subPots.length; j++) {
                    if (Math.abs(this.subPots[j].points) >= 23) {
                        this.subPots[j].amount += this.sabbacPot;
                        this.sabbacPot = 0;
                    }
                }
                console.log(this.subPots);
            }


            // Finding out hands to open
            var bestValue = 0;
            if (!foldWin) {
                for (var i = 0; i < 6; i++) {
                    var playerPosition = (i + this.dealer + 1) % 6;
                    var player = this.players [playerPosition];

                    if (player) {
                        // Checking Folded players
                        if (player.folded || player.totalPoints === 0) {
                            continue;
                        }

                        // Checking if better score

                        if (Math.abs(bestValue) < Math.abs(player.totalPoints)) {
                            bestValue = player.totalPoints;
                            openHands.push({playerPosition: playerPosition, playerCards: player.cards});
                            continue;
                        }

                        // Checking if same score
                        if (Math.abs(bestValue) === Math.abs(player.totalPoints)) {
                            if (player.totalPoints < 0 && bestValue > 0) {
                                bestValue = player.totalPoints;
                                openHands.push({playerPosition: playerPosition, playerCards: player.cards});
                            } else if (player.totalPoints == bestValue) {
                                bestValue = player.totalPoints;
                                openHands.push({playerPosition: playerPosition, playerCards: player.cards});
                            }
                        }
                    }
                }
            }

            for (var i = 0; i < this.subPots.length; i++) {
                if (!this.subPots[i].winners) {
                    this.subPots[i].winners = [];
                }
            }

            for (var i = 0; i < this.subPots.length; i++) {
                for (var j = 0; j < this.subPots[i].winners.length; j++) {
                    this.players[this.subPots[i].winners[j]].getMoney(Math.floor(this.subPots[i].amount / this.subPots[i].winners.length));
                }
                if (this.subPots[i].winners.length === 0)
                    this.sabbacPot += this.subPots[i].amount;
            }

            var dataBombouts = [];

            for (var i = 0; i < bombouts.length; i++) {
                if (bombouts[i])
                    dataBombouts.push({position: bombouts[i].position});
            };

            var data = {
                subPots: this.subPots,
                bombouts: dataBombouts,
                sabbacPot: this.sabbacPot,
                openHands: openHands,
                afterDemise: afterDemise,
                foldWin: foldWin
            };

            this.showWinner(data);

            this.resetTimer = setTimeout(this.resetPlayers.bind(this), 8000);
        },

        showWinner: function (data) {
            console.log("Showing winner");
            var playerPanel = this.playerPanels[this.playerTurn];
            if (this.tween) {
                this.tween.stop();
                SB.game.tweens.remove(this.tween);
                this.tween = null;
                playerPanel.timer.drawTimer(0);
            }

            if (this.turnTimer) {
                this.turnTimer.destroy();
                this.turnTimer = null;
            }

            this.actionPanel.hidePanel();
            this.bettingPanel.hidePanel();
            var subPots = data.subPots;
            var bombouts = data.bombouts;
            var openHands = data.openHands;
            var winnerDelay = (data.foldWin) ? 0 : 1500;
            this.sabbacPot = data.sabbacPot;

            // Take all bets and put them in main pot
            for (var i = 0; i < 6; i++) {
                if (this.players[i]) {
                    this.players[i].resetForPhase();
                    this.playerPanels[i].showBet();
                }
            }

            this.updateMainPot();

            setTimeout(function() {
                if (!data.afterDemise) {
                    for (var i = 0; i < bombouts.length; i++) {
                        var actualPos = bombouts[i].position, tablePos = bombouts[i].position;

                        this.playerPanels[tablePos].playAnimation("bomb-out");
                        this.playerPanels [tablePos].bomboutPay(Math.min(this.players [actualPos].money, Math.ceil(this.mainPot / 10)));
                        this.players [actualPos].takeAnte(Math.min(this.players [actualPos].money, Math.ceil(this.mainPot / 10)));
                        this.playerPanels [tablePos].updateMoney();
                    }
                }

                if (data.afterDemise) {
                    this.suddenDemiseMsg.visible = true;
                    this.diamond.visible = false;
                }

                for (var i = 0; i < openHands.length; i++) {
                    // Temporary remove card for demised player so that card doesnt immediately show up
                    if (this.demisedPlayers && this.demisedPlayers.indexOf(openHands[i].playerPosition) !== -1) {
                        var card = this.players[openHands[i].playerPosition].removeCard();
                    }
                    this.playerPanels[openHands[i].playerPosition].updateCards();
                    this.playerPanels[openHands[i].playerPosition].faceUpCards();

                    if (this.demisedPlayers && this.demisedPlayers.indexOf(openHands[i].playerPosition) !== -1) {
                        this.players[openHands[i].playerPosition].getCard(card);
                    }
                }

                var i = 0;

                var suddenDemiseDraw = function() {
                    if (i < openHands.length) {
                        if (this.demisedPlayers && this.demisedPlayers.indexOf(openHands[i].playerPosition) !== -1) {
                            this.animateCard(function() {
                                this.playerPanels[openHands[i].playerPosition].faceUpCards();
                                i++;
                                setTimeout(suddenDemiseDraw, 1000);
                            }.bind(this), openHands[i].playerPosition);
                        } else {
                            i++;
                            suddenDemiseDraw();
                        }
                    } else {
                        endRound();
                    }
                }.bind(this);

                var endRound = function() {
                    if (data.afterDemise) {
                        for (var i = 0; i < bombouts.length; i++) {
                            var actualPos = bombouts[i].position, tablePos = bombouts[i].position;

                            this.playerPanels[tablePos].playAnimation("bomb-out");
                            this.playerPanels [tablePos].bomboutPay(Math.min(this.players [actualPos].money, Math.ceil(this.mainPot / 10)));
                            this.players [actualPos].takeAnte(Math.min(this.players [actualPos].money, Math.ceil(this.mainPot / 10)));
                            this.playerPanels [tablePos].updateMoney();
                        }
                    }
                    for (var i = 0; i < subPots.length; i++) {
                        for (var j = 0; j < subPots[i].winners.length; j++) {
                            var actualPos = subPots[i].winners[j], tablePos = subPots[i].winners[j];
                            var prizeMoney  = Math.floor(subPots[i].amount / subPots[i].winners.length);
                            var playerPanel = this.playerPanels[tablePos];

                            var mainPotTimeOut = (bombouts.length === 0) ? 0 : 2250;
                            mainPotTimeOut += i * 750;
                            setTimeout(function(prizeMoney) {
                                this.updateMoney();
                                this.mainToPlayer(prizeMoney);
                            }.bind(playerPanel, prizeMoney), mainPotTimeOut);
                            playerPanel.showWinnerStatus();
                            if (Math.abs(subPots[i].points) >= 23) {
                                setTimeout(function(prizeMoney) {
                                    this.updateMoney();
                                    this.sabaacToPlayer(prizeMoney);
                                }.bind(playerPanel, prizeMoney), mainPotTimeOut);
                            }
                        }

                        if (subPots[i].winners.length === 0) {
                            setTimeout(function(prizeMoney) {
                                this.updateMoney();
                                this.mainToSabaac(prizeMoney);
                            }.bind(this.playerPanels[0], subPots[i].amount), mainPotTimeOut);
                        }

                        setTimeout(function(i) {
                            this.mainPot -= subPots[i].amount;
                            if (this.mainPot <= 0)
                                this.mainPot = 0;
                            this.updateMainPot();
                        }.bind(this, i), mainPotTimeOut);
                        if (Math.abs(subPots[i].points) >= 23) {
                            this.sabaacMsg.visible = true;
                            this.diamond.visible = false;
                            setTimeout(function() {
                                this.updateSabbacPot();
                            }.bind(this), mainPotTimeOut);
                        } else {
                            this.updateSabbacPot();
                        }

                    }

                    if (Math.abs(this.players[0].totalPoints) > 23.5 || Math.abs(this.players[0].totalPoints) === 0) {
                        SB.SFX.bombout.play();
                    } else if (Math.abs(this.players[0].totalPoints) >= 23 && !data.foldWin) {
                        SB.SFX.sabaac.play();
                    }

                    var msg = "", lines = 1;;

                    if (data.foldWin) {
                        msg += this.players[subPots[0].winners[0]].name + " wins " + prizeMoney + " credits."
                    } else {
                        for (var i = 0; i < subPots.length; i++) {
                            if (subPots[i].winners.length > 0) {
                                var playerNames = this.players[subPots[i].winners[0]].name;
                                for (var k = 1; k < subPots[i].winners.length; k++) {
                                    playerNames += " and " + this.players[subPots[i].winners[k]].name;
                                }
                                playerNames += (subPots[i].winners.length > 1) ? " splits " : " wins ";
                                if (msg != "") {
                                    msg += "\n";
                                    lines++;
                                }
                                var pointMsg;
                                switch (subPots[i].points) {
                                    case 23.5:
                                        pointMsg = "Idiot's Array";
                                        break;
                                    case 22.5:
                                        pointMsg = "Fairy Empress";
                                        break;
                                    case 21.5:
                                        pointMsg = "Longing Hearts";
                                        break;
                                    case 20.5:
                                        pointMsg = "Temptation";
                                        break;
                                    default :
                                        pointMsg = subPots[i].points + "";
                                }
                                msg += playerNames + subPots[i].amount + " credits with a hand of " + pointMsg + ".";
                            }
                        }
                    }

                    if (msg == "") {
                        msg += "Everyone bombed out"
                    }

                    this.messageBox.show(msg, lines);
                }.bind(this);

                suddenDemiseDraw();
            }.bind(this), winnerDelay)
        },

        suddenDemise: function (players) {
            this.demisedPlayers = []
            for (var i = 0; i < players.length; i++) {
                players[i].getCard(this.currentDeck.dealCard());
                this.demisedPlayers.push(players[i].position)
            }

            this.calculateWinner(true);
        },

        startSabaacShiftTimer: function() {
            if (this.shiftTimer) {
                this.shiftTimer.stop();
                this.shiftTimer.destroy();
                this.shiftTimer = null;
            }

            this.shiftTimer = SB.game.time.create(true);

            this.shiftTimer.add(30000 + Math.random() * 90000, this.sabaacShift, this);
            this.shiftTimer.start();
        },

        sabaacShift: function() {
            SB.SFX.shift.play();
            this.startSabaacShiftTimer();

            var cardsToShift = Math.ceil(Math.random() * 4) * 2;
            var playerCardsToShift = Math.ceil(Math.random() * cardsToShift / 2);
            var playerCards = [], playerCardPositions = [];
            for (var i = 0; i < 6; i++) {
                if (!this.players[i]) {
                    continue;
                }
                if (this.players[i].folded) {
                    continue;
                }
                for (var j = 0; j < this.players[i].cards.length; j++) {
                    playerCards.push(this.players[i].cards[j]);
                    playerCardPositions.push({ player: i, position: j, list: "cards" });
                }
                for (var k = 0; k < this.players[i].interferenceCards.length; k++) {
                    playerCards.push(this.players[i].interferenceCards[k]);
                    playerCardPositions.push({ player: i, position: k, list: "interferenceCards" });
                }
            }

            var selectedCards = [], selectedPositions = [];

            for (var i = 0; i < playerCards.length; i++) {
                selectedPositions.push(i);
            }

            selectedPositions = _.sample(selectedPositions, playerCardsToShift);

            for (var i = 0; i < selectedPositions.length; i++) {
                var position = selectedPositions[i];
                var card1 = playerCards[position];
                if (card1.isInterferenced) {
                    continue;
                }
                if (Math.random() < 0.5 && i < selectedPositions.length - 1) {
                    var position2 = selectedPositions[i + 1];
                    var card2 = playerCards[position2];
                    this.players[playerCardPositions[position].player].tradeCard(card2, playerCardPositions[position].position);
                    this.players[playerCardPositions[position2].player].tradeCard(card1, playerCardPositions[position2].position);
                    this.playerPanels[playerCardPositions[position].player].updateCards();
                    this.playerPanels[playerCardPositions[position2].player].updateCards();
                    if (playerCardPositions[position].player === 0)
                        this.playerPanels[playerCardPositions[position].player].dissolveAndAppear(playerCardPositions[position].position);
                    if (playerCardPositions[position2].player === 0)
                        this.playerPanels[playerCardPositions[position2].player].dissolveAndAppear(playerCardPositions[position2].position);
                    i++;
                } else {
                    var card2 = this.currentDeck.dealCard();
                    this.players[playerCardPositions[position].player].tradeCard(card2, playerCardPositions[position].position);
                    this.playerPanels[playerCardPositions[position].player].updateCards();
                    if (playerCardPositions[position].player === 0)
                        this.playerPanels[playerCardPositions[position].player].dissolveAndAppear(playerCardPositions[position].position);
                }
            }
        },

        resetPlayers: function() {
            for (var i = 0; i < 6; i++) {
                if (this.players[i]) {
                    this.playerPanels[i].hideCards();
                    this.players[i].resetForRound();
                    this.playerPanels[i].updateCards();
                    this.playerPanels[i].hidePointsPanel();
                    this.playerPanels[i].hideWinnerStatus();
                }
            }

            this.playerTurn = null;
            this.phasesCount = 1;

            this.maximumCurrentBet = 0;
            this.raises = 0;
            this.lastTurn = 0;
            this.alderaaned = false;
            this.foldedPlayers = 0;
            this.allinedPlayers = 0;
            this.alderaanedPlayer = -1;
            this.subPots = [];
            this.mainPot = 0;
            this.switchOnGameLights();
            this.messageBox.hide();
            this.aiAlderaan = -2;
            this.suddenDemiseMsg.visible = false;
            this.sabaacMsg.visible = false;
            this.diamond.visible = true;

            for (var i = 0; i < 6; i++) {
                if (this.players[i]) {
                    // 1 to bet atleast something
                    if (this.players[i].money < SB.Globals.ante + 1) {
                        this.players[i] = null;
                        this.playerPanels[i].hide();
                        this.playerCount--;
                    }
                }
            }
            if (this.players[0] === null) {
                if (Cocoon.Ad.MoPub) {
                    var loader = new SB.Loading();
                    loader.show();
                    SB.ads.loadAds("interstitial", function () {
                        loader.hide();
                        this.winnerMsg.show(false);
                    }.bind(this));
                } else {
                    this.winnerMsg.show(false);
                }
            } else if (this.playerCount === 1) {
                this.winnerMsg.show(true);
            } else {
                this.startRound();
            }
        },

        /*
         Design Code starts here
         */

        animateCard: function (callback, playerPosition) {
            var playerPanel = this.playerPanels[playerPosition];
            var pos = {x: 0, y: 0};
            pos.x = playerPanel.handCardsArray[2].worldPosition.x * SB.xScale;
            pos.y = playerPanel.handCardsArray[2].worldPosition.y * SB.yScale;

            var angle = playerPanel.handCards.rotation;

            var card = this.physicalDeck.pop();
            var tween = SB.game.add.tween(card);

            tween.to({ x: pos.x, y: pos.y, rotation: angle }, 250, "Linear", true);

            tween.onComplete.add(function() {
                playerPanel.updateCards();
                if (playerPosition === 0) {
                    playerPanel.faceUpCards();
                }
                callback();
                SB.game.tweens.remove(tween);
                tween = null;
                card.destroy();
            });

            SB.SFX.deal.play();
        },

        tradeCardAnimation: function (callback, playerPosition) {
            var playerPanel = this.playerPanels[playerPosition];
            var pos = {x: 0, y: 0};
            pos.x = playerPanel.handCardsArray[2].worldPosition.x * SB.xScale;
            pos.y = playerPanel.handCardsArray[2].worldPosition.y * SB.yScale;

            var angle = playerPanel.handCards.rotation;

            var card = this.physicalDeck.pop();
            var tween = SB.game.add.tween(card);

            tween.from({ x: pos.x, y: pos.y, rotation: angle }, 190, "Linear", true);

            tween.onComplete.add(function() {
                tween.to({ x: pos.x, y: pos.y, rotation: angle }, 190, "Linear", true); {}
                    tween.onComplete.add(function() {
                        playerPanel.updateCards();
                        if (playerPosition === 0) {
                            playerPanel.faceUpCards();
                        }
                        callback();
                        SB.game.tweens.remove(tween);
                        tween = null;
                        card.destroy();
                    });
            });
            SB.SFX.deal.play();
        },

        createPhysicalDeck: function () {
            // Create Physical Deck
            for (var i = this.physicalDeck.length; i < 72; i++) {
                var card = SB.game.add.sprite(1640 - (i * 0.2), 495 - (i * 0.1), "sprites", "card-back");
                card.scale.set(0.4, 0.4);
                this.physicalDeck.push(card);
            }
        },

        switchOffGameLights: function () {
            if (this.lightTween1) {
                SB.game.tweens.remove(this.lightTween1);
                this.lightTween1 = null;
            }
            if (this.lightTween2) {
                SB.game.tweens.remove(this.lightTween2);
                this.lightTween2 = null;
            }
            this.lightTween1 = SB.game.add.tween(this.light1Crop).to({height: 0}, 1000, "Linear", true);
            this.lightTween2 = SB.game.add.tween(this.light2Crop).to({y: 455}, 1000, "Linear", true);

            this.lightTween1.onUpdateCallback(function(){
                this.gameLights.updateCrop();
                this.gameLights2.updateCrop();
            }.bind(this));
            this.lightTween2.onComplete.add(function() {
                SB.game.tweens.remove(this.lightTween1);
                this.lightTween1 = null;
                SB.game.tweens.remove(this.lightTween2);
                this.lightTween2 = null;
            }.bind(this));
        },

        switchOnGameLights: function () {
            this.light1Crop.height = 456;
            this.light2Crop.y = 0;
            this.gameLights.updateCrop();
            this.gameLights2.updateCrop();
        },

        createEnvironment: function () {
            this.background = SB.game.add.sprite(0, 0, "background");
            this.gameLights = SB.game.add.sprite(17, 70, "game-lights");
            this.gameLights2 = SB.game.add.sprite(17, 981, "game-lights2");
            this.gameLights2.anchor.set(0, 1);

            this.light1Crop = new Phaser.Rectangle(0, 0, 1827, 456);
            this.gameLights.crop(this.light1Crop);

            this.light2Crop = new Phaser.Rectangle(0, 0, 1827, 455);
            this.gameLights2.crop(this.light2Crop);

            this.dealerContainer = SB.game.add.group();

            SB.game.add.button(0, 0, "sprites", this.returnToMainMenu, this, "rtlobby-normal", "rtlobby-normal", "rtlobby-over");

            this.createPhysicalDeck();

            this.sabaacMsg = SB.game.add.sprite(825, 360, "sprites", "sabbac");
            this.diamond = SB.game.add.sprite(835, 380, "sprites", "sabacc-logo");
            this.sabaacMsg.scale.set(0.75, 0.75);
            this.sabaacMsg.visible = false;
            this.suddenDemiseMsg = SB.game.add.sprite(770, 305, "sprites", "sudden-demise");
            this.suddenDemiseMsg.visible = false;

            SB.game.add.sprite(1625, 415, "sprites", "droid");

            this.playerPanels.push (new SB.PlayerPanel(1, this.cardSelectCallback.bind(this)));

            var aiNames = ["R5-C7", "R9-P3", "R6-G9", "R3-S6", "R7-X4"]

            for (var i = 0; i < this.NO_OF_PLAYERS - 1; i++) {
                this.playerPanels.push (new SB.PlayerPanel(i + 2, this.cardSelectCallback.bind(this)));
            }

            this.dealerToken = new SB.DealerToken(this.dealer + 1, this.dealerContainer);

            this.mainPotImage = SB.game.add.group();
            this.mainPotImage.create(0, 0, "sprites", "main-pot");
            this.mainPotText = SB.game.add.text(145, 52, "0");
            this.mainPotChip = SB.game.add.sprite(0, 0, "sprites", "chip1");
            this.mainPotChip.scale.set(0.72, 0.72);

            this.mainPotImage.add(this.mainPotText);
            this.mainPotImage.add(this.mainPotChip);
            this.mainPotImage.position.set(600, 420);

            this.sabbacPotImage = SB.game.add.group();
            this.sabbacPotImage.create(0, 0, "sprites", "sabbac-pot-chip");
            this.sabbacPotText = SB.game.add.text(160, 63, "0");

            this.sabbacPotImage.add(this.sabbacPotText);
            this.sabbacPotImage.position.set(1050, 420);

            this.mainPotText.anchor.set(0.5, 0.5);
            this.mainPotText.font = 'MyFont';
            this.mainPotText.fontWeight = 'bold';
            this.mainPotText.fontSize = 32;
            this.mainPotText.fill = '#FFFFFF';

            this.sabbacPotText.anchor.set(0.5, 0.5);
            this.sabbacPotText.font = 'MyFont';
            this.sabbacPotText.fontWeight = 'bold';
            this.sabbacPotText.fontSize = 32;
            this.sabbacPotText.fill = '#FFFFFF';

            this.bettingPanel = new SB.BettingPanel();
            this.actionPanel = new SB.ActionPanel(this.executeAction.bind(this));
            this.actionPanel.hidePanel();
            this.bettingPanel.hidePanel();

            this.helpMsg = SB.game.add.text(1920 / 2, 800, "Please select a card to protect");
            this.helpMsg.anchor.set(0.5, 0.5);
            this.helpMsg.font = "Arial";
            this.helpMsg.stroke = "#00FFFF";
            this.helpMsg.strokeThickness = 6;
            this.helpMsg.visible = false;

            this.messageBox = new SB.FeedbackObject();
            this.winnerMsg = new SB.WinnerMessage(this.replayGame.bind(this), this.returnToMainMenu.bind(this));

            // Creating SFX
            SB.SFX = {};
            SB.SFX.bet = this.game.add.sound("bet");
            SB.SFX.deal = this.game.add.sound("deal");
            SB.SFX.interference = this.game.add.sound("interference");
            SB.SFX.movechip = this.game.add.sound("movechip");
            SB.SFX.shuffle = this.game.add.sound("shuffle");
            SB.SFX.shift = this.game.add.sound("shift");
            SB.SFX.turn = this.game.add.sound("turn");
            SB.SFX.winchip = this.game.add.sound("winchip");
            SB.SFX.sabaac = this.game.add.sound("sabaac");
            SB.SFX.bombout = this.game.add.sound("bombout");

            SB.SFX.alderaan = this.game.add.sound("alderaan");
            SB.SFX.check = this.game.add.sound("check");
            SB.SFX.fold = this.game.add.sound("fold");
            SB.SFX.raise = this.game.add.sound("raise");
            SB.SFX.timer = this.game.add.sound("timer");
        },

        updateSabbacPot: function () {
            this.sabbacPotText.text = this.sabbacPot;
        },

        updateMainPot: function () {
            this.mainPotText.text = this.mainPot;
            for (var i = 0; i < 6; i++) {
                if (this.players[i] && this.players[i].betAmount !== 0) {
                    this.playerPanels[i].pileToMainPot(this.players[i].betAmount);
                }
            }

            setTimeout(function() {
                this.mainPotChip.frameName = "chip" + SB.utils.getChipColor (this.mainPot) + "";
            }.bind(this), 500);
        },

        returnToMainMenu: function() {
            document.removeEventListener("backbutton", this.returnToMainMenu.bind(this), false);

            if (this.turnTimer)
                this.turnTimer.stop();
            if (this.shiftTimer)
                this.shiftTimer.stop();

            if (this.tween) {
                this.tween.stop();
                SB.game.tweens.remove(this.tween);
                this.tween = null;
            }

            var id = setTimeout(function() {}, 0);

            while (id--) {
                clearTimeout(id); // will do nothing if no timeout with id is present
            }

            if (Cocoon.Ad.MoPub) {
                var loader = new SB.Loading();
                loader.show();
                SB.ads.loadAds("interstitial", function () {
                    this.state.start('Menu');
                }.bind(this));
            } else {
                this.state.start('Menu');
            }
        },

        replayGame: function() {
            var id = setTimeout(function() {}, 0);

            while (id--) {
                clearTimeout(id); // will do nothing if no timeout with id is present
            }
            this.state.start('SingleGame');
        }
    };

    return SB.SingleState;
});

/**
 * Deck Object containing 76 cards deck.
 * Card Type Reference:
 *          Suits: St (Staves), Fl (Flasks), Co (Coins), Sb (Sabres)
 *          Suit Card Values : 1-11, C (Commander), MI (Mistress), MA (Master), A (Ace)
 *          Ranked Cards: I (Idiot), Q (Queen of Air and Darkness), E (Endurance), B (Balance), D (Demise), M (Moderation), Ev (The Evil One), S (The Star)
 */

SB.PlayerSingle = function (isAI, position, name) {
    this.name = name;
    this.position = position;
    this.isAI = isAI || false;
    this.cards = new Array ();
    this.interferenceCards = [];
    this.totalPoints = 0;
    this.betAmount = 0;
    this.money = 10000;
    this.folded = false;
    this.cardSelectWaitingFor = "";
    this.allIn = false;
};

SB.PlayerSingle.prototype = {
    takeAnte: function (amount) {
        this.money = this.money - amount;
    },

    putBet: function (amount) {
        this.money -= amount;
        this.betAmount += amount;
    },

    getCard: function(card) {
        this.cards.push(card);
        this.totalPoints += card.points;
    },

    removeCard: function() {
        var card = this.cards.pop();
        this.totalPoints -= card.points;

        return card;
    },

    tradeCard: function(newCard, oldCardPosition) {
        this.totalPoints -= this.cards[oldCardPosition].points;
        this.cards[oldCardPosition] = newCard;
        this.totalPoints += newCard.points;
    },

    giveCard: function(oldCardPosition) {
        this.totalPoints -= this.cards[oldCardPosition].points;
        this.cards.splice(oldCardPosition, 1);
    },

    resetForPhase: function() {
        this.betAmount = 0;
    },

    resetForRound: function() {
        this.totalPoints = 0;
        this.betAmount = 0;
        this.folded = false;
        this.cards = [];
        this.interferenceCards = [];
        this.cardSelectWaitingFor = "";
    },

    getMoney: function (amount) {
        this.money += amount;
    },

    protectCard: function (oldCardPosition) {
        var card = this.cards.splice(oldCardPosition, 1)[0];
        card.isInterferenced = true;
        this.interferenceCards.push(card);
    }
}

SB.Deck = function () {
    this.cards = new Array ();
};

SB.Deck.prototype = {
    makeDeck: function () {
        var values = new Array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "C", "MI", "MA", "A");
        var points = new Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
        var suits = new Array("Sb", "St", "Fl", "Co");
        var rankCards = new Array("I", "Q", "E", "B", "D", "M", "Ev", "S");
        var rankValues = new Array(0, -2, -8, -11, -13, -14, -15, -17);
        var i, j;
        var n;

        n = values.length * suits.length;

        // Set array of cards.

        this.cards = new Array();

        // Fill the array with 'n' packs of cards.

        // Fill in suit cards
        for (i = 0; i < suits.length; i++) {
            for (j = 0; j < values.length; j++) {
                var card = new SB.Card(values[j], suits[i], points[j]);
                this.cards.push(card);
                card.cardToString();
            }
        }

        // Fill in ranked cards
        for (i = 0; i < rankCards.length; i++) {
            for (j = 0; j < 2; j++) {
                var card = new SB.Card(rankCards[i], "Rk", rankValues[i]);
                this.cards.push(card);
                card.cardToString();
            }
        }
    },

    shuffle: function () {
        var i, j, k;
        var temp;

        // Shuffle the stack '3' times.

        for (i = 0; i < 3; i++) {
            for (j = 0; j < this.cards.length; j++) {
                k = Math.floor(Math.random() * this.cards.length);
                temp = this.cards[j];
                this.cards[j] = this.cards[k];
                this.cards[k] = temp;
            }
        }
    },

    dealCard: function () {
        return this.cards.shift();
    }
}



