define([
    'phaser',
    'underscore',
    'js/objects/DealerToken.js',
    'js/objects/FeedbackObject.js',
    'js/objects/Events.js',
    'js/objects/ServerMessage.js',
    'js/objects/PlayerPanel.js',
    'js/objects/ActionPanel.js',
    'js/objects/BettingPanel.js',
    'js/objects/Card.js',
    'js/objects/BuyinBox.js',
], function (
    Phaser,
    UnderScore,
    DealerToken,
    Events,
    ServerMessage,
    PlayerPanel,
    BettingPanel,
    ActionPanel,
    Card,
    BuyinBox
) {
    'use strict';

    SB.MultiState = function() {};

    SB.MultiState.prototype = {
        currentDeck: null,
        players: [],
        playerPanels: [],
        gamePhase: SB.Constants.WAITING,
        mainPot: 0,
        sabbacPot: 0,
        callAll: false,
        checkFold: false,
        dealer: 2,
        playerTurn: null,
        phasesCount: 1,
        folderPlayers: 0,
        maximumCurrentBet: 0,
        raises: 0,
        lastTurn: 0,
        alderaaned: false,
        playerCount: 6,
        physicalDeck: [],
        alderaanedPlayer: -1,
        foldedPlayers: 0,
        turnTimer: null,
        playerPosition: 0,
        playerWaiting: true,

        preload: function() {

        },

        create: function() {
            this.socket = io.connect('http://sabaccserver-sabaacmulti.rhcloud.com:8000', {'transports': ['websocket', 'polling']});

            document.addEventListener("backbutton", this.returnToLobby.bind(this), false);

            this.stage.disableVisibilityChange = true;

            if (!this.game.device.desktop) {
                Cocoon.App.onSuspended.addEventListener(function () {
                    console.log("suspended");
                });

                CocoonJS.App.onActivated.addEventListener(function () {
                    console.log("activated")
                });
            }

            this.NO_OF_PLAYERS = 6;

            this.setup();

            this.createEnvironment();

            SB.events = new SB.Events;

            this.socket.emit(SB.events.CLIENT_READY, {
                tableNo: SB.tableNo,
                playerName: SB.CurrentUser.username,
                id: SB.CurrentUser.id
            });

            this.socket.on(SB.events.START_GAME, function(msg){
                this.playerPosition = msg.position;
                this.dealer = parseInt(msg.dealer);
                SB.Globals.blind = parseInt(msg.blind);
                SB.Globals.ante = parseInt(msg.ante);

                for (var i = 0; i < msg.players.length; i++) {
                    var player = msg.players[i];
                    if (player) {
                        SB.Globals.buyin = player.money;
                        this.players[player.position] = new SB.Player(player.position, player.name, player.money);
                        this.players[player.position].betAmount = player.betAmount;
                        for (var j = 0; j < player.cards; j++)
                            this.players [player.position].getCard(new SB.Card("0", "0", 0));
                        this.players [player.position].interferenceCards = player.interferenceCards;
                    }
                }

                if (msg.count > 2) {
                    this.serverInfoBox.show("Waiting for next round to start");
                }

                this.dealer = this.calculateTablePosition(this.dealer);
                this.startGame();
            }.bind(this));

            this.socket.on(SB.events.GAME_WAITING, this.gameWaiting.bind(this));
            this.socket.on(SB.events.PLAYER_CONNECTED, this.addPlayer.bind(this));
            this.socket.on(SB.events.PLAYER_DISCONNECTED, this.removePlayer.bind(this));
            this.socket.on(SB.events.START_ROUND, this.startRound.bind(this));
            this.socket.on(SB.events.TAKE_ANTE, this.takeAnte.bind(this));
            this.socket.on(SB.events.TAKE_BET, this.makeBet.bind(this));
            this.socket.on(SB.events.ROUND_DEAL, this.dealCards.bind(this));
            this.socket.on(SB.events.TAKE_TURN, this.takeTurn.bind(this));
            this.socket.on(SB.events.TAKE_ACTION, this.recieveAction.bind(this));
            this.socket.on(SB.events.COLLECT_BETS, this.switchPhase.bind(this));
            this.socket.on(SB.events.SHOW_WINNER, this.calculateWinner.bind(this));
            this.socket.on(SB.events.RESET_PLAYERS, this.resetPlayers.bind(this));
            this.socket.on(SB.events.SABAAC_SHIFT, this.sabaacShift.bind(this));
            this.socket.on(SB.events.FORCE_ALDERAAN, this.forceAlderaan.bind(this));
            this.socket.on(SB.events.UPDATE_PLAYERS, this.updatePlayerMoney.bind(this));
        },

        setup: function() {
            this.currentDeck = null;
            this.players = [];
            this.playerPanels = [];
            this.gamePhase = SB.Constants.WAITING;
            this.mainPot = 0;
            this.sabbacPot = 0;
            this.callAll = false;
            this.checkFold = false;
            this.dealer = 2;
            this.playerTurn = null;
            this.phasesCount = 1;
            this.folderPlayers = 0;
            this.maximumCurrentBet = 0;
            this.raises = 0;
            this.lastTurn = 0;
            this.alderaaned = false;
            this.playerCount = 6;
            this.physicalDeck = [];
            this.alderaanedPlayer = -1;
            this.foldedPlayers = 0;
            this.turnTimer = null;
            this.shiftTimer = null;
            this.playerPosition = 0;
            this.playerWaiting = true;
        },

        update: function () {

        },

        gameWaiting: function() {
            var playerPanel = this.playerPanels[this.playerTurn];
            if (this.turnTimer)
                this.turnTimer.stop();

            if (this.tween) {
                this.tween.stop();
                SB.game.tweens.remove(this.tween);
                this.tween = null;
                playerPanel.timer.drawTimer(0);
            }

            this.serverInfoBox.show("Waiting for more players to join.");
            this.actionPanel.hidePanel();
        },

        addPlayer: function(data) {
            var i = data.position;
            var player = data.player;
            this.players[player.position] = new SB.Player(player.position, player.name, player.money);
            var player = this.players[i];
            var panelPosition = (i + 6 - this.playerPosition) % 6;
            this.playerPanels [panelPosition].assignPlayer(this.players [i]);
            this.playerPanels [panelPosition].updateMoney();
        },

        removePlayer: function(data) {
            var pos = data.position;
            var tablePos = this.calculateTablePosition(pos);

            if (this.players[pos].betAmount > 0) {
                this.playerPanels[tablePos].pileToMainPot(this.players[pos].betAmount);
            }

            this.players[pos] = null;
            this.playerPanels[tablePos].hidePointsPanel();
            this.playerPanels[tablePos].hideWinnerStatus();
            this.playerPanels[tablePos].hide();
        },


        /*
         Game Logic Code starts here
         */

        startGame: function () {
            console.log("Game Started");
            if (this.gamePhase === SB.Constants.WAITING) {
                for (var i = 0; i < 6; i++) {
                    var player = this.players[i];
                    if (player) {
                        var panelPosition = (i + 6 - this.playerPosition) % 6;
                        this.playerPanels [panelPosition].assignPlayer(this.players [i]);
                        this.playerPanels [panelPosition].showBet();
                        this.playerPanels [panelPosition].updateMoney();
                        if (player.cards.length > 0)
                            this.playerPanels [panelPosition].updateCards();
                    }
                }

                this.dealerToken = new SB.DealerToken((this.dealer + 1) % 6, this.tokenContainer);
            }
        },

        startRound: function (dealer) {
            console.log("Starting new round....");

            this.playerWaiting = false;

            this.serverInfoBox.hide();

            for (var i = 0; i < 6; i++) {
                if (this.players[i]) {
                    this.players[i].isWaiting = false;
                }
            }

            this.dealer = this.calculateTablePosition(parseInt(dealer));
            this.dealerToken.shift(this.dealer);
        },

        takeAnte: function (amount) {
            console.log("Taking Ante....");

            this.gamePhase = "Ante";

            for (var i = 0; i < 6; i++) {
                if (this.players[i]) {
                    this.players [i].takeAnte(amount);
                    this.playerPanels [this.calculateTablePosition(i)].updateMoney();
                    this.playerPanels [this.calculateTablePosition(i)].chipToSabacc(amount);
                    this.sabbacPot += SB.Globals.ante;
                }
            }

            // Remove only once for the player himself
            this.removeCredits(amount);

            this.updateSabbacPot();
        },

        dealCards: function (data) {
            console.log("Dealing Cards....");

            this.gamePhase = "Dealing";

            SB.SFX.shuffle.play();
            this.createPhysicalDeck();

            var i = 0, j = 0;

            var callback = function() {
                if (j < 6) {
                    var playerPosition = (j + this.dealer + 1) % 6;
                    var actualPosition = this.calculateActualPosition(playerPosition);

                    j++;
                    if (this.players[actualPosition] && !this.players[actualPosition].isWaiting) {
                        if (playerPosition === 0) {
                            this.players [actualPosition].getCard(new SB.Card(data[i].value, data[i].suit, data[i].points));
                        } else {
                            this.players [actualPosition].getCard(new SB.Card("0", "0", 0));
                        }

                        this.animateCard(callback, playerPosition);
                    } else {
                        callback();
                    }
                } else {
                    i++;
                    if (i < 2) {
                        j = 0;
                        callback();
                    }
                }

            }.bind(this);

            callback();
        },

        takeTurn: function (data) {
            this.playerTurn = this.calculateTablePosition(data.playerTurn);
            this.gamePhase = data.gamePhase;
            this.maximumCurrentBet = data.maximumCurrentBet;
            this.phasesCount = data.phasesCount;
            this.alderaaned = data.alderaaned;

            this.actionPanel.hidePanel();
            this.bettingPanel.hidePanel();

            this.startTurnTime(20000);
            if (this.playerWaiting) {
                return;
            }
            if (this.gamePhase == "Betting") {
                if (this.calculateTablePosition(this.playerPosition) === this.playerTurn) {
                    SB.SFX.turn.play();
                    if (this.callAll) {
                        this.socket.emit(SB.events.RECIEVE_ACTION, {name: "call"});
                        this.actionPanel.hidePanel();
                        return;
                    }

                    if (this.checkFold) {
                        this.socket.emit(SB.events.RECIEVE_ACTION, {name: "checkFold"});
                        this.actionPanel.hidePanel();
                        return;
                    }

                    this.callAll = false;
                    this.checkFold = false;

                    var btnsToShow = this.findBetBtnsToShow();

                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                } else {
                    var player = this.players[this.playerPosition];
                    if (!player.folded) {
                        var btnsToShow = ["checkFoldBtn", "callallBtn"];

                        this.actionPanel.hidePanel();
                        this.actionPanel.showPanel(btnsToShow);

                        if (this.checkFold) {
                            this.actionPanel.checkFoldBtn.toggle();
                        }
                        if (this.callAll) {
                            this.actionPanel.callallBtn.toggle();
                        }
                    }
                }
            } else if (this.gamePhase == "Trading") {
                if (this.calculateTablePosition(this.playerPosition) === this.playerTurn) {
                    SB.SFX.turn.play();
                    var btnsToShow = this.findTradeBtnsToShow();

                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                }
            }
        },

        findMaxValueTobet: function(player) {
            var highestMoney = 0, secondHighest = 0;

            for (var i = 0; i < 6; i++) {
                if (this.players[i] && (this.players[i].money + this.players[i].betAmount) > secondHighest) {
                    if ((this.players[i].money + this.players[i].betAmount) > highestMoney) {
                        secondHighest = highestMoney;
                        highestMoney = (this.players[i].money + this.players[i].betAmount);
                    } else {
                        secondHighest = (this.players[i].money + this.players[i].betAmount);
                    }
                }
            }
            return Math.min((player.money + player.betAmount), secondHighest);
        },

        findBetBtnsToShow: function(dontProtect) {
            // Show Action Panel with actions player can take (note check and call cant come together, all in is always avaliable and raise only if player has more money than current max)
            var btnsToShow = new Array("foldBtn");
            var player = this.players[this.playerPosition];
            if (player.betAmount == this.maximumCurrentBet) {
                btnsToShow.push("checkBtn");
            } else {
                if (player.money >= (this.maximumCurrentBet - player.betAmount)) {
                    btnsToShow.push("callBtn");
                } else {
                    btnsToShow.push("allinBtn");
                }
            }

            if (this.bettingPanel.container.visible) {
                btnsToShow.push("confirmBtn");
            } else {
                if (this.maximumCurrentBet === 0) {
                    btnsToShow.push("betBtn");
                } else if (player.money >= (2 * this.maximumCurrentBet - player.betAmount)) {
                    btnsToShow.push("raiseBtn");
                }
            }

            if (player.interferenceCards.length === 0 && !dontProtect) {
                btnsToShow.push("protectBtn");
            }

            return btnsToShow;
        },

        findTradeBtnsToShow: function (dontProtect) {
            // Show Action Panel with actions player can take during trading phaser, alderaan only avaliable after 2nd betting
            var player = this.players[this.playerPosition];
            var btnsToShow = new Array();
            if (this.phasesCount > 1 && !this.alderaaned) {
                btnsToShow.push("alderaanBtn");
            }
            btnsToShow.push("standBtn", "tradeBtn");

            if (player.interferenceCards.length < 2 && !dontProtect) {
                btnsToShow.push("protectBtn");
            }

            if (player.cards.length + player.interferenceCards.length < 5) {
                btnsToShow.push("drawBtn");
            }

            return btnsToShow;
        },

        switchPhase: function (data) {
            this.callAll = false;
            this.checkFold = false;
            this.gamePhase = data.gamePhase;
            switch(data.gamePhase) {
                case "Alderaan":
                    break;
                case "Betting":
                    this.phasesCount++;
                    this.maximumCurrentBet = 0;
                    break;
                case "Trading":
                    this.mainPot = data.mainPot;
                    this.updateMainPot();
                    for (var i = 0; i < 6; i++) {
                        if (this.players[i]) {
                            this.players[i].resetForPhase();
                            this.playerPanels[this.calculateTablePosition(i)].showBet();
                        }
                    }
            }
        },

        startTurnTime: function(time) {
            var playerPanel = this.playerPanels[this.playerTurn];
            if (this.turnTimer) {
                this.turnTimer.destroy();
                this.turnTimer = null;
            }
            this.turnTimer = SB.game.time.create(true);
            this.turnTimer.add(time, function() {
                SB.SFX.timer.stop();
                if (this.tween) {
                    this.tween.stop();
                    SB.game.tweens.remove(this.tween);
                    this.tween = null;
                    playerPanel.timer.drawTimer(0);
                }
            }, this);
            this.turnTimer.start();

            var temp = { value: 4 };
            this.tween = SB.game.add.tween(temp).to({ value: 0}, 15500, "Linear", true);
            this.tween.onUpdateCallback(function(){
                playerPanel.timer.drawTimer(temp.value);
            }.bind(this));
            this.tween.onComplete.add(function() {
                this.tween.stop();
                SB.game.tweens.remove(this.tween);
                this.tween = null;
                playerPanel.timer.drawTimer(0);
                if (this.playerTurn === 0)
                    SB.SFX.timer.play();
            }.bind(this));

        },

        executeAction: function (btn) {
            var player = this.players[this.calculateActualPosition(this.playerTurn)];
            var playerPanel = this.playerPanels[this.playerTurn];
            switch (btn.name) {
                case "callall":
                    this.callAll = !this.actionPanel.callallBtn.state;
                    if (this.checkFold) {
                        this.checkFold = false;
                        this.actionPanel.checkFoldBtn.toggle();
                    }
                    return;
                case "check-fold":
                    this.checkFold = !this.actionPanel.checkFoldBtn.state;
                    if (this.callAll) {
                        this.callAll = false;
                        this.actionPanel.callallBtn.toggle();
                    }
                    return;
                    break;
                case "bet":
                case "raise":
                    if (this.maximumCurrentBet === 0) {
                        this.bettingPanel.showPanel(Math.min(SB.Globals.blind, player.money), this.findMaxValueTobet(player));
                    } else {
                        this.bettingPanel.showPanel(2 * this.maximumCurrentBet, this.findMaxValueTobet(player));
                    }
                    var btnsToShow = this.findBetBtnsToShow();
                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    return;
                case "protect":
                    player.cardSelectWaitingFor = "protect";
                    var btnsToShow = ["backBtn"];
                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    this.helpMsg.visible = true;
                    this.helpMsg.text = "Select a card to Protect";
                    return;
                case "trade":
                    player.cardSelectWaitingFor = "trade";
                    var btnsToShow = ["backBtn"];
                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    this.helpMsg.visible = true;
                    this.helpMsg.text = "Select a card to Trade";
                    return;
                case "back":
                    if (this.gamePhase == "Betting") {
                        var btnsToShow = this.findBetBtnsToShow();
                    } else {
                        var btnsToShow = this.findTradeBtnsToShow();
                    }
                    this.actionPanel.hidePanel();
                    this.actionPanel.showPanel(btnsToShow);
                    this.helpMsg.visible = false;
                    return;
                case "draw": player.cardSelectWaitingFor = ""; //continue
                case "fold" :
                case "call" :
                case "allin":
                case "check":
                case "stand":
                case "alderaan":
                    this.socket.emit(SB.events.RECIEVE_ACTION, {name: btn.name});
                    break;
                case  "confirm":
                    this.socket.emit(SB.events.RECIEVE_ACTION, {name: btn.name, amount: this.bettingPanel.value});
                    break;
            }

            if (this.turnTimer) {
                this.turnTimer.stop();
                SB.SFX.timer.stop();
            }

            if (this.tween) {
                this.tween.stop();
                SB.game.tweens.remove(this.tween);
                this.tween = null;
                playerPanel.timer.drawTimer(0);
            }

            this.bettingPanel.hidePanel();
            this.actionPanel.hidePanel();
        },

        recieveAction: function(data) {
            var playerPosition = data.playerTurn;
            var player = this.players[playerPosition];
            var playerPanel = this.playerPanels[this.calculateTablePosition(playerPosition)];

            if (data.action !== "protect") {
                if (this.turnTimer)
                    this.turnTimer.stop();

                if (this.tween) {
                    this.tween.stop();
                    SB.game.tweens.remove(this.tween);
                    this.tween = null;
                    playerPanel.timer.drawTimer(0);
                }

            }

            switch (data.action) {
                case "fold":
                    playerPanel.fold();
                    break;
                case "draw":
                    player.getCard(new SB.Card(data.card.value, data.card.suit, data.card.points));
                    this.animateCard(function() {}, this.playerTurn);
                    break;
                case "trade":
                    player.giveCard(data.positionInHand);
                    playerPanel.updateCards();
                    if (this.playerTurn === 0) {
                        playerPanel.faceUpCards();
                    }
                    player.getCard(new SB.Card(data.card.value, data.card.suit, data.card.points));
                    this.tradeCardAnimation(function() {}, this.playerTurn);
                    break;
                case "protect":
                    var card = player.cards[data.positionInHand];
                    card.updateCard(data.card.value, data.card.suit, data.card.points);

                    player.protectCard(data.positionInHand);
                    player.cardSelectWaitingFor = "";
                    playerPanel.updateCards();

                    if (this.playerTurn === 0) {
                        playerPanel.faceUpCards();
                        this.actionPanel.hidePanel();
                        if (this.gamePhase == "Betting") {
                            var btnsToShow = this.findBetBtnsToShow(true);
                        } else {
                            var btnsToShow = this.findTradeBtnsToShow(true);
                        }
                        this.actionPanel.showPanel(btnsToShow);
                    }
                    SB.SFX.interference.play();
                    break;
                case "alderaan":
                    SB.SFX.alderaan.play();
                    this.switchOffGameLights();
                    break;
                case "check":
                case "stand": SB.SFX.check.play();
                case "call":
                    break;
            }
        },

        forceAlderaan: function() {
            SB.SFX.alderaan.play();
            this.switchOffGameLights();
        },

        cardSelectCallback: function(card) {
            var player = this.players[this.calculateActualPosition(this.playerTurn)];
            var playerPanel = this.playerPanels[this.playerTurn];

            switch (player.cardSelectWaitingFor) {
                case "trade" :
                    this.socket.emit(SB.events.RECIEVE_ACTION, { name: "trade", cardPosition: card.positionInHand });
                    player.cardSelectWaitingFor = "";
                    break;
                case "protect":
                    this.socket.emit(SB.events.RECIEVE_ACTION, { name: "protect", cardPosition: card.positionInHand });
                    player.cardSelectWaitingFor = "";
                    break;
            }
            this.helpMsg.visible = false;
        },

        foldPlayer: function(player, playerPanel) {
            if (player.betAmount !== this.maximumCurrentBet) {
                playerPanel.fold();
                this.foldedPlayers++;
            }
        },

        makeBet: function (data) {
            var position, amount, maxBet;
            position = data.position;
            amount = data.amount;
            maxBet = data.maxBet;

            this.players[position].putBet(amount);
            this.playerPanels[this.calculateTablePosition(position)].updateMoney();
            if (amount !== 0) {
                this.playerPanels[this.calculateTablePosition(position)].chipToPile(amount, (this.maximumCurrentBet !== maxBet) ? SB.SFX.raise : SB.SFX.bet);
            }
            this.maximumCurrentBet = maxBet;
            if (this.calculateTablePosition(position) === 0) {
                this.removeCredits(amount);
            }
        },

        checkForSpecialCards: function(cards) {
            var cardList = {};
            var idiot, two, three, queen, end, master, mistress, evil, commander;
            if (cards.length > 3) {
                return 0;
            }
            for (var i = 0; i < cards.length; i++) {
                switch (cards[i].value) {
                    case "I": idiot++; break;
                    case "2": two++; break;
                    case "3": three++; break;
                    case "Q": queen++; break;
                    case "E": end++; break;
                    case "MA": master++; break;
                    case "MI": mistress++; break;
                    case "Ev": evil++; break;
                    case "C": commander++; break;
                    default : return 0;
                }
            }

            if (idiot === 1 && two === 1 && three === 1) {
                // Idiot's Array
                return 23.5;
            } else if (queen === 2 && cards.length === 2) {
                // Fairy Empress
                return 22.5;
            } else if (end === 1 && master === 1, mistress === 1) {
                // Longing Hearts
                return 21.5;
            } else if (commander === 1 && evil === 1 && mistress === 1) {
                // Temptation
                return 20.5;
            } else {
                return 0;
            }
        },

        calculateWinner: function(data) {
            var playerPanel = this.playerPanels[this.playerTurn];
            if (this.tween) {
                this.tween.stop();
                SB.game.tweens.remove(this.tween);
                this.tween = null;
                playerPanel.timer.drawTimer(0);
            }

            this.actionPanel.hidePanel();
            this.bettingPanel.hidePanel();

            var subPots = data.subPots;
            var bombouts = data.bombouts;
            var openHands = data.openHands;
            var winnerDelay = (data.foldWin) ? 750 : 1500;
            this.sabbacPot = data.sabbacPot;

            // Take all bets and put them in main pot
            for (var i = 0; i < 6; i++) {
                if (this.players[i]) {
                    if (this.players[i].betAmount > 0) {
                        this.playerPanels[this.calculateTablePosition(i)].pileToMainPot(this.players[i].betAmount);
                        this.mainPot += this.players[i].betAmount;
                        this.players[i].resetForPhase();
                        this.playerPanels[this.calculateTablePosition(i)].showBet();
                    }
                }
            }

            this.updateMainPot();

            setTimeout(function() {
                if (!data.afterDemise) {
                    for (var i = 0; i < bombouts.length; i++) {
                        var actualPos = bombouts[i].position, tablePos = this.calculateTablePosition(bombouts[i].position);

                        this.playerPanels[tablePos].playAnimation("bomb-out");
                        this.playerPanels [tablePos].bomboutPay(Math.min(this.players [actualPos].money, Math.ceil(this.mainPot / 10)));
                        this.players [actualPos].takeAnte(Math.min(this.players [actualPos].money, Math.ceil(this.mainPot / 10)));
                        this.playerPanels [tablePos].updateMoney();
                        if (tablePos === 0) {
                            this.removeCredits(Math.min(this.players [actualPos].money, Math.ceil(this.mainPot / 10)));
                        }
                    }
                }

                if (data.afterDemise) {
                    this.suddenDemiseMsg.visible = true;
                    this.diamond.visible = false;
                }

                for (var i = 0; i < openHands.length; i++) {
                    this.players[openHands[i].playerPosition].updateHand(openHands[i].playerCards);
                    this.playerPanels[this.calculateTablePosition(openHands[i].playerPosition)].updateCards();
                    this.playerPanels[this.calculateTablePosition(openHands[i].playerPosition)].faceUpCards();
                }

                var i = 0;

                var suddenDemiseDraw = function() {
                    if (i < openHands.length) {
                        if (this.players[openHands[i].playerPosition].cards.length < openHands[i].playerCards.length) {
                            var card = openHands[i].playerCards[openHands[i].playerCards.length - 1];
                            this.players[openHands[i].playerPosition].getCard(new SB.Card(card.value, card.suit, card.points));
                            this.animateCard(function() {
                                this.playerPanels[this.calculateTablePosition(openHands[i].playerPosition)].faceUpCards();
                                i++;
                                setTimeout(suddenDemiseDraw, 1000);
                            }.bind(this), this.calculateTablePosition(openHands[i].playerPosition));
                        } else {
                            i++;
                            suddenDemiseDraw();
                        }
                    } else {
                        endRound();
                    }
                }.bind(this);

                var endRound = function() {
                    if (data.afterDemise) {
                        for (var i = 0; i < bombouts.length; i++) {
                            var actualPos = bombouts[i].position, tablePos = this.calculateTablePosition(bombouts[i].position);

                            this.playerPanels[tablePos].playAnimation("bomb-out");
                            this.playerPanels [tablePos].bomboutPay(Math.min(this.players [actualPos].money, Math.ceil(this.mainPot / 10)));
                            this.players [actualPos].takeAnte(Math.min(this.players [actualPos].money, Math.ceil(this.mainPot / 10)));
                            this.playerPanels [tablePos].updateMoney();
                            if (tablePos === 0) {
                                this.removeCredits(Math.min(this.players [actualPos].money, Math.ceil(this.mainPot / 10)));
                            }
                        }
                    }

                    for (var i = 0; i < subPots.length; i++) {
                        for (var j = 0; j < subPots[i].winners.length; j++) {
                            var actualPos = subPots[i].winners[j], tablePos = this.calculateTablePosition(subPots[i].winners[j]);
                            var prizeMoney  = Math.floor(subPots[i].amount / subPots[i].winners.length);
                            var playerPanel = this.playerPanels[tablePos];

                            this.players[actualPos].getMoney(prizeMoney);
                            if (tablePos === 0) {
                                this.addCredits(prizeMoney);
                            }
                            var mainPotTimeOut = (bombouts.length === 0) ? 0 : 2250;
                            mainPotTimeOut += i * 750;
                            setTimeout(function(prizeMoney) {
                                this.updateMoney();
                                this.mainToPlayer(prizeMoney);
                            }.bind(playerPanel, prizeMoney), mainPotTimeOut);
                            playerPanel.showWinnerStatus();
                            if (Math.abs(subPots[i].points) >= 23) {
                                setTimeout(function(prizeMoney) {
                                    this.updateMoney();
                                    this.sabaacToPlayer(prizeMoney);
                                }.bind(playerPanel, prizeMoney), mainPotTimeOut);
                            }
                        }

                        if (subPots[i].winners.length === 0) {
                            setTimeout(function(prizeMoney) {
                                this.updateMoney();
                                this.mainToSabaac(prizeMoney);
                            }.bind(this.playerPanels[0], subPots[i].amount), mainPotTimeOut);
                        }

                        setTimeout(function(i) {
                            this.mainPot -= subPots[i].amount;
                            if (this.mainPot <= 0)
                                this.mainPot = 0;
                            this.updateMainPot();
                        }.bind(this, i), mainPotTimeOut);
                        if (Math.abs(subPots[i].points) >= 23) {
                            this.sabaacMsg.visible = true;
                            this.diamond.visible = false;
                            setTimeout(function() {
                                this.updateSabbacPot();
                            }.bind(this), mainPotTimeOut);
                        } else {
                            this.updateSabbacPot();
                        }

                    }

                    if (Math.abs(this.players[this.playerPosition].totalPoints) > 23.5 || Math.abs(this.players[this.playerPosition].totalPoints) === 0) {
                        SB.SFX.bombout.play();
                    } else if (Math.abs(this.players[this.playerPosition].totalPoints) >= 23 && !data.foldWin) {
                        SB.SFX.sabaac.play();
                    }

                    var msg = "", lines = 1;;

                    if (data.foldWin) {
                        msg += this.players[subPots[0].winners[0]].name + " wins " + prizeMoney + " credits."
                    } else {
                        for (var i = 0; i < subPots.length; i++) {
                            if (subPots[i].winners.length > 0) {
                                var playerNames = this.players[subPots[i].winners[0]].name;
                                for (var k = 1; k < subPots[i].winners.length; k++) {
                                    playerNames += " and " + this.players[subPots[i].winners[k]].name;
                                }
                                playerNames += (subPots[i].winners.length > 1) ? " splits " : " wins ";
                                if (msg != "") {
                                    msg += "\n";
                                    lines++;
                                }
                                var pointMsg;
                                switch (subPots[i].points) {
                                    case 23.5:
                                        pointMsg = "Idiot's Array";
                                        break;
                                    case 22.5:
                                        pointMsg = "Fairy Empress";
                                        break;
                                    case 21.5:
                                        pointMsg = "Longing Hearts";
                                        break;
                                    case 20.5:
                                        pointMsg = "Temptation";
                                        break;
                                    default :
                                        pointMsg = subPots[i].points + "";
                                }
                                msg += playerNames + subPots[i].amount + " credits with a hand of " + pointMsg + ".";
                            }
                        }
                    }

                    if (msg == "") {
                        msg += "Everyone bombed out"
                    }

                    this.messageBox.show(msg, lines);
                }.bind(this);

                suddenDemiseDraw();
            }.bind(this), winnerDelay)
        },

        addCredits: function(value) {
            SB.CurrentUser.credits += value;
            if (SB.CurrentUser.id === -1000) {
                localStorage.setItem("credits", SB.CurrentUser.credits);
            } else {
                var data = {userid: SB.CurrentUser.id, credits: value};
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: SB.API_URL + "add.credits.api.php",
                    data: {'data': JSON.stringify(data)},
                    success: function(data) {

                    }.bind(this),
                    error: function() {

                    }.bind(this)
                });
            }
        },

        removeCredits: function(value) {
            SB.CurrentUser.credits -= value;
            if (SB.CurrentUser.id === -1000) {
                localStorage.setItem("credits", SB.CurrentUser.credits);
            } else {
                var data = {userid: SB.CurrentUser.id, credits: value};
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: SB.API_URL + "get.credits.api.php",
                    data: {'data': JSON.stringify(data)},
                    success: function(data) {

                    }.bind(this),
                    error: function() {

                    }.bind(this)
                });
            }
        },

        sabaacShift: function(data) {
            SB.SFX.shift.play();
            var player = this.players[this.playerPosition];

            for (var i = 0; i < player.cards.length; i++) {
                if (player.cards[i].value != data[i].value || player.cards[i].suit != data[i].suit) {
                    player.totalPoints -= player.cards[i].points;
                    player.totalPoints += data[i].points;
                    player.cards[i].updateCard(data[i].value, data[i].suit, data[i].points);
                    this.playerPanels[this.calculateTablePosition(this.playerPosition)].dissolveAndAppear(i);
                }
            }
        },

        resetPlayers: function() {
            if (this.players[this.playerPosition].money <= 0) {
                this.state.start('Lobby');
            } else {
                for (var i = 0; i < 6; i++) {
                    if (this.players[this.calculateActualPosition(i)]) {
                        this.playerPanels[i].hideCards();
                        this.players[this.calculateActualPosition(i)].resetForRound();
                        this.playerPanels[i].updateCards();
                        this.playerPanels[i].hidePointsPanel();
                        this.playerPanels[i].hideWinnerStatus();
                    }
                }

                this.switchOnGameLights();
                this.messageBox.hide();
                this.sabaacMsg.visible = false;
                this.suddenDemiseMsg.visible = false;
                this.diamond.visible = true;
            }
        },

        /*
         Design Code starts here
         */

        animateCard: function (callback, playerPosition) {
            var playerPanel = this.playerPanels[playerPosition];
            var pos = {x: 0, y: 0};
            pos.x = playerPanel.handCardsArray[2].worldPosition.x * SB.xScale;
            pos.y = playerPanel.handCardsArray[2].worldPosition.y * SB.yScale;

            var angle = playerPanel.handCards.rotation;

            var card = this.physicalDeck.pop();
            var tween = SB.game.add.tween(card);

            tween.to({ x: pos.x, y: pos.y, rotation: angle }, 250, "Linear", true);

            tween.onComplete.add(function() {
                playerPanel.updateCards();
                if (playerPosition === 0) {
                    playerPanel.faceUpCards();
                    playerPanel.showPointPanel();
                }
                callback();
                SB.game.tweens.remove(tween);
                tween = null;
                card.destroy();
            });

            SB.SFX.deal.play();
        },

        tradeCardAnimation: function (callback, playerPosition) {
            var playerPanel = this.playerPanels[playerPosition];
            var pos = {x: 0, y: 0};
            pos.x = playerPanel.handCardsArray[2].worldPosition.x * SB.xScale;
            pos.y = playerPanel.handCardsArray[2].worldPosition.y * SB.yScale;

            var angle = playerPanel.handCards.rotation;

            var card = this.physicalDeck.pop();
            var tween = SB.game.add.tween(card);

            tween.from({ x: pos.x, y: pos.y, rotation: angle }, 190, "Linear", true);

            tween.onComplete.add(function() {
                tween.to({ x: pos.x, y: pos.y, rotation: angle }, 190, "Linear", true); {}
                    tween.onComplete.add(function() {
                        playerPanel.updateCards();
                        if (playerPosition === 0) {
                            playerPanel.faceUpCards();
                            playerPanel.showPointPanel();
                        }
                        callback();
                        SB.game.tweens.remove(tween);
                        tween = null;
                        card.destroy();
                    });
            });
            SB.SFX.deal.play();
        },

        createPhysicalDeck: function () {
            this.physicalDeckGroup = this.physicalDeckGroup || SB.game.add.group();
            // Create Physical Deck
            for (var i = this.physicalDeck.length; i < 72; i++) {
                var card = SB.game.add.sprite(1640 - (i * 0.2), 495 - (i * 0.1), "sprites", "card-back");
                card.scale.set(0.4, 0.4);
                this.physicalDeckGroup.add(card);
                this.physicalDeck.push(card);
            }
        },

        switchOffGameLights: function () {
            if (this.lightTween1) {
                SB.game.tweens.remove(this.lightTween1);
                this.lightTween1 = null;
            }
            if (this.lightTween2) {
                SB.game.tweens.remove(this.lightTween2);
                this.lightTween2 = null;
            }
            this.lightTween1 = SB.game.add.tween(this.light1Crop).to({height: 0}, 1000, "Linear", true);
            this.lightTween2 = SB.game.add.tween(this.light2Crop).to({y: 455}, 1000, "Linear", true);

            this.lightTween1.onUpdateCallback(function(){
                this.gameLights.updateCrop();
                this.gameLights2.updateCrop();
            }.bind(this));
            this.lightTween2.onComplete.add(function() {
                SB.game.tweens.remove(this.lightTween1);
                this.lightTween1 = null;
                SB.game.tweens.remove(this.lightTween2);
                this.lightTween2 = null;
            }.bind(this));
        },

        switchOnGameLights: function () {
            this.light1Crop.height = 456;
            this.light2Crop.y = 0;
            this.gameLights.updateCrop();
            this.gameLights2.updateCrop();
        },

        calculateTablePosition: function(pos) {
            pos = (pos + 6 - this.playerPosition) % 6;
            return pos;
        },

        calculateActualPosition: function(pos) {
            pos = pos + this.playerPosition;
            pos %= 6;

            return pos;
        },

        createEnvironment: function () {
            this.background = SB.game.add.sprite(0, 0, "background");
            this.gameLights = SB.game.add.sprite(17, 70, "game-lights");
            this.gameLights2 = SB.game.add.sprite(17, 981, "game-lights2");
            this.gameLights2.anchor.set(0, 1);

            this.light1Crop = new Phaser.Rectangle(0, 0, 1827, 456);
            this.gameLights.crop(this.light1Crop);

            this.light2Crop = new Phaser.Rectangle(0, 0, 1827, 455);
            this.gameLights2.crop(this.light2Crop);

            this.menu = SB.game.add.group();
            this.menu.position.set(130,50);

            if (SB.CurrentUser.id != -1000)
                this.buyCreditsBtn = new SB.MenuButton("table-menu", "buy-credits", this.openShop.bind(this), { x: 0, y: 0 }, this.menu);
            this.addCreditsBtn = new SB.MenuButton("table-menu", "add-credits", this.openAddCredits.bind(this), { x: 0, y: 0 }, this.menu);
            this.rtLobbyBtn = new SB.MenuButton("table-menu", "to-lobby", this.returnToLobby.bind(this), { x: 0, y: 0 }, this.menu);
            this.menuBtn = new SB.MenuButton("table-menu", "menu", this.toggleMenu.bind(this), { x: 0, y: 0 }, this.menu);
            this.rtLobbyBtn.get().visible = false;
            if (SB.CurrentUser.id != -1000)
                this.buyCreditsBtn.get().visible = false;
            this.addCreditsBtn.get().visible = false;

            this.createPhysicalDeck();

            this.sabaacMsg = SB.game.add.sprite(825, 360, "sprites", "sabbac");
            this.suddenDemiseMsg = SB.game.add.sprite(770, 305, "sprites", "sudden-demise");
            this.suddenDemiseMsg.visible = false;
            this.diamond = SB.game.add.sprite(835, 380, "sprites", "sabacc-logo");
            this.sabaacMsg.scale.set(0.75, 0.75);
            this.sabaacMsg.visible = false;

            SB.game.add.sprite(1625, 415, "sprites", "droid");

            this.playerPanels.push (new SB.PlayerPanel(1, this.cardSelectCallback.bind(this)));

            for (var i = 0; i < this.NO_OF_PLAYERS - 1; i++) {
                this.playerPanels.push (new SB.PlayerPanel(i + 2));
            }

            this.mainPotImage = SB.game.add.group();
            this.mainPotImage.create(0, 0, "sprites", "main-pot");
            this.mainPotText = SB.game.add.text(145, 52, "0");
            this.mainPotChip = SB.game.add.sprite(0, 0, "sprites", "chip1");
            this.mainPotChip.scale.set(0.72, 0.72);

            this.tokenContainer = SB.game.add.group();

            this.mainPotImage.add(this.mainPotText);
            this.mainPotImage.add(this.mainPotChip);
            this.mainPotImage.position.set(600, 420);

            this.sabbacPotImage = SB.game.add.group();
            this.sabbacPotImage.create(0, 0, "sprites", "sabbac-pot-chip");
            this.sabbacPotText = SB.game.add.text(160, 63, "0");

            this.sabbacPotImage.add(this.sabbacPotText);
            this.sabbacPotImage.position.set(1050, 420);

            this.mainPotText.anchor.set(0.5, 0.5);
            this.mainPotText.font = 'MyFont';
            this.mainPotText.fontWeight = 'bold';
            this.mainPotText.fontSize = 32;
            this.mainPotText.fill = '#FFFFFF';

            this.sabbacPotText.anchor.set(0.5, 0.5);
            this.sabbacPotText.font = 'MyFont';
            this.sabbacPotText.fontWeight = 'bold';
            this.sabbacPotText.fontSize = 32;
            this.sabbacPotText.fill = '#FFFFFF';

            this.bettingPanel = new SB.BettingPanel();
            this.actionPanel = new SB.ActionPanel(this.executeAction.bind(this));
            this.actionPanel.hidePanel();
            this.bettingPanel.hidePanel();

            this.helpMsg = SB.game.add.text(1920 / 2, 800, "Please select a card to protect");
            this.helpMsg.anchor.set(0.5, 0.5);
            this.helpMsg.font = "Arial";
            this.helpMsg.stroke = "#00FFFF";
            this.helpMsg.strokeThickness = 6;
            this.helpMsg.visible = false;

            this.messageBox = new SB.FeedbackObject();

            this.serverInfoBox = new SB.ServerMessage();

            this.buyinBox = new SB.BuyinBox(this.addInCredits.bind(this));

            this.shop = new SB.Shop(this.addCredits.bind(this));
            this.shop.hide();

            // Creating SFX
            SB.SFX = {};
            SB.SFX.bet = this.game.add.sound("bet");
            SB.SFX.deal = this.game.add.sound("deal");
            SB.SFX.interference = this.game.add.sound("interference");
            SB.SFX.movechip = this.game.add.sound("movechip");
            SB.SFX.shuffle = this.game.add.sound("shuffle");

            SB.SFX.alderaan = this.game.add.sound("alderaan");
            SB.SFX.bombout = this.game.add.sound("bombout");
            SB.SFX.check = this.game.add.sound("check");
            SB.SFX.fold = this.game.add.sound("fold");
            SB.SFX.raise = this.game.add.sound("raise");
            SB.SFX.sabaac = this.game.add.sound("sabaac");
            SB.SFX.shift = this.game.add.sound("shift");
            SB.SFX.turn = this.game.add.sound("turn");
            SB.SFX.winchip = this.game.add.sound("winchip");
            SB.SFX.timer = this.game.add.sound("timer");
        },

        openAddCredits: function() {
            var currentCredits = this.players[this.playerPosition].money;
            this.buyinBox.showPanel(0, Math.min(Math.max((SB.Globals.buyin - currentCredits), 0), Math.max((SB.CurrentUser.credits - currentCredits), 0)));
        },

        // add credits from reserves
        addInCredits: function(value) {
            this.socket.emit(SB.events.ADD_IN_CREDITS, value);
        },

        updatePlayerMoney: function(data) {
            for (var i = 0; i < 6; i++) {
                if (this.players[i] && data[i]) {
                    this.players[i].money = parseInt(data[i]);
                    this.playerPanels[this.calculateTablePosition(i)].updateMoney();
                }
            }
        },

        openShop: function() {
            this.shop.show();
        },

        buyCredits: function(value) {
            SB.CurrentUser.credits += parseInt(value);
        },

        updateSabbacPot: function () {
            this.sabbacPotText.text = this.sabbacPot;
        },

        updateMainPot: function () {
            this.mainPotText.text = this.mainPot;
            for (var i = 0; i < 6; i++) {
                if (this.players[i] && this.players[i].betAmount !== 0) {
                    this.playerPanels[this.calculateTablePosition(i)].pileToMainPot(this.players[i].betAmount);
                }
            }

            setTimeout(function() {
                this.mainPotChip.frameName = "chip" + SB.utils.getChipColor (this.mainPot) + "";
            }.bind(this), 500);
        },

        toggleMenu: function() {
            if (this.addCreditsBtn.get().visible) {
                SB.game.add.tween(this.addCreditsBtn.get()).to( { y: 0 }, 500, "Linear", true);
                if (SB.CurrentUser.id != -1000)
                    SB.game.add.tween(this.buyCreditsBtn.get()).to( { y: 0 }, 500, "Linear", true);
                SB.game.add.tween(this.rtLobbyBtn.get()).to( { y: 0 }, 500, "Linear", true).onComplete.add(function() {
                    this.rtLobbyBtn.get().visible = false;
                    if (SB.CurrentUser.id != -1000)
                        this.buyCreditsBtn.get().visible = false;
                    this.addCreditsBtn.get().visible = false;
                }.bind(this));
            } else {
                this.rtLobbyBtn.get().visible = true;
                if (SB.CurrentUser.id != -1000)
                    this.buyCreditsBtn.get().visible = true;
                this.addCreditsBtn.get().visible = true;

                SB.game.add.tween(this.rtLobbyBtn.get()).to( { y: 100 }, 500, "Linear", true);
                SB.game.add.tween(this.addCreditsBtn.get()).to( { y: 200 }, 500, "Linear", true);
                if (SB.CurrentUser.id != -1000)
                    SB.game.add.tween(this.buyCreditsBtn.get()).to( { y: 300 }, 500, "Linear", true);

            }
        },

        returnToLobby: function() {
            this.socket.disconnect();
            document.removeEventListener("backbutton", this.returnToLobby.bind(this), false);

            if (this.turnTimer)
                this.turnTimer.stop();

            if (this.tween) {
                this.tween.stop();
                SB.game.tweens.remove(this.tween);
                this.tween = null;
            }

            var id = setTimeout(function() {}, 0);

            while (id--) {
                clearTimeout(id); // will do nothing if no timeout with id is present
            }

            this.state.start('Lobby');
        }
    };

    return SB.MultiState;
});

SB.Player = function (position, name, money) {
    this.isWaiting = true;
    this.name = name;
    this.position = position;
    this.cards = new Array ();
    this.interferenceCards = [];
    this.totalPoints = 0;
    this.betAmount = 0;
    this.money = money;
    this.folded = false;
    this.cardSelectWaitingFor = "";
};

SB.Player.prototype = {
    takeAnte: function (amount) {
        this.money = this.money - amount;
    },

    putBet: function (amount) {
        this.money -= amount;
        this.betAmount += amount;
    },

    getCard: function(card) {
        this.cards.push(card);
        this.totalPoints += card.points;
    },

    tradeCard: function(newCard, oldCardPosition) {
        this.totalPoints -= this.cards[oldCardPosition].points;
        this.cards[oldCardPosition] = newCard;
        this.totalPoints += newCard.points;
    },

    giveCard: function(oldCardPosition) {
        this.totalPoints -= this.cards[oldCardPosition].points;
        this.cards.splice(oldCardPosition, 1);
    },

    updateHand: function(cards) {
        this.totalPoints = 0;

        for (var i = 0; i < this.cards.length; i++) {
            this.cards[i].updateCard(cards[i].value, cards[i].suit, cards[i].points);
            this.totalPoints += cards[i].points;
        }

        for (var i = 0; i < this.interferenceCards.length; i++) {
            this.totalPoints += this.interferenceCards[i].points;
        }
    },

    resetForPhase: function() {
        this.betAmount = 0;
    },

    resetForRound: function() {
        this.totalPoints = 0;
        this.betAmount = 0;
        this.folded = false;
        this.cards = [];
        this.interferenceCards = [];
        this.cardSelectWaitingFor = "";
    },

    getMoney: function (amount) {
        this.money += amount;
    },

    protectCard: function (oldCardPosition) {
        var card = this.cards.splice(oldCardPosition, 1)[0];
        card.isInterferenced = true;
        this.interferenceCards.push(card);
    }
}

