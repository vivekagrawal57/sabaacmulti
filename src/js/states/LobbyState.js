define([
    'phaser',
    'objects/ui/TableButton'
], function (
    Phaser,
    TableButton
) {
    'use strict';

    SB.LobbyState = function() {};

    SB.LobbyState.prototype = {
        preload: function() {

        },

        create: function() {
            this.credits = SB.CurrentUser.credits;
            document.addEventListener("backbutton", this.backToMain.bind(this), false);

            this.background = SB.game.add.sprite(0, 0, "lobbyBG");
            this.recommended = SB.game.add.sprite(0, 0, "sprites2", "recommended");

            var btns = SB.game.add.group();

            SB.game.add.button(1790, 40, "sprites2", this.backToMain, this, "lobby-exit-normal", "lobby-exit-normal", "lobby-exit-down");

            this.creditsValue = SB.game.add.text(1850, 250, "Your bankroll: " + this.credits);
            this.creditsValue.fill = "#fffa7c";
            this.creditsValue.stroke = "black";
            this.creditsValue.strokeThickness = 3;
            this.creditsValue.fontSize = 42;
            this.creditsValue.anchor.set(1, 0.5);

            SB.game.add.button(1670, 300, "sprites2", function() {
                this.openShop();
            }, this, "add-button-normal", "add-button-normal", "add-button-down");

            this.loginBox = new SB.LoginBox(this.loginPlayer.bind(this), this.loginFB.bind(this));
            this.loginBtn = new SB.MenuButton("big-ui-button", "login-register", this.loginBox.showLogin.bind(this.loginBox), { x: 520, y: 327 }, btns, 1);
            this.fbLoginBtn = new SB.MenuButton("big-ui-button", "facebook-login", this.facebookLogin.bind(this), { x: 1400, y: 327 }, btns, 1);
            this.changePassBtn = new SB.MenuButton("big-ui-button", "change-pass", this.loginBox.showChangePass.bind(this.loginBox), { x: 1400, y: 327 }, btns, 1);
            this.logoutBtn = new SB.MenuButton("big-ui-button", "logout", this.logoutPlayer.bind(this), { x: 520, y: 327 }, btns, 1);

            this.welcomeMsg = SB.game.add.text(50, 250, "Welcome User");
            this.welcomeMsg.fill = "white";
            this.welcomeMsg.fontSize = 36;
            this.welcomeMsg.font = 'MyFont';
            this.welcomeMsg.anchor.set(0, 0.5);

            new SB.TableButton(200, {x: 75, y: 460}, "chip1", 0, this.openShop.bind(this), this.loginBox.showLogin.bind(this.loginBox), btns);
            new SB.TableButton(300, {x: 377, y: 460}, "chip2", 1, this.openShop.bind(this), this.loginBox.showLogin.bind(this.loginBox), btns);
            new SB.TableButton(600, {x: 679, y: 460}, "chip3", 2, this.openShop.bind(this), this.loginBox.showLogin.bind(this.loginBox), btns);
            new SB.TableButton(1000, {x: 981, y: 460}, "chip4", 3, this.openShop.bind(this), this.loginBox.showLogin.bind(this.loginBox), btns);
            new SB.TableButton(2000, {x: 1283, y: 460}, "chip5", 4, this.openShop.bind(this), this.loginBox.showLogin.bind(this.loginBox), btns);
            new SB.TableButton(5000, {x: 1585, y: 460}, "chip6", 5, this.openShop.bind(this), this.loginBox.showLogin.bind(this.loginBox), btns);

            this.changePassBtn.get().visible = false;
            this.logoutBtn.get().visible = false;

            if (!SB.game.device.desktop)
                this.setupFacebook();

            if (!SB.CurrentUser.guestLogin) {
                this.loginBtn.get().visible = false;
                this.fbLoginBtn.get().visible = false;
                this.logoutBtn.get().visible = true;
                if (!SB.CurrentUser.fbLogin)
                    this.changePassBtn.get().visible = true;
            }

            this.welcomeMsg.text = "Welcome " + SB.CurrentUser.username;

            this.shop = new SB.Shop(this.addCredits.bind(this));
            this.shop.hide();

            this.setRecommended(this.credits);
        },

        openShop: function() {
            this.shop.show();
        },

        backToMain: function() {
            document.removeEventListener("backbutton", this.backToMain.bind(this), false);
            this.state.start('Menu');
        },

        setRecommended: function(value) {
            switch (true) {
                case (value < 300):
                    this.recommended.position.set(60, 440);
                    break;
                case (value >= 300 && value < 600):
                    this.recommended.position.set(362, 440);
                    break;
                case (value >= 600 && value < 1000):
                    this.recommended.position.set(664, 440);
                    break;
                case (value >= 1000 && value < 2000):
                    this.recommended.position.set(966, 440);
                    break;
                case (value >= 2000 && value < 5000):
                    this.recommended.position.set(1268, 440);
                    break;
                case (value >= 5000):
                    this.recommended.position.set(1570, 440);
                    break;
            }
        },

        addCredits: function(value) {
            this.credits += parseInt(value);
            this.creditsValue.text = "Your bankroll: " + this.credits;
            this.setRecommended(this.credits);
            SB.CurrentUser.credits = this.credits;
        },

        logoutPlayer: function() {
            SB.CurrentUser = null;

            localStorage.setItem("username", "");
            localStorage.setItem("password", "");

            document.removeEventListener("backbutton", this.backToMain.bind(this), false);
            this.state.start('Menu');
        },

        loginPlayer: function(data) {
            this.loginBtn.get().visible = false;
            this.fbLoginBtn.get().visible = false;
            this.changePassBtn.get().visible = true;
            this.logoutBtn.get().visible = true;
            this.welcomeMsg.text = "Welcome " + data["data"].username;
            this.welcomeMsg.visible = true;

            SB.CurrentUser = {
                id: data["data"]["user_id"],
                username: data["data"]["username"],
                credits: parseInt(data["data"]["credits"]),
                fbLogin: false,
                guestLogin: false
            }

            this.credits = SB.CurrentUser.credits;
            this.creditsValue.text = "Your bankroll: " + this.credits;
            this.setRecommended(this.credits);
        },

        loginFB: function(data) {
            this.loginBtn.get().visible = false;
            this.fbLoginBtn.get().visible = false;
            this.logoutBtn.get().visible = true;
            this.welcomeMsg.text = "Welcome " + data.username;
            this.welcomeMsg.visible = true;

            SB.CurrentUser = {
                id: data["user_id"],
                username: data["username"],
                credits: parseInt(data["credits"]),
                fbLogin: true,
                guestLogin: false
            }

            this.credits = SB.CurrentUser.credits;
            this.creditsValue.text = "Your bankroll: " + this.credits;
            this.setRecommended(this.credits);
        },

        setupFacebook: function() {
            this.fbService = Cocoon.Social.Facebook;
            this.socialAPI = null;
            this.fbService.init({
                appId      : "1156812971047963",
                xfbml      : true,
                version    : 'v2.5'

            }, function () {
                this.socialAPI = this.fbService.getSocialInterface();
            }.bind(this));
        },

        facebookLogin: function() {
            this.fbService.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    this.loginBox.loggingFacebook(response);
                } else {
                    this.socialAPI.login(function(response, error){
                        if (response.status === 'connected') {
                            console.log("Login status: " + JSON.stringify(response));
                            this.loginBox.loggingFacebook(response);
                        }
                    });
                }
            }.bind(this));
        }
    };

    return SB.LobbyState;
});