define([
    'phaser',
    'js/objects/LoginBox.js',
    'js/objects/Shop.js',
    'js/objects/Utility.js',
    'js/objects/Ads.js',
    'js/objects/SignInBox.js',
    'js/objects/ui/SoundButton.js',
], function (
    Phaser,
    LoginBox,
    Shop,
    SoundButton,
    Utility,
    Ads
) {
    'use strict';

    SB.MenuState = function() {};

    SB.MenuState.prototype = {
        preload: function() {

        },

        create: function() {
            SB.utils = new SB.Utility();
            SB.ads = new SB.Ads();

            this.background = SB.game.add.sprite(0, 0, "menuBG");

            SB.game.add.button(520, 613, "sprites2", this.startSingle, this, "single-normal", "single-normal", "single-down");
            SB.game.add.button(1020, 600, "sprites2", this.startMulti, this, "multi-normal", "multi-normal", "multi-down");
            SB.game.add.button(1700, 900, "sprites2", this.startTutorial, this, "help-normal", "help-normal", "help-down");
            SB.game.tweens.frameBased = true;

            this.signInBox = new SB.SignInBox();
        },

        startSingle: function() {
            this.state.start('SingleGame');
        },

        startMulti: function() {
            if (SB.CurrentUser)
                this.state.start('Lobby');
            else
                this.signInBox.show();
        },

        startTutorial: function() {
            this.state.start('Tutorial');
        }
    };

    return SB.MenuState;
});