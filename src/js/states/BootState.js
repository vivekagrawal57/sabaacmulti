define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.BootState = function() {};

    SB.BootState.prototype = {
        preload: function() {
            this.load.image("preloaderBar", "././assets/preload.png");
            this.load.image("preloaderBarBg", "././assets/preload_empty.png");
            this.load.image("preloaderBg", "././assets/preload_bg.jpg");
        },

        init: function() {

        },

        create: function() {
            if (this.game.device.desktop) //if playing on desktop
            {
                this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL; //always show whole game
                this.scale.pageAlignHorizontally = true; //align horizontally
                this.scale.pageAlignVertically = true;
                SB.xScale = 1;
                SB.yScale = 1;
            }
            else
            {
                SB.xScale = 1920 / SB.gameWidth;
                SB.yScale = 1080 / SB.gameHeight;

                SB.game.world.scale.set(SB.gameWidth / 1920, SB.gameHeight / 1080);
            }

            this.state.start('Preload');   //start the Preloader state
        }
    };

    return SB.BootState;
});