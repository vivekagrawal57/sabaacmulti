var SB = SB || {};

(function () {
    'use strict';

    SB.Utils = {

    };

    SB.Constants = {
        WAITING: 1,
        STARTED: 2,
    };

    SB.Globals = {
        // Table Values
        buyin: 10000,
        blind: 100,
        ante: 25,
    };

    SB.API_URL = "http://sabaccphp-sabaacmulti.rhcloud.com/";

    requirejs.config({
        baseUrl: "js/",
        waitSeconds: 0,
        paths: {
            phaser: 'lib/phaser/phaser',
            underscore: 'lib/underscore/underscore',
            jquery: 'lib/jquery/jquery'
        },
        shim: {
            'phaser': {
                exports: 'Phaser'
            }
        }
    });

    require([
        'phaser',
        'Game',
        'jquery'
    ],
    function (
        Phaser,
        Game
    ) {
        var game = new Game();
        game.start();
    });
}());