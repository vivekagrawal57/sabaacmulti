define([
    'phaser',
    'states/BootState',
    'states/PreloadState',
    'states/MultiState',
    'states/MenuState',
    'states/SingleState',
    'states/LobbyState',
    'states/Tutorial'
], function (
    Phaser,
    BootState,
    PreloadState,
    MultiState,
    MenuState,
    SingleState,
    LobbyState,
    Tutorial
) {
    'use strict';

    SB.Game = function () {};

    SB.Game.prototype = {
        start: function() {
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                SB.gameWidth = window.innerWidth;
                SB.gameHeight = window.innerHeight;
                SB.game = new Phaser.Game(SB.gameWidth, SB.gameHeight, Phaser.CANVAS, '');
                Cocoon.Device.autoLock(false);
            } else {
                SB.game = new Phaser.Game(1920, 1080, Phaser.CANVAS, '');
            }
            SB.game.state.add('Boot', BootState);
            SB.game.state.add('Preload', PreloadState);
            SB.game.state.add('Menu', MenuState);
            SB.game.state.add('MultiGame', MultiState);
            SB.game.state.add('SingleGame', SingleState);
            SB.game.state.add('Tutorial', Tutorial);
            SB.game.state.add('Lobby', LobbyState);
            SB.game.state.start('Boot');
        }
    };

    return SB.Game;
});