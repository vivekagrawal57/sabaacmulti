define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.Timer = function() {
        this.create();
    };

    SB.Timer.prototype = {
        timerLines: [],

        create: function() {
            this.container = SB.game.add.group();
            this.graphics = SB.game.add.graphics(17, 42);
            this.graphics.lineStyle(5, 0xffff00, 10);
            this.container.add(this.graphics);
            this.timerLines.push(this.graphics);

            this.container.add(this.graphics);
        },

        drawTimer: function (timeLeft) {
            var totalLength = 342;

            var timeTotal = 4;

            var lengthToDraw = Math.floor(totalLength * timeLeft / timeTotal);

            this.graphics.clear();
            this.graphics.lineStyle(5, 0xffff00, 10);

            var drawLength = (lengthToDraw > 42) ? 42 : lengthToDraw;
            this.graphics.moveTo(44, 2);
            this.graphics.lineTo(44 - drawLength, 2);
            lengthToDraw -= drawLength;

            drawLength = (lengthToDraw > 86) ? 86 : lengthToDraw;
            this.graphics.moveTo(3, 0);
            this.graphics.lineTo(3, drawLength);
            lengthToDraw -= drawLength;

            drawLength = (lengthToDraw > 85) ? 85 : lengthToDraw;
            this.graphics.moveTo(1, 84);
            this.graphics.lineTo(1 + drawLength, 84);
            lengthToDraw -= drawLength;

            drawLength = (lengthToDraw > 86) ? 86 : lengthToDraw;
            this.graphics.moveTo(84, 86);
            this.graphics.lineTo(84, 86 - drawLength);
            lengthToDraw -= drawLength;

            drawLength = (lengthToDraw > 42) ? 42 : lengthToDraw;
            this.graphics.moveTo(86, 2);
            this.graphics.lineTo(86 - drawLength, 2);
        }
    };

    return SB.Timer;
});



