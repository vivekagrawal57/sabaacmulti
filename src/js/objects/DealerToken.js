define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.DealerToken = function(position, parent) {
        this.position = position;
        this.container = parent;
        this.create();
    };

    SB.DealerToken.prototype = {
        token: null,
        position: 4,
        coordinates: [
            { x: 770, y: 610 },
            { x: 370, y: 590 },
            { x: 525, y: 335 },
            { x: 840, y: 320 },
            { x: 1490, y: 420 },
            { x: 1325, y: 625 }
        ],

        create: function() {
            var pos = this.coordinates[this.position];
            this.token = SB.game.add.sprite(pos.x, pos.y, "sprites", "dealer-token");
            this.tween = SB.game.add.tween(this.token);
            this.token.visible = false;
            this.container.add(this.token);
        },

        shift: function(newPosition) {
            this.token.visible = true
            if (this.tween) {
                SB.game.tweens.remove(this.tween);
                this.tween = null;
                this.tween = SB.game.add.tween(this.token);
            }
            this.token.position.set(this.coordinates[this.position].x, this.coordinates[this.position].y);
            this.tween.to({ x: this.coordinates[newPosition].x, y: this.coordinates[newPosition].y }, 600, "Linear", true);
            this.position = newPosition;
        }
    };

    return SB.DealerToken;
});


