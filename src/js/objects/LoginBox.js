define([
    'phaser',
    'js/objects/ui/InputBox.js',
    'js/objects/ui/MenuButton.js',
    'js/objects/ui/Loading.js',
], function (
    Phaser,
    InputBox,
    MenuButton,
    Loading
) {
    'use strict';

    SB.LoginBox = function(setupPlayer, setupFBPlayer, setupDevicePlayer) {
        this.setupPlayer = setupPlayer;
        this.setupFBPlayer = setupFBPlayer;
        this.setupDevicePlayer = setupDevicePlayer;
        this.create();
    };

    SB.LoginBox.prototype = {
        create: function() {
            this.container = SB.game.add.group();
            var bmd = SB.game.add.bitmapData(1920, 1080);
            bmd.ctx.beginPath();
            bmd.ctx.rect(0, 0, 1920, 1080);
            bmd.ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
            bmd.ctx.fill();

            var cover = SB.game.add.sprite(0, 0, bmd);
            cover.anchor.set(0.5, 0.5);
            cover.inputEnabled = true;

            var bg = SB.game.add.sprite(0, -150, "login-bg")
            bg.anchor.set(0.5, 0.5);

            var closeBtn = SB.game.add.button(345, -400, "sprites2", this.hide, this, "lobby-exit-normal", "lobby-exit-normal", "lobby-exit-down");
            closeBtn.scale.set(0.75, 0.75);

            this.errorMsg = SB.game.add.text(0, -30, "Wrong Username or Password");
            this.errorMsg.fill = "black";
            this.errorMsg.stroke = "#00FFFF";
            this.errorMsg.strokeThickness = 6;
            this.errorMsg.fontSize = 28;
            this.errorMsg.anchor.set(0.5, 0.5);
            this.errorMsg.alpha = 0;

            // Login Box
            this.login = SB.game.add.group();
            this.login.create(-62, -410, "sprites2", "login-text");
            this.login.create(-320, -280, "sprites2", "user-text");
            this.loginUser = new SB.InputBox("Username", { x: 100, y: -265 }, this.login, false);
            this.login.create(-320, -200, "sprites2", "pass-text");
            this.loginPass = new SB.InputBox("Password", { x: 100, y: -185 }, this.login, true);

            new SB.MenuButton("ui-button", "login", this.logging.bind(this), { x: -150, y: -100 }, this.login);
            new SB.MenuButton("ui-button", "register", this.showRegister.bind(this), { x: 150, y: -100 }, this.login);
            new SB.MenuButton("big-ui-button", "forgot-password", this.showForgot.bind(this), { x: 230, y: 50 }, this.login, 0.75);

            // Register Box
            this.register = SB.game.add.group();
            this.register.create(-93, -410, "sprites2", "register-text");
            this.register.create(-260, -280, "sprites2", "user-text");
            this.registerUser = new SB.InputBox("Username", { x: 140, y: -267 }, this.register, false);
            this.register.create(-183, -220, "sprites2", "email-text");
            this.registerEmail = new SB.InputBox("Email Address", { x: 140, y: -207 }, this.register, false);
            this.register.create(-255, -160, "sprites2", "pass-text");
            this.registerPass = new SB.InputBox("Password", { x: 140, y: -147 }, this.register, true);
            this.register.create(-363, -100, "sprites2", "confirm-pass-text");
            this.registerConfirmPass = new SB.InputBox("Confirm Password", { x: 140, y: -87 }, this.register, true);

            new SB.MenuButton("ui-button", "login", this.showLogin.bind(this), { x: -150, y: 30 }, this.register);
            new SB.MenuButton("ui-button", "register", this.registering.bind(this), { x: 150, y: 30 }, this.register);

            // Change Password Box
            this.changePass = SB.game.add.group();
            this.changePass.create(-155, -405, "sprites2", "change-pass-text");
            this.changePass.create(-298, -280, "sprites2", "old-password");
            this.changeOldPass = new SB.InputBox("Old Password", { x: 140, y: -267 }, this.changePass, true);
            this.changePass.create(-312, -210, "sprites2", "new-password");
            this.changeNewPass = new SB.InputBox("New Password", { x: 140, y: -197 }, this.changePass, true);
            this.changePass.create(-363, -140, "sprites2", "confirm-pass-text");
            this.changeConfirmPass = new SB.InputBox("Confirm Password", { x: 140, y: -127 }, this.changePass, true);

            new SB.MenuButton("ui-button", "submit", this.changingPass.bind(this), { x: 0, y: 30 }, this.changePass);

            // Forgot Password Box
            this.forgotPass = SB.game.add.group();
            this.forgotPass.create(-155, -405, "sprites2", "forgot-pass-text");
            this.forgotPass.create(-323, -220, "sprites2", "email-text");
            this.forgotEmail = new SB.InputBox("Email Address", { x: 0, y: -207 }, this.forgotPass, false);
            new SB.MenuButton("ui-button", "submit", this.recovering.bind(this), { x: 0, y: -90 }, this.forgotPass);


            this.container.add(cover);
            this.container.add(bg);
            this.container.add(closeBtn);
            this.container.add(this.errorMsg);
            this.container.position.set (960, 540);
            this.container.add(this.login);
            this.container.add(this.register);
            this.container.add(this.changePass);
            this.container.add(this.forgotPass);

            this.hide();

            this.loadingCircle = new SB.Loading();
        },

        showLogin: function () {
            this.hide();
            this.container.visible = true;
            this.login.visible = true;
        },

        showRegister: function () {
            this.hide();
            this.container.visible = true;
            this.register.visible = true;
        },

        showChangePass: function () {
            this.hide();
            this.container.visible = true;
            this.changePass.visible = true;
        },

        showForgot: function () {
            this.hide();
            this.container.visible = true;
            this.forgotPass.visible = true;
        },

        showError: function (text) {
            this.errorMsg.text = text;
            if (this.errorTween) {
                this.errorTween.stop();
                SB.game.tweens.remove(this.errorTween);
                this.errorTween = null;
            }
            this.errorMsg.alpha = 1;
            this.errorTween = SB.game.add.tween(this.errorMsg).to( { alpha: 0 }, 1000, "Linear", true, 2000);
        },

        logging: function () {
            if (this.loginUser.getText() == "") {
                this.showError("Please enter Username");
                return;
            }
            if (this.loginPass.getText() == "") {
                this.showError("Please enter Password");
                return;
            }

            this.loadingCircle.show();

            var data = {username: this.loginUser.getText(), password: this.loginPass.getText(), oauth_provider: "normal"};
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SB.API_URL + "login.api.php",
                data: {'data': JSON.stringify(data)},
                success: function(data) {
                    this.loadingCircle.hide();
                    if (data.responseStatus === "true") {
                        this.hide();
                        this.setupPlayer(data);
                        localStorage.setItem("username", this.loginUser.getText());
                        localStorage.setItem("password", this.loginPass.getText());
                    } else {
                        this.showError("Wrong username or password");
                    }
                }.bind(this),
                error: function() {
                    this.loadingCircle.hide();
                    this.showError("Something went wrong with the Internet");
                }.bind(this)
            });
        },

        loggingFacebook: function (fbData) {
            this.loadingCircle.show();

            var data = {username: fbData.authResponse.user.id, oauth_provider: "facebook"};
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SB.API_URL + "login.api.php",
                data: {'data': JSON.stringify(data)},
                success: function(data) {
                    this.loadingCircle.hide();
                    console.log(JSON.stringify(data));
                    if (data.responseStatus === "true") {
                        this.hide();
                        if (!data["account_exist"] && SB.CurrentUser && SB.CurrentUser.guestLogin) {
                            console.log("Guest was already logged in");
                            var guestCredits = Math.max(SB.CurrentUser.credits, 300);
                            this.removeCredits(SB.CurrentUser.credits);
                            this.addCredits(data["data"]["user_id"], guestCredits - 300);
                            data["data"]["credits"] = guestCredits;
                        }
                        this.setupFBPlayer({
                            user_id: data["data"]["user_id"],
                            username: fbData.authResponse.user.first_name,
                            credits: data["data"]["credits"]
                        });
                        localStorage.setItem("username", fbData.authResponse.user.id);
                        localStorage.setItem("password", "facebook");
                        localStorage.setItem("player_name", fbData.authResponse.user.first_name);
                    }
                }.bind(this),
                error: function() {
                    this.loadingCircle.hide();
                    this.showError("Something went wrong with the Internet");
                }.bind(this)
            });
        },

        loggingDevice: function (udid) {
            this.loadingCircle.show();
            console.log(udid);

            var data = {username: udid, oauth_provider: "device"};
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SB.API_URL + "login.api.php",
                data: {'data': JSON.stringify(data)},
                success: function(data) {
                    this.loadingCircle.hide();
                    console.log("here "+ JSON.stringify(data));
                    if (data.responseStatus === "true") {
                        this.hide();
                        this.setupDevicePlayer({
                            user_id: data["data"]["user_id"],
                            username: "Guest" + _.random(10000, 99999),
                            credits: data["data"]["credits"]
                        });
                        localStorage.setItem("username", udid);
                        localStorage.setItem("password", "device");
                    }
                }.bind(this),
                error: function() {
                    this.loadingCircle.hide();
                    this.showError("Something went wrong with the Internet");
                }.bind(this)
            });
        },

        registering: function () {
            if (this.registerUser.getText() == "") {
                this.showError("Please enter Username");
                return;
            }
            if (this.registerEmail.getText() == "") {
                this.showError("Please enter Email");
                return;
            }
            var atpos = this.registerEmail.getText().indexOf("@");
            var dotpos = this.registerEmail.getText().lastIndexOf(".");
            if (atpos<1 || dotpos<atpos+2 || dotpos+2>=this.registerEmail.getText().length) {
                this.showError("Please enter valid Email");
                return;
            }
            if (this.registerPass.getText() == "") {
                this.showError("Please enter Password");
                return;
            }
            if (this.registerPass.getText().length < 8) {
                this.showError("Password should be of atleast 8 characters");
                return;
            }
            if (this.registerConfirmPass.getText() !== this.registerPass.getText()) {
                this.showError("Passwords should match");
                return;
            }

            this.loadingCircle.show();

            var data = {username: this.registerUser.getText(), password: this.registerPass.getText(), email: this.registerEmail.getText()};
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SB.API_URL + "register.api.php",
                data: {'data': JSON.stringify(data)},
                success: function(data) {
                    this.loadingCircle.hide();
                    if (data.responseStatus === "true") {
                        this.hide();
                        if (SB.CurrentUser && SB.CurrentUser.guestLogin) {
                            console.log("Guest was already logged in");
                            var guestCredits = Math.max(SB.CurrentUser.credits, 300);
                            this.removeCredits(SB.CurrentUser.credits);
                            this.addCredits(data["data"]["user_id"], guestCredits - 300);
                            data["data"]["credits"] = guestCredits;
                        }
                        this.setupPlayer(data);
                        localStorage.setItem("username", this.registerUser.getText());
                        localStorage.setItem("password", this.registerPass.getText());
                    } else {
                        this.showError(data.error);
                    }
                }.bind(this),
                error: function() {
                    this.loadingCircle.hide();
                    this.showError("Something went wrong with the Internet");
                }.bind(this)
            });
        },

        changingPass: function() {
            if (this.changeOldPass.getText() == "") {
                this.showError("Please enter Old Password");
                return;
            }
            if (this.changeNewPass.getText() == "") {
                this.showError("Please enter New Password");
                return;
            }
            if (this.changeNewPass.getText().length < 8) {
                this.showError("New Password should be of atleast 8 characters");
                return;
            }
            if (this.changeConfirmPass.getText() !== this.changeNewPass.getText()) {
                this.showError("Passwords should match");
                return;
            }

            this.loadingCircle.show();

            var data = {userid: SB.CurrentUser.id, oldpassword: this.changeOldPass.getText(), newpassword: this.changeNewPass.getText()};
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SB.API_URL + "update.pass.api.php",
                data: {'data': JSON.stringify(data)},
                success: function(data) {
                    this.loadingCircle.hide();
                    if (data.responseStatus === "true") {
                        this.showError("Password Changed");
                    } else {
                        this.showError("Incorrect Old Password");
                    }
                }.bind(this),
                error: function() {
                    this.loadingCircle.hide();
                    this.showError("Something went wrong with the Internet");
                }.bind(this)
            });
        },

        recovering: function() {
            if (this.forgotEmail.getText() == "") {
                this.showError("Please enter Email");
                return;
            }
            var atpos = this.forgotEmail.getText().indexOf("@");
            var dotpos = this.forgotEmail.getText().lastIndexOf(".");
            if (atpos<1 || dotpos<atpos+2 || dotpos+2>=this.forgotEmail.getText().length) {
                this.showError("Please enter valid Email");
                return;
            }

            this.loadingCircle.show();

            var data = {email: this.forgotEmail.getText()};
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SB.API_URL + "forgot.pass.api.php",
                data: {'data': JSON.stringify(data)},
                success: function(data) {
                    console.log(JSON.stringify(data));
                    this.loadingCircle.hide();
                    if (data.responseStatus === "true") {
                        this.showLogin();
                        this.showError("Username and Password Sent");
                    } else {
                        this.showError("No Associated account with that Email found");
                    }
                }.bind(this),
                error: function() {
                    this.loadingCircle.hide();
                    this.showError("Something went wrong with the Internet");
                }.bind(this)
            });
        },

        addCredits: function(id, value) {
            var data = {userid: id, credits: value};
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SB.API_URL + "add.credits.api.php",
                data: {'data': JSON.stringify(data)},
                success: function(data) {

                }.bind(this),
                error: function() {

                }.bind(this)
            });

        },

        removeCredits: function(value) {
            var data = {userid: SB.CurrentUser.id, credits: value};
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SB.API_URL + "get.credits.api.php",
                data: {'data': JSON.stringify(data)},
                success: function(data) {

                }.bind(this),
                error: function() {

                }.bind(this)
            });

        },

        hide: function () {
            this.container.visible = false;
            this.login.visible = false;
            this.register.visible = false;
            this.changePass.visible = false;
            this.forgotPass.visible = false;
        }
    };

    return SB.LoginBox;
});
