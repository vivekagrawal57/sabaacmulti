define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.Utility = function() {

    };

    SB.Utility.prototype = {
        getChipColor: function(amount) {
            if (amount <= 10) {
                return 1;
            } else if (amount <= 50) {
                return 2;
            } else if (amount <= 200) {
                return 3;
            } else if (amount <= 500) {
                return 4;
            } else if (amount <= 1000) {
                return 5;
            } else {
                return 6;
            }
        }
    };

    return SB.Utility;
});