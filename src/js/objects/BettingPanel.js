define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.BettingPanel = function() {
        this.create();
    };

    SB.BettingPanel.prototype = {
        min: 0,
        max: 10000,
        value: 0,

        create: function() {
            this.barValues = ["ALL IN", this.max * 0.75, this.max * 0.5, this.max * 0.25],
            this.container = SB.game.add.group();
            this.container.create(0, 0, "sprites", "bet-bg");
            this.container.create(85, 90, "sprites", "betting-bar");

            this.valueText = SB.game.add.text(148, 820, this.value);
            this.valueText.fill = "white";
            this.valueText.fontSize = 40;
            this.valueText.anchor.set(0.5, 0.5);
            this.container.add(this.valueText);
            this.barTexts = [];

            for (var i = 0; i < this.barValues.length; i++) {
                this.barTexts.push(SB.game.add.text(160, 60 + i * 160, this.barValues[i]));
                if (i === 0) { this.barTexts[i].position.x -= 60; }
                this.barTexts[i].fill = "yellow";
                this.barTexts[i].stroke = "#000000";
                this.barTexts[i].strokeThickness = 6;
                this.container.add(this.barTexts[i]);
            }

            this.slider = SB.game.add.sprite(60, 715, "sprites", "icon-chip");
            this.slider.inputEnabled = true;
            this.slider.input.enableDrag();
            this.container.add(this.slider);

            this.decrement = this.container.add(SB.game.add.button(122, 840, "sprites", this.onDecrement, this, "icon-bet-decrease-normal", "icon-bet-decrease-normal", "icon-bet-decrease-down"));
            this.increment = this.container.add(SB.game.add.button(122, 750, "sprites", this.onIncrement, this, "icon-bet-increase-normal", "icon-bet-increase-normal", "icon-bet-increase-down"));
            this.container.position.set (1628, 25);

            this.slider.events.onDragUpdate.add(this.slide.bind(this));
        },
        onIncrement: function () {
            this.value += SB.Globals.blind;
            this.value = (this.value < this.max) ? this.value : this.max;
            this.value = Math.floor(this.value / SB.Globals.blind) * SB.Globals.blind;
            this.valueText.text = this.value;
            this.slider.position.y = 75 + ((this.max - this.value) / (this.max - this.min)) * 640;
        },
        onDecrement: function () {
            this.value -= SB.Globals.blind;
            this.value = (this.value < this.min) ? this.min : this.value;
            this.value = Math.ceil(this.value / SB.Globals.blind) * SB.Globals.blind;
            this.valueText.text = this.value;
            this.slider.position.y = 75 + ((this.max - this.value) / (this.max - this.min)) * 640;
        },
        slide: function(sprite, pointer, dragX, dragY, snapPoint) {
            this.slider.position.x = 60;
            if (this.slider.position.y < 75) {
                this.slider.position.y = 75;
            } else if (this.slider.position.y > 715) {
                this.slider.position.y = 715;
            }

            this.value = Math.floor(this.max - (((this.slider.position.y - 75) / 640) * (this.max - this.min)));
            this.valueText.text = this.value;
        },
        showPanel:  function (minValue, maxValue) {
            this.container.visible = true;
            this.min = minValue;
            this.max = maxValue;
            this.value = this.min;
            this.valueText.text = this.value;
            this.slider.position.y = 75 + ((this.max - this.value) / (this.max - this.min)) * 640;

            if (maxValue < minValue) {
                this.min = maxValue;
            }

            this.decrement.visible = (this.min !== this.max);
            this.increment.visible = (this.min !== this.max);

            this.barValues = ["ALL IN", Math.ceil(this.max * 0.75 / SB.Globals.blind) * SB.Globals.blind, Math.ceil(this.max * 0.5 / SB.Globals.blind) * SB.Globals.blind, Math.ceil(this.max * 0.25 / SB.Globals.blind) * SB.Globals.blind];
            for (var i = 1; i < this.barValues.length; i++) {
                this.barTexts[i].text = this.barValues[i];
            }
        },
        hidePanel: function () {
            this.container.visible = false;
        }
    };

    return SB.BettingPanel;
});
