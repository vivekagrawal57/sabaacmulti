define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.FeedbackObject = function() {
        this.create();
    };

    SB.FeedbackObject.prototype = {
        message: null,

        create: function() {
            this.container = SB.game.add.group();

            this.sprite = SB.game.add.sprite(0, 0, "sprites", "message-bg");
            this.sprite.anchor.set(0.5, 0.5);
            this.sprite.scale.set(1, 0.75);
            this.container.add(this.sprite);

            this.message = SB.game.add.text(0, 0, "Congratulations to Player for winning 10000 with a hand of 23 points.");
            this.message.fill = "black";
            this.message.wordWrap = true;
            this.message.wordWrapWidth = 390;
            this.message.stroke = "#00FFFF";
            this.message.strokeThickness = 6;
            this.message.fontSize = 28;
            this.message.anchor.set(0.5, 0.5);
            this.container.add(this.message);

            this.container.position.set(257, 480);
            this.container.visible = false;
        },

        show: function(msg, lines) {
            lines = lines || 1;
            switch (lines) {
                case 1:
                    this.sprite.scale.set(1, 0.75);
                    break;
                case 2:
                    this.sprite.scale.set(1, 0.8);
                    break;
                case 3:
                    this.sprite.scale.set(1, 1.1);
                    break;
                case 4:
                    this.sprite.scale.set(1, 1.4);
                    break;
                case 5:
                    this.sprite.scale.set(1, 1.75);
                    break;
            }
            this.container.visible = true;
            this.message.text = msg;
            this.tween = SB.game.add.tween(this.container).from( { x: -400 }, 500, "Linear", true);
            this.tween.onComplete.add(function() {
                SB.game.tweens.remove(this.tween);
                this.tween = null;
            }.bind(this));
        },

        hide: function() {
            this.container.visible = false;
        }
    };

    return SB.FeedbackObject;
});



