define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.Card = function(value, suit, points) {
        this.name = "";
        this.value = value;
        this.suit = suit;
        this.points = points;
        this.isInterferenced = false;
    };

    SB.Card.prototype = {
        updateCard: function(value, suit, points) {
            this.value = value;
            this.suit = suit;
            this.points = points;
        },
        cardToString: function() {
            switch (this.value) {
                case "1" :
                    this.name = "1 of ";
                    break;
                case "2" :
                    this.name = "2 of ";
                    break;
                case "3" :
                    this.name = "3 of ";
                    break;
                case "4" :
                    this.name = "4 of ";
                    break;
                case "5" :
                    this.name = "5 of ";
                    break;
                case "6" :
                    this.name = "6 of ";
                    break;
                case "7" :
                    this.name = "7 of ";
                    break;
                case "8" :
                    this.name = "8 of ";
                    break;
                case "9" :
                    this.name = "9 of ";
                    break;
                case "10" :
                    this.name = "10 of ";
                    break;
                case "11" :
                    this.name = "11 of ";
                    break;
                case "C":
                    this.name = "Commander of ";
                    break;
                case "MI" :
                    this.name = "Mistress of ";
                    break;
                case "MA" :
                    this.name = "Master of ";
                    break;
                case "A" :
                    this.name = "Ace of ";
                    break;
                case "I" :
                    this.name = "The Idiot (0)";
                    break;
                case "Q" :
                    this.name = "Queen of Air and Darkness (-2)";
                    break;
                case "E" :
                    this.name = "Endurance (-8)";
                    break;
                case "B" :
                    this.name = "Balance (-11)";
                    break;
                case "D" :
                    this.name = "Demise (-13)";
                    break;
                case "M" :
                    this.name = "Moderation (-14)";
                    break;
                case "Ev" :
                    this.name = "The Evil One (-15)";
                    break;
                case "S" :
                    this.name = "The Star (-17)";
                    break;
            }

            switch (this.suit) {
                case "Sb" :
                    this.name += "Sabres";
                    break;
                case "St" :
                    this.name += "Staves";
                    break;
                case "Co" :
                    this.name += "Coins";
                    break;
                case "Fl" :
                    this.name += "Flasks";
                    break;
            }
        }
    };

    return SB.Card;
});





