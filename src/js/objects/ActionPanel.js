define([
    'phaser',
    'js/objects/ui/BettingUIButton.js',
    'js/objects/ui/BettingUIToggleButton.js'
], function (
    Phaser,
    BettingUIButton,
    BettingUIToggleButton
) {
    'use strict';

    SB.ActionPanel = function(callback) {
        this.create(callback);
    };

    SB.ActionPanel.prototype = {
        create: function(callback) {
            this.container = SB.game.add.group();
            var bg = SB.game.add.sprite(-45, 0, "sprites", "action-panel-bg")
            bg.scale.set(1.2, 1);
            this.container.add(bg);

            this.container.position.set (1195, 940);
            this.btnsContainer = SB.game.add.group();

            // Creating All action buttons
            // Creating Betting Buttons
            this.callallBtn = new SB.BettingUIToggleButton("callall", { x: 0, y: 0 }, { x: 0, y: -20 }, callback);
            this.checkBtn = new SB.BettingUIButton("check", { x: 0, y: 0 }, { x: 0, y: -20 }, callback);
            this.callBtn = new SB.BettingUIButton("call", { x: 0, y: 0 }, { x: 0, y: -17 }, callback);
            this.foldBtn = new SB.BettingUIButton("fold", { x: 0, y: 0 }, { x: 2, y: -20 }, callback);
            this.allinBtn = new SB.BettingUIButton("allin", { x: 0, y: 0 }, { x: -2, y: -17 }, callback);
            this.raiseBtn = new SB.BettingUIButton("raise", { x: 0, y: 0 }, { x: 2, y: -20 }, callback);
            this.betBtn = new SB.BettingUIButton("bet", { x: 0, y: 0 }, { x: 2, y: -20 }, callback);
            this.checkFoldBtn = new SB.BettingUIToggleButton("check-fold", { x: 0, y: 0 }, { x: 2, y: -20 }, callback);
            this.confirmBtn = new SB.BettingUIToggleButton("confirm", { x: 0, y: 0 }, { x: 2, y: -20 }, callback);

            // Creating Trading Buttons
            this.alderaanBtn = new SB.BettingUIButton("alderaan", { x: 0, y: 0 }, { x: 0, y: -16 }, callback);
            this.drawBtn = new SB.BettingUIButton("draw", { x: 0, y: 0 }, { x: -1, y: -16 }, callback);
            this.tradeBtn = new SB.BettingUIButton("trade", { x: 0, y: 0 }, { x: 0, y: -16 }, callback);
            this.protectBtn = new SB.BettingUIButton("protect", { x: 0, y: 0 }, { x: 2, y: -18 }, callback);
            this.standBtn = new SB.BettingUIButton("stand", { x: 0, y: 0 }, { x: 2, y: -16 }, callback);
            this.backBtn = new SB.BettingUIButton("back", { x: 0, y: 0 }, { x: 2, y: -20 }, callback);

            this.btnsContainer.add(this.callallBtn.get());
            this.btnsContainer.add(this.checkBtn.get());
            this.btnsContainer.add(this.callBtn.get());
            this.btnsContainer.add(this.foldBtn.get());
            this.btnsContainer.add(this.allinBtn.get());
            this.btnsContainer.add(this.raiseBtn.get());
            this.btnsContainer.add(this.alderaanBtn.get());
            this.btnsContainer.add(this.drawBtn.get());
            this.btnsContainer.add(this.tradeBtn.get());
            this.btnsContainer.add(this.betBtn.get());
            this.btnsContainer.add(this.checkFoldBtn.get());
            this.btnsContainer.add(this.protectBtn.get());
            this.btnsContainer.add(this.standBtn.get());
            this.btnsContainer.add(this.backBtn.get());
            this.btnsContainer.add(this.confirmBtn.get());
            this.container.add(this.btnsContainer)
            this.btnsContainer.scale.set (0.9, 0.9);
            this.btnsContainer.position.set (40, 10);
        },
        showPanel: function (btns) {
            this.container.visible = true;
            var startPos, increment;
            switch  (btns.length) {
                case 1:
                    startPos = 370;
                    increment = 300;
                    break;
                case 2:
                    startPos = 200;
                    increment = 300;
                    break;
                case 3:
                    startPos = 160;
                    increment = 205;
                    break;
                case 4:
                    startPos = 110;
                    increment = 170;
                    break;
                case 5:
                    startPos = 45;
                    increment = 160;
                    break;
            }

            for (var i = 0; i < btns.length; i++) {
                this[btns[i]].get().visible = true;
                this[btns[i]].get().position.set(startPos + increment * i, 100);
                if (this[btns[i]].state === false) {
                    this[btns[i]].toggle();
                }
            }
        },
        hidePanel: function() {
            this.container.visible = false;
            this.callallBtn.get().visible = false;
            this.checkBtn.get().visible = false;
            this.callBtn.get().visible = false;
            this.foldBtn.get().visible = false;
            this.allinBtn.get().visible = false;
            this.raiseBtn.get().visible = false;
            this.alderaanBtn.get().visible = false;
            this.drawBtn.get().visible = false;
            this.tradeBtn.get().visible = false;
            this.betBtn.get().visible = false;
            this.checkFoldBtn.get().visible = false;
            this.protectBtn.get().visible = false;
            this.standBtn.get().visible = false;
            this.backBtn.get().visible = false;
            this.confirmBtn.get().visible = false;
        }
    };

    return SB.ActionPanel;
});