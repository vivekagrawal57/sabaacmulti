define([
    'phaser',
    'js/objects/ui/Loading.js',
], function (
    Phaser,
    Loading
) {
    'use strict';

    SB.Shop = function(callback) {
        this.callback = callback;
        this.create();
    };

    SB.Shop.prototype = {
        create: function() {
            this.container = SB.game.add.group();
            var bmd = SB.game.add.bitmapData(1920, 1080);
            bmd.ctx.beginPath();
            bmd.ctx.rect(0, 0, 1920, 1080);
            bmd.ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
            bmd.ctx.fill();

            var cover = SB.game.add.sprite(0, 0, bmd);
            cover.anchor.set(0.5, 0.5);
            cover.inputEnabled = true;

            var bg = SB.game.add.sprite(0, 0, "shop-bg")
            bg.anchor.set(0.5, 0.5);

            var closeBtn = SB.game.add.button(770, -460, "sprites2", this.hide, this, "lobby-exit-normal", "lobby-exit-normal", "lobby-exit-down");
            closeBtn.scale.set(0.75, 0.75);
            closeBtn.anchor.set(0.5, 0.5);

            this.creditsValue = SB.game.add.text(-365, -355, "100000");
            this.creditsValue.fill = "black";
            this.creditsValue.font = 'MyFont';
            this.creditsValue.stroke = "#fee17c";
            this.creditsValue.strokeThickness = 3;
            this.creditsValue.fontSize = 42;
            this.creditsValue.anchor.set(0, 0.5);

            this.errorMsg = SB.game.add.text(0, 350, "Purchase Failed");
            this.errorMsg.fill = "black";
            this.errorMsg.stroke = "#00FFFF";
            this.errorMsg.strokeThickness = 6;
            this.errorMsg.fontSize = 40;
            this.errorMsg.anchor.set(0.5, 0.5);
            this.errorMsg.alpha = 0;

            this.btn300 = SB.game.add.button(-480, 100, "sprites2", this.purchaseCredits, this, "shop-buy-normal", "shop-buy-normal", "shop-buy-down");
            this.btn300.name = "300";
            this.btn300.anchor.set(0.5, 0.5);

            this.btn650 = SB.game.add.button(0, 100, "sprites2", this.purchaseCredits, this, "shop-buy-normal", "shop-buy-normal", "shop-buy-down");
            this.btn650.name = "650";
            this.btn650.anchor.set(0.5, 0.5);

            this.btn1000 = SB.game.add.button(480, 100, "sprites2", this.purchaseCredits, this, "shop-buy-normal", "shop-buy-normal", "shop-buy-down");
            this.btn1000.name = "1000";
            this.btn1000.anchor.set(0.5, 0.5);

            this.container.add(cover);
            this.container.add(bg);
            this.container.add(closeBtn);
            this.container.add(this.creditsValue);
            this.container.add(this.errorMsg);
            this.container.add(this.btn300);
            this.container.add(this.btn650);
            this.container.add(this.btn1000);
            this.container.position.set (960, 540);

            this.loadingCircle = new SB.Loading();

            if (!SB.game.device.desktop)
                this.setupInAPP();
        },

        setupInAPP: function() {
            var productIds = [
                "com.sabacc.300",
                "com.sabacc.650",
                "com.sabacc.1000"
            ];

            this.inappsService = Cocoon.InApp;
            this.inappsService.on("purchase", {
                start: function(productId) {
                    this.loadingCircle.show();
                    this.productId = productId;
                    console.log("purchase started " +  this.productId);
                }.bind(this),
                error: function(productId, error) {
                    this.loadingCircle.hide();
                    console.log("purchase failed " +  this.productId + " error: " + JSON.stringify(error));
                }.bind(this),
                complete: function(purchase) {
                    this.purchasing(this.productId);
                    console.log("purchase completed " + JSON.stringify(purchase));
                }.bind(this)
            });

            // Service initialization
            this.inappsService.initialize({}, function(error){
                    this.inappsService.fetchProducts(productIds, function(products, error){
                        for (var i = 0; i < products.length; ++i) {
                            var product = products[i];
                            console.log(product.title);
                            console.log(product.description);
                            console.log(product.localizedPrice);
                        }
                    });
                }.bind(this)
            );
        },

        purchaseCredits: function(btn) {
            var value = parseInt(btn.name);

            var productID;
            switch (value) {
                case 300:
                    productID = "com.sabacc.300";
                    break;
                case 650:
                    productID = "com.sabacc.650";
                    break;
                case 1000:
                    productID = "com.sabacc.1000";
                    break;
            }
            this.inappsService.purchase(productID, 1, function(error) {

            });

        },

        purchasing: function(value) {
            switch (value) {
                case "com.sabacc.300":
                    value = 300;
                    break;
                case "com.sabacc.650":
                    value = 650;
                    break;
                case "com.sabacc.1000":
                    value = 1000;
                    break;
            }
            var data = {userid: SB.CurrentUser.id, credits: value};
            $.ajax({
                type: "POST",
                dataType: "json",
                url: SB.API_URL + "add.credits.api.php",
                data: {'data': JSON.stringify(data)},
                success: function(data) {
                    this.loadingCircle.hide();
                    if (this.callback) {
                        this.callback(value);
                    }
                }.bind(this),
                error: function() {
                    this.loadingCircle.hide();
                    this.showError("Something went wrong with the Internet");
                }.bind(this)
            });
        },

        show: function () {
            this.creditsValue.text = SB.CurrentUser.credits;
            this.container.visible = true;
        },

        hide: function () {
            this.container.visible = false;
        }
    };

    return SB.Shop;
});
