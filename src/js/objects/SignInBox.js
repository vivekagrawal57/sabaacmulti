define([
    'phaser',
    'js/objects/ui/InputBox.js',
    'js/objects/ui/MenuButton.js',
    'js/objects/ui/Loading.js',
], function (
    Phaser,
    InputBox,
    MenuButton,
    Loading
) {
    'use strict';

    SB.SignInBox = function() {
        this.create();
    };

    SB.SignInBox.prototype = {
        create: function() {
            this.container = SB.game.add.group();
            var bmd = SB.game.add.bitmapData(1920, 1080);
            bmd.ctx.beginPath();
            bmd.ctx.rect(0, 0, 1920, 1080);
            bmd.ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
            bmd.ctx.fill();

            var cover = SB.game.add.sprite(0, 90, bmd);
            cover.anchor.set(0.5, 0.5);
            cover.inputEnabled = true;
            this.container.add(cover);

            var signInBG = SB.game.add.sprite(0, 0, "sprites2", "tutorial-bg")
            signInBG.anchor.set(0.5, 0.5);
            this.container.add(signInBG);

            this.loginBox = new SB.LoginBox(this.loginPlayer.bind(this), this.loginFB.bind(this), this.loginDevice.bind(this));
            this.loginBtn = new SB.MenuButton("big-ui-button", "login-register", this.loginBox.showLogin.bind(this.loginBox), { x: -200, y: -75 }, this.container, 1);
            this.fbLoginBtn = new SB.MenuButton("big-ui-button", "facebook-login", this.facebookLogin.bind(this), { x: 200, y: -75 }, this.container, 1);
            this.guestLoginBtn = new SB.MenuButton("big-ui-button", "guest-login", this.guestLogin.bind(this), { x: 0, y: 75 }, this.container, 1);

            var closeBtn = SB.game.add.button(370, -220, "sprites2", this.hide, this, "lobby-exit-normal", "lobby-exit-normal", "lobby-exit-down");
            closeBtn.scale.set(0.75, 0.75);

            this.errorMsg = SB.game.add.text(0, 0, "Something wrong with internet");
            this.errorMsg.fill = "black";
            this.errorMsg.stroke = "#00FFFF";
            this.errorMsg.strokeThickness = 6;
            this.errorMsg.fontSize = 28;
            this.errorMsg.anchor.set(0.5, 0.5);
            this.errorMsg.alpha = 0;

            this.container.add(closeBtn);
            this.container.add(this.errorMsg);

            this.container.position.set(960, 450);
            this.loadingCircle = new SB.Loading();

            if (!SB.game.device.desktop)
                this.setupFacebook();

            this.hide();
        },

        loginPlayer: function(data) {
            SB.CurrentUser = {
                id: data["data"]["user_id"],
                username: data["data"]["username"],
                credits: parseInt(data["data"]["credits"]),
                fbLogin: false,
                guestLogin: false
            }

            SB.game.state.start('Lobby');
        },

        loginFB: function(data) {
            SB.CurrentUser = {
                id: data["user_id"],
                username: data["username"],
                credits: parseInt(data["credits"]),
                fbLogin: true,
                guestLogin: false
            }

            SB.game.state.start('Lobby');
        },

        facebookLogin: function() {
            this.fbService.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    this.loginBox.loggingFacebook(response);
                } else {
                    this.socialAPI.login(function(response, error){
                        if (response.status === 'connected') {
                            console.log("Login status: " + JSON.stringify(response));
                            this.loginBox.loggingFacebook(response);
                        }
                    });
                }
            }.bind(this));
        },

        setupFacebook: function() {
            this.fbService = Cocoon.Social.Facebook;
            this.socialAPI = null;
            this.fbService.init({
                appId      : "1156812971047963",
                xfbml      : true,
                version    : 'v2.5'

            }, function () {
                this.socialAPI = this.fbService.getSocialInterface();
            }.bind(this));
        },

        guestLogin: function() {
            this.loginBox.loggingDevice(Cocoon.Device.getDeviceId());
        },

        loginDevice: function(data) {
            SB.CurrentUser = {
                id: data["user_id"],
                username: data["username"],
                credits: parseInt(data["credits"]),
                fbLogin: false,
                guestLogin: true
            }

            console.log(JSON.stringify(SB.CurrentUser));

            SB.game.state.start('Lobby');
        },

        showError: function (text) {
            this.errorMsg.text = text;
            if (this.errorTween) {
                this.errorTween.stop();
                SB.game.tweens.remove(this.errorTween);
                this.errorTween = null;
            }
            this.errorMsg.alpha = 1;
            this.errorTween = SB.game.add.tween(this.errorMsg).to( { alpha: 0 }, 1000, "Linear", true, 2000);
        },

        show: function () {
            this.container.visible = true;
        },

        hide: function () {
            this.container.visible = false;
        }
    };

    return SB.SignInBox;
});

