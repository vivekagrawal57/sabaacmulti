define([
    'phaser',
    'js/objects/ui/Loading.js',
], function (
    Phaser,
    Loading
) {
    'use strict';

    SB.Ads = function() {
        this.create();
    };

    SB.Ads.prototype = {
        create: function() {
            this.loadingCircle = new SB.Loading();

            if (!SB.game.device.desktop)
                this.setupAds();
        },

        setupAds: function() {
                console.log("Ads were created");
                this.adService = Cocoon.Ad.Chartboost;

                this.adService.configure({
                    ios: {
                        appId:"571ce6d743150f6f79336a5e",
                        appSignature:"d870336b516191ecc0a0489cd50902b3c5e2572a"
                    },
                    android: {
                        appId:"571ce81504b01670d9273345",
                        appSignature:"31dca2bc2823960f369846c5479cd8b59bd71e84"
                    }
                });

                this.interstitial = this.adService.createInterstitial("572bb7eb04b0167808519830");

                this.interstitial.on("load", function(){
                    console.log("Interstitial loaded");
                    this.interstitial.show();
                }.bind(this));
                this.interstitial.on("fail", function(){
                    console.log("Interstitial failed");
                    this.loadingCircle.hide();
                    this.callback();
                }.bind(this));
                this.interstitial.on("show", function(){
                    console.log("Interstitial shown");
                }.bind(this));
                this.interstitial.on("dismiss", function(){
                    console.log("Interstitial dismissed");
                    this.callback();
                }.bind(this));

                this.rewardedVideo = this.adService.createRewardedVideo("572bb92304b0167804672e57");
                this.rewardedVideo.on("load", function(){
                    this.rewardedVideo.show();
                    console.log("Rewarded Video loaded");
                }.bind(this));

                this.rewardedVideo.on("fail", function(){
                    console.log("Rewarded Video failed");
                });

                this.rewardedVideo.on("show", function(){
                    console.log("Rewarded Video shown");
                });

                this.rewardedVideo.on("dismiss", function(){
                    console.log("Rewarded Video dismissed");
                });

                this.rewardedVideo.on("click", function(){
                    console.log("Rewarded Video clicked");
                });

                this.rewardedVideo.on("reward", function(){
                    console.log("Reward completed");
                });
        },

        loadAds: function(type, callback) {
            console.log("ads were loaded");
            this.callback = callback;
            if (type == "interstitial") {
                console.log("ads were loaded2");
                this.interstitial.load();
            } else {
                console.log("ads were loaded3");
                this.rewardedVideo.load();
            }
        },
    };

    return SB.Ads;
});
