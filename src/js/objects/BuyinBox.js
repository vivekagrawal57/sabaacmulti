define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.BuyinBox = function(callback) {
        this.callback = callback;
        this.create();
    };

    SB.BuyinBox.prototype = {
        min: 0,
        max: 10000,
        value: 0,

        create: function() {
            this.container = SB.game.add.group();
            this.bg = SB.game.add.sprite(0, 0, "sprites2", "buyin-bg");
            this.bg.anchor.set(0.5, 0.5);
            this.container.add(this.bg);

            this.bar = SB.game.add.sprite(-628, -35, "sprites2", "buyin-bar");
            this.bar.scale.set(0, 1);
            this.container.add(this.bar);

            this.valueBg = SB.game.add.group();
            var label = SB.game.add.sprite(0, 0, "sprites2", "buyin-label");
            label.anchor.set(0.5, 0.5);
            this.valueBg.add(label);
            this.valueText = SB.game.add.text(0, 20, this.value);
            this.valueText.fill = "white";
            this.valueText.fontSize = 40;
            this.valueText.anchor.set(0.5, 0.5);
            this.valueBg.add(this.valueText);
            this.valueBg.position.set(0, 40);
            this.container.add(this.valueBg);
            this.barTexts = [];

            this.minText = SB.game.add.text(-690, -50, "0");
            this.minText.fill = "#ffda5b";
            this.minText.fontSize = 40;

            this.maxText = SB.game.add.text(600, -50, "10000");
            this.maxText.fill = "#ffda5b";
            this.maxText.fontSize = 40;

            this.container.add(this.minText);
            this.container.add(this.maxText);

            this.slider = SB.game.add.sprite(0, -25, "sprites2", "buyin-slider");
            this.slider.anchor.set(0.5, 0.5);
            this.slider.inputEnabled = true;
            this.slider.input.enableDrag();
            this.container.add(this.slider);
            this.container.position.set (960, 540);

            this.slider.position.x = -630;
            this.valueBg.position.x = this.slider.position.x;

            this.slider.events.onDragUpdate.add(this.slide.bind(this));

            var closeBtn = SB.game.add.button(770, -350, "sprites2", this.hidePanel, this, "lobby-exit-normal", "lobby-exit-normal", "lobby-exit-down");
            closeBtn.scale.set(0.75, 0.75);
            closeBtn.anchor.set(0.5, 0.5);

            new SB.MenuButton("table-menu", "buyin-icon", this.addCredits.bind(this), { x: -200, y: 200 }, this.container);

            new SB.MenuButton("table-menu", "cancel", this.hidePanel.bind(this), { x: 200, y: 200 }, this.container);

            this.container.add(closeBtn);

            this.hidePanel();
        },

        addCredits: function() {
            this.callback(this.value);
            this.hidePanel();
        },

        slide: function(sprite, pointer, dragX, dragY, snapPoint) {
            this.slider.position.y = -25;
            if (this.slider.position.x < -630) {
                this.slider.position.x = -630;
            } else if (this.slider.position.x > 560) {
                this.slider.position.x = 560;
            }

            this.value = Math.floor(this.min + (((this.slider.position.x + 630) / 1190) * (this.max - this.min)));
            this.valueText.text = this.value;
            this.valueBg.position.x = this.slider.position.x;

            this.bar.scale.set(((this.slider.position.x + 630) / 1190), 1);
        },

        showPanel:  function (minValue, maxValue) {
            this.maxText.text = maxValue;
            this.container.visible = true;
            this.min = minValue;
            this.max = maxValue;
            this.value = this.min;
            this.valueText.text = this.value;
            this.slider.position.x = -630;

            if (maxValue < minValue) {
                this.min = maxValue;
            }
        },

        hidePanel: function () {
            this.container.visible = false;
        }
    };

    return SB.BuyinBox;
});
