define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.BettingUIToggleButton = function(name, position, iconPosition, onClick) {
        this.create(name, position, iconPosition, onClick);
    };

    SB.BettingUIToggleButton.prototype = {
        message: null,

        create: function(name, position, iconPosition, onClick) {
            this.name = name;
            this.container = SB.game.add.group();
            this.bg = SB.game.add.sprite(position.x, position.y, "sprites", "button-bg");
            this.button = SB.game.add.sprite(iconPosition.x, iconPosition.y, "sprites", "icon-" + name + "-normal");
            this.button.inputEnabled = true;
            this.state = true;
            this.toggle = function() {
                this.state = !this.state;
                this.button.frameName = (this.state) ? "icon-" + this.name + "-normal" : "icon-" + this.name + "-down";
            }
            this.button.events.onInputUp.add(this.toggle.bind(this));
            this.button.events.onInputUp.add(onClick.bind(this));
            this.button.name = name;

            this.bg.anchor.set(0.5, 0.5);
            this.button.anchor.set(0.5, 0.5);

            this.container.add(this.bg);
            this.container.add(this.button);
        },

        get: function() {
            return this.container;
        }
    };

    return SB.BettingUIToggleButton;
});




