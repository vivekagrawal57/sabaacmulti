define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.InputBox = function(title, position, parent, isPassword) {
        this.create(title, position, parent, isPassword);
        this.actualText = ""
    };

    SB.InputBox.prototype = {
        create: function(title, position, parent, isPassword) {
            this.container = SB.game.add.group();

            this.bg = SB.game.add.sprite(0, 0, "sprites2", "input-box");
            this.bg.anchor.set(0.5, 0.5);

            this.container.add(this.bg);
            this.container.position.set(position.x, position.y);
            parent.add(this.container);

            this.bg.inputEnabled = true;

            this.text = SB.game.add.text(-200, 0, "");
            this.text.fill = "white";
            this.text.fontSize = 28;
            this.text.anchor.set(0, 0.5);

            this.container.add(this.text);

            this.bg.events.onInputUp.add(function() {
                var text = this.text;
                Cocoon.Dialog.prompt({
                    title : title,
                    text: this.actualText,
                    secureText: isPassword
                },{
                    success : function(text2){
                        if (isPassword) {
                            text.text = new Array(text2.length + 1).join( "*" )
                        } else {
                            text.text = text2;
                        }
                        this.actualText = text2;
                    }.bind(this),
                    cancel : function(){  }
                });
            }, this);
        },

        getText: function () {
            return this.actualText;
        }
    };

    return SB.InputBox;
});
