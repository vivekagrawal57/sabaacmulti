define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.SoundButton = function() {
        this.create();
    };

    SB.SoundButton.prototype = {
        create: function(position) {
            this.container = SB.game.add.group();
            var bg = SB.game.add.sprite(0, 0, "sprites2", "button-round-green");
            bg.anchor.set(0.5, 0.5);

            this.soundOnBtn = SB.game.add.button(0, 0, "sprites2", this.muteSound, this, "icon-sound-on-normal", "icon-sound-on-normal", "icon-sound-on-down");
            this.soundOnBtn.anchor.set(0.5, 0.5);
            this.soundOffBtn = SB.game.add.button(0, 0, "sprites2", this.unmuteSound, this, "icon-sound-off-normal", "icon-sound-off-normal", "icon-sound-off-down");
            this.soundOffBtn.anchor.set(0.5, 0.5);

            this.container.add(bg);
            this.container.add(this.soundOffBtn);
            this.container.add(this.soundOnBtn);
            this.container.position.set(75, 1000);

            if (localStorage.getItem("Mute") === "0") {
                this.muteSound();
            } else {
                this.unmuteSound();
            }
        },

        muteSound: function() {
            SB.game.sound.mute = true;
            this.soundOffBtn.visible = true;
            this.soundOnBtn.visible = false;
            localStorage.setItem("Mute", SB.game.sound.mute ? 0 : 1);
        },

        unmuteSound: function() {
            SB.game.sound.mute = false;
            this.soundOffBtn.visible = false;
            this.soundOnBtn.visible = true;
            localStorage.setItem("Mute", SB.game.sound.mute ? 0 : 1);
        }
    };

    return SB.SoundButton;
});