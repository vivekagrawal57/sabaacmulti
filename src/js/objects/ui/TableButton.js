define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.TableButton = function(buyin, position, counter, tableNo, shopFunction, loginFunction, parent) {
        this.buyin = buyin;
        this.tableNo = tableNo;
        this.position = position;
        this.counter = counter;
        this.shopFunction = shopFunction;
        this.loginFunction = loginFunction;
        this.parent = parent;

        this.create();
    };

    SB.TableButton.prototype = {
        create: function() {
            this.container = SB.game.add.group();
            this.bg = SB.game.add.sprite(0, 0, "sprites2", "tablelist-bg");

            this.buyinText = SB.game.add.text(131, 120, this.buyin);
            this.buyinText.anchor.set(0.5, 0.5);
            this.buyinText.font = 'MyFont';
            this.buyinText.fontWeight = 'normal';
            this.buyinText.fontSize = 38;
            this.buyinText.fill = '#FFFFFF';

            this.chipCounter = SB.game.add.sprite(80, 205, "sprites", this.counter)

            this.button = SB.game.add.button(0, 355, "sprites2", this.joinTable, this, "play-normal", "play-normal", "play-down");
            this.button.name = name;
            this.container.add(this.bg);
            this.container.add(this.buyinText);
            this.container.add(this.chipCounter);
            this.container.add(this.button);
            this.container.position.set(this.position.x, this.position.y);

            this.parent.add(this.container);
        },

        joinTable: function() {
            if (this.buyin <= SB.CurrentUser.credits) {
                SB.tableNo = this.tableNo;
                SB.game.state.start('MultiGame');
            } else {
                if (SB.CurrentUser.id !== -1000)
                    this.shopFunction();
                else
                    this.loginFunction();
            }
        }
    };

    return SB.TableButton;
});




