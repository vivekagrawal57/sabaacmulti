define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.BettingUIButton = function(name, position, iconPosition, onClick) {
        this.create(name, position, iconPosition, onClick);
    };

    SB.BettingUIButton.prototype = {
        create: function(name, position, iconPosition, onClick) {
            this.container = SB.game.add.group();
            this.bg = SB.game.add.sprite(position.x, position.y, "sprites", "button-bg");
            this.button = SB.game.add.button(iconPosition.x, iconPosition.y, "sprites", onClick, this, "icon-" + name + "-normal", "icon-" + name + "-normal", "icon-" + name + "-down");
            this.button.name = name;

            this.bg.anchor.set(0.5, 0.5);
            this.button.anchor.set(0.5, 0.5);

            this.container.add(this.bg);
            this.container.add(this.button);
        },

        get: function() {
            return this.container;
        }
    };

    return SB.BettingUIButton;
});



