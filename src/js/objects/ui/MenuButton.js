define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.MenuButton = function(button, icon, onClick, position, parent, scale) {
        this.create(button, icon, onClick, position, parent, scale);
    };

    SB.MenuButton.prototype = {
        create: function(button, icon, onClick, position, parent, scale) {
            scale = scale || 1;
            this.container = SB.game.add.group();
            this.button = SB.game.add.button(0, 0, "sprites2", onClick, this, button + "-normal", button + "-normal", button + "-down");
            this.icon = SB.game.add.sprite(0, 0, "sprites2", icon);

            this.button.anchor.set(0.5, 0.5);
            this.icon.anchor.set(0.5, 0.5);

            this.container.position.set(position.x, position.y)
            this.container.add(this.button);
            this.container.add(this.icon);
            this.container.scale.set(scale, scale);
            parent.add(this.container);
        },

        get: function() {
            return this.container;
        }
    };

    return SB.MenuButton;
});




