define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.Loading = function() {
        this.create();
    };

    SB.Loading.prototype = {
        create: function() {
            this.container = SB.game.add.group();
            var bmd = SB.game.add.bitmapData(1920, 1080);
            bmd.ctx.beginPath();
            bmd.ctx.rect(0, 0, 1920, 1080);
            bmd.ctx.fillStyle = 'rgba(0, 0, 0, 0.5)';
            bmd.ctx.fill();

            var cover = SB.game.add.sprite(960, 540, bmd);
            cover.anchor.set(0.5, 0.5);
            cover.inputEnabled = true;

            var circle = SB.game.add.sprite(960, 540, "sprites2", "loading");
            circle.anchor.set(0.5, 0.5);

            this.loading = SB.game.add.tween(circle).to( { rotation: 6.28 }, 1500, "Linear", true, 0, -1);

            this.hide();

            this.container.add(cover);
            this.container.add(circle);
        },

        show: function() {
            this.loading.resume();
            this.container.visible = true;
        },

        hide: function() {
            this.loading.pause();
            this.container.visible = false;
        }
    };

    return SB.Loading;
});
