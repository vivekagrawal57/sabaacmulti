define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.WinnerMessage = function(replayFunction, returnFunction) {
        this.replayFunction = replayFunction;
        this.returnFunction = returnFunction;
        this.create();
    };

    SB.WinnerMessage.prototype = {
        message: null,

        create: function() {
            this.container = SB.game.add.group();

            this.sprite = SB.game.add.sprite(0 , 0, "sprites", "server-message");
            this.sprite.anchor.set(0.5, 0.5);
            this.sprite.scale.set(1.33, 1.33);
            this.container.add(this.sprite);

            this.message = SB.game.add.text(0, -100, "You Win!");
            this.message.fill = "black";
            this.message.wordWrap = true;
            this.message.wordWrapWidth = 600;
            this.message.fontSize = 50;
            this.message.stroke = "#00FFFF";
            this.message.strokeThickness = 6;
            this.message.anchor.set(0.5, 0.5);
            this.message.align = "center";
            this.container.add(this.message);

            this.container.position.set(960, 480);

            this.returnBtn = SB.game.add.button(150, 100, "sprites", this.returnFunction, this, "rtlobby-normal", "rtlobby-normal", "rtlobby-over");
            this.returnBtn.anchor.set(0.5, 0.5);
            this.container.add(this.returnBtn);

            this.replayBtn = SB.game.add.button(-150, 100, "sprites", this.replayFunction, this, "replay-normal", "replay-normal", "replay-down");
            this.replayBtn.anchor.set(0.5, 0.5);
            this.container.add(this.replayBtn);

            this.hide();
        },

        show: function(win) {
            var msg;
            if (win) {
                msg = "You Win!";
            } else {
                msg = "You're bust, guess we'll have to take your ship!"
            }
            this.container.visible = true;
            this.message.text = msg;
        },

        hide: function() {
            this.container.visible = false;
        }
    };

    return SB.WinnerMessage;
});




