define([
    'phaser',
    'js/objects/Timer.js'
], function (
    Phaser,
    Timer
) {
    'use strict';

    SB.PlayerPanel = function(playerPos, cardCallback) {
        this.playerPositon = playerPos;
        this.cardCallback = cardCallback;
        this.create();
    };

    SB.PlayerPanel.prototype = {
        chipPositions: [
            { x: 1040, y: 615 },
            { x: 558, y: 653 },
            { x: 345, y: 390 },
            { x: 940, y: 290 },
            { x: 1307, y: 332 },
            { x: 1490, y: 557 }
        ],
        create: function() {
            this.holder = SB.game.add.group();
            this.bg = SB.game.add.sprite(0, 0, "sprites", "player-bg");
            if (this.playerPosition === 1) {
                this.avatar = SB.game.add.sprite(17, 42, "sprites", "avatar-2");
            } else {
                this.avatar = SB.game.add.sprite(17, 42, "sprites", "avatar-1");
            }

            this.pointsPanel = SB.game.add.group();
            this.pointsPanel.create(10, 140, "sprites", "point-value");

            this.timer = new SB.Timer();

            // Creating Cards in player hands

            this.handCards = SB.game.add.group();
            this.handCardsArray = [];

            this.interferenceCards = SB.game.add.group();
            this.interferenceCardsArray = [];

            var n = 5;
            var startPosition = (n % 2 == 0) ? 80 : 130;
            startPosition -= (Math.ceil(n/2) - 1) * 100;

            var i, card;

            for (i = 0; i < n; i++) {
                card = SB.game.add.sprite(startPosition + (i * 100), 0, "sprites", "card-back");
                card.positionInHand = i;
                card.alpha = 0;
                if (this.playerPositon === 1) {
                    card.inputEnabled = true;
                    card.events.onInputUp.add(this.cardCallback);
                }

                this.handCardsArray.push(card);
                this.handCards.add(card);
            }

            for (i = 0; i < 2; i++) {
                card = SB.game.add.sprite(startPosition + (i * 100), 0, "sprites", "interference-card");
                if (this.playerPositon === 4) {
                    card.position.x -= (i === 0) ? 150 : -150;
                }
                card.positionInHand = i;
                this.interferenceCardsArray.push(card);
                this.interferenceCards.add(card);
            }

            this.handCards.scale.set(0.6, 0.6);
            this.interferenceCards.scale.set(0.6, 0.6);

            // Creating Bet Counter

            this.bet = SB.game.add.group();
            this.betBg = SB.game.add.sprite(0, 0, "sprites", "player-chip");
            this.betChip = SB.game.add.sprite(0, 0, "sprites", "chip1");
            this.animationChip = SB.game.add.sprite(0, 0, "sprites", "chip1");
            this.animationChip.visible = false;
            this.betChip.scale.set(0.6, 0.6);
            this.animationChip.scale.set(0.6, 0.6);
            this.betValue = SB.game.add.text(79, 45, "100");

            this.bet.add(this.betBg);
            this.bet.add(this.betChip);
            this.bet.add(this.betValue);


            this.nameText = SB.game.add.text(225, 62, "");
            this.moneyText = SB.game.add.text(240, 120, '10000');
            this.valueText = SB.game.add.text(185, 165, 'Hand: 0');
            this.pointsPanel.add(this.valueText);

            // Setting Property of all the texts related to player

            this.betValue.anchor.set(0.5, 0.5);
            this.betValue.font = 'MyFont';
            this.betValue.fontWeight = 'bold';
            this.betValue.fontSize = 18;
            this.betValue.fill = '#FFFFFF';

            this.nameText.anchor.set(0.5, 0.5);
            this.nameText.font = 'Arial';
            this.nameText.fontWeight = 'bold';
            this.nameText.fontSize = 34;
            this.nameText.fill = '#FFFB7D';

            this.moneyText.anchor.set(0.5, 0.5);
            this.moneyText.font = 'MyFont';
            this.moneyText.fontWeight = 'normal';
            this.moneyText.fontSize = 34;
            this.moneyText.fill = '#FFFFFF';

            this.valueText.anchor.set(0.5, 0.5);
            this.valueText.font = 'MyFont';
            this.valueText.fontWeight = 'normal';
            this.valueText.fontSize = 24;
            this.valueText.stroke = "#000000";
            this.valueText.strokeThickness = 5;
            this.valueText.fill = '#FFFB7D';

            this.pointsPanel.visible = false;
            this.bet.visible = false;

            this.animation = SB.game.add.sprite(0, 0, "sprites", "sabbac");

            this.bomboutAnimation = SB.game.add.sprite(23, 46, "sprites", "lava_0000");
            this.bomboutAnimation.animations.add("Explode", Phaser.Animation.generateFrameNames("lava_", 0, 19, "", 4), 15, false);


            this.animation.alpha = 0;

            this.holder.add(this.handCards);
            this.holder.add(this.interferenceCards);
            this.holder.add(this.pointsPanel);
            this.holder.add(this.bg);
            this.holder.add(this.avatar);
            this.holder.add(this.timer.container);
            this.holder.add(this.nameText);
            this.holder.add(this.moneyText);
            this.holder.add(this.bet);
            this.holder.add(this.bomboutAnimation);
            this.holder.add(this.animation);

            // Seat player according to their positions (Player playing the game always at position 1)
            switch (this.playerPositon) {
                case 1:
                    this.holder.position.set(760, 840);
                    this.handCards.position.set(0, -160);
                    this.handCards.scale.set(1, 1);
                    this.handCards.rotation = 0;
                    this.animation.position.set(50, -140);
                    this.interferenceCards.position.set(150, -330);
                    this.interferenceCards.scale.set(1, 1);
                    this.interferenceCards.rotation = 0;
                    this.bet.position.set(280, -225);
                    break;
                case 2:
                    this.holder.position.set(150, 800);
                    this.handCards.position.set(220, -155);
                    this.handCards.rotation = 20 * (Math.PI / 180);
                    this.animation.position.set(200, -165);
                    this.interferenceCards.position.set(345, -220);
                    this.interferenceCards.rotation = 20 * (Math.PI / 180);
                    this.bet.rotation = 20 * (Math.PI / 180);
                    this.bet.position.set(420, -155);
                    break;
                case 3:
                    this.holder.position.set(150, 90);
                    this.handCards.position.set(160, 200);
                    this.handCards.rotation = -20 * (Math.PI / 180);
                    this.animation.position.set(140, 200);
                    this.interferenceCards.position.set(288, 270);
                    this.interferenceCards.rotation = -20 * (Math.PI / 180);
                    this.bet.rotation = 160 * (Math.PI / 180);
                    this.betValue.rotation = Math.PI;
                    this.betValue.position.set(79, 38);
                    this.bet.position.set(260, 345);
                    break;
                case 4:
                    this.holder.position.set(760, 40);
                    this.handCards.position.set(60, 140);
                    this.handCards.rotation = 0;
                    this.animation.position.set(40, 140);
                    this.interferenceCards.position.set(155, 250);
                    this.interferenceCards.rotation = 0;
                    this.bet.rotation = Math.PI;
                    this.betValue.rotation = Math.PI;
                    this.betValue.position.set(79, 38);
                    this.bet.position.set(240, 310);
                    break;
                case 5:
                    this.holder.position.set(1370, 90);
                    this.handCards.position.set(20, 140);
                    this.handCards.rotation = 25 * (Math.PI / 180);
                    this.animation.position.set(0, 120);
                    this.interferenceCards.position.set(60, 280);
                    this.interferenceCards.rotation = 25 * (Math.PI / 180);
                    this.bet.rotation = 205 * (Math.PI / 180);
                    this.betValue.rotation = Math.PI;
                    this.betValue.position.set(79, 38);
                    this.bet.position.set(-20, 310);
                    break;
                case 6:
                    this.holder.position.set(1370, 800);
                    this.handCards.position.set(-20, -100);
                    this.handCards.rotation = -25 * (Math.PI / 180);
                    this.animation.position.set(-40, -100);
                    this.interferenceCards.position.set(25, -230);
                    this.interferenceCards.rotation = -25 * (Math.PI / 180);
                    this.bet.rotation = -25 * (Math.PI / 180);
                    this.bet.position.set(110, -230);
                    break;
            };
            this.animation.rotation = this.handCards.rotation;
            this.hide();
        },
        getObject: function () {
            return this.holder;
        },
        assignPlayer: function(player) {
            this.player = player;
            this.nameText.text = player.name;
            this.show();
        },
        updateMoney: function () {
            this.moneyText.text = this.player.money;
            if (this.player.money === 0) {
                this.moneyText.text = "ALL IN";
            }
        },
        showBet: function () {
            if (this.player.betAmount === 0) {
                this.bet.visible = false;
                return;
            }
            this.bet.visible = true;
            this.betValue.text = this.player.betAmount;
            switch (this.betValue.text.length) {
                case 4:
                    this.betValue.fontSize = 16;
                    break;
                case 5:
                    this.betValue.fontSize = 14;
                    break;
                default :
                    this.betValue.fontSize = 18;
                    break;
            }
            this.betChip.frameName = "chip" + SB.utils.getChipColor (this.player.betAmount) + "";
        },
        updateCards: function () {
            this.handCards.visible = true;
            var n = this.player.cards.length;
            var startPosition = (n % 2 == 0) ? 80 : 130;
            startPosition -= (Math.ceil(n/2) - 1) * 100;

            for (var i = 0; i < n; i++) {
                this.handCardsArray [i].inputEnabled = true;
                this.handCardsArray [i].position.set(startPosition + (i * 100), 0);
                this.handCardsArray [i].alpha = 1;
            }

            for (i = n; i < 5; i++) {
                this.handCardsArray [i].alpha = 0;
                this.handCardsArray [i].inputEnabled = false;
            }

            this.valueText.text = "Hand: " + this.player.totalPoints;

            for (var i = 0; i < this.player.interferenceCards.length; i++) {
                var card = this.player.interferenceCards[i];
                this.interferenceCardsArray[i].frameName = card.suit + "/" + card.value + "";
                this.interferenceCardsArray[i].inputEnabled = false;
            }
        },
        fold: function() {
            SB.SFX.fold.play();
            this.player.folded = true;
            this.handCards.visible = false;
        },
        faceUpCards: function () {
            for (var i = 0; i < this.player.cards.length; i++) {
                var card = this.player.cards[i];
                this.handCardsArray[i].frameName = card.suit + "/" + card.value + "";
            }
            this.showPointPanel();
        },
        dissolveAndAppear: function (position) {
            var sprite = SB.game.add.sprite(this.handCardsArray[position].worldPosition.x * SB.xScale - 3, this.handCardsArray[position].worldPosition.y * SB.yScale - 3, "sprites", "card-yellow-border");
            var dissolveTween = SB.game.add.tween(this.handCardsArray[position]).to( { alpha: 0 }, 1000, "Linear", true, 0);
            dissolveTween.onComplete.add(function() {
                dissolveTween.stop();
                SB.game.tweens.remove(dissolveTween);
                dissolveTween = null;
                if (this.playerPositon === 1) {
                    this.faceUpCards();
                }
                dissolveTween = SB.game.add.tween(this.handCardsArray[position]).to( { alpha: 1 }, 1000, "Linear", true, 0);
                dissolveTween.onComplete.add(function() {
                    sprite.destroy();
                });
            }.bind(this));
        },
        hideCards: function () {
            var i;
            for (i = 0; i < this.player.cards.length; i++) {
                this.handCardsArray[i].frameName = "card-back";
            }
            for (i = 0; i < 2; i++) {
                this.interferenceCardsArray[i].frameName = "interference-card";
            }
        },
        hidePointsPanel: function () {
            this.pointsPanel.visible = false;
        },
        showPointPanel: function () {
            this.pointsPanel.visible = true;
            var specialCards = this.checkForSpecialCards();
            if (specialCards === 0) {
                this.valueText.text = "Hand: " + this.player.totalPoints;
            } else {
                this.valueText.text = specialCards;
            }
        },
        showWinnerStatus: function () {
            this.nameText.visible = false;
            this.bg.frameName = "player-bg-winner";
        },
        hideWinnerStatus: function () {
            this.nameText.visible = true;
            this.bg.frameName = "player-bg";
            if (this.tween)
                this.tween.stop();
            this.animation.alpha = 0;
        },
        playAnimation: function (name) {
            if (this.tween) {
                SB.game.tweens.remove(this.tween);
            }
            this.tween = null;
            this.animation.frameName = name;
            this.tween = SB.game.add.tween(this.animation).to( { alpha: 1 }, 500, "Linear", true, 0, -1);
            this.tween.yoyo(true, 1000);
            this.bomboutAnimation.animations.play("Explode");
        },
        chipToPile: function(value, sfx) {
            this.animationChip.visible = true;
            this.animationChip.frameName = "chip" + SB.utils.getChipColor (value) + "";
            if (this.chipAnimation) {
                SB.game.tweens.remove(this.chipAnimation);
                this.chipAnimation = null;
            }
            this.chipAnimation = SB.game.add.tween(this.animationChip);
            this.animationChip.position.set (this.holder.worldPosition.x * SB.xScale + 110, this.holder.worldPosition.y * SB.yScale + 85);
            this.chipAnimation.to ({x: this.chipPositions[this.playerPositon - 1].x, y: this.chipPositions[this.playerPositon - 1].y}, 250, "Linear", true);
            this.chipAnimation.onComplete.add(function() {
                this.animationChip.visible = false;
                this.showBet();
            }.bind(this));
            sfx.play();
        },
        pileToMainPot: function(value) {
            this.animationChip.visible = true;
            this.animationChip.frameName = "chip" + SB.utils.getChipColor (value) + "";
            if (this.chipAnimation) {
                SB.game.tweens.remove(this.chipAnimation);
                this.chipAnimation = null;
            }
            this.chipAnimation = SB.game.add.tween(this.animationChip);
            this.animationChip.position.set (this.chipPositions[this.playerPositon - 1].x, this.chipPositions[this.playerPositon - 1].y);
            this.chipAnimation.to ({x: 600, y: 420}, 500, "Linear", true);
            this.chipAnimation.onComplete.add(function() {
                this.animationChip.visible = false;
            }.bind(this));
            SB.SFX.movechip.play();
        },
        chipToSabacc: function (value) {
            this.animationChip.visible = true;
            this.animationChip.frameName = "chip" + SB.utils.getChipColor (value) + "";
            if (this.chipAnimation) {
                SB.game.tweens.remove(this.chipAnimation);
                this.chipAnimation = null;
            }
            this.chipAnimation = SB.game.add.tween(this.animationChip);
            this.animationChip.position.set (this.holder.worldPosition.x * SB.xScale + 110, this.holder.worldPosition.y * SB.yScale + 85);
            this.chipAnimation.to ({x: 1065, y: 430}, 500, "Linear", true);
            this.chipAnimation.onComplete.add(function() {
                this.animationChip.visible = false;
            }.bind(this));
            SB.SFX.bet.play();
        },
        bomboutPay: function(value) {
            this.animationChip.visible = true;
            this.animationChip.frameName = "chip" + SB.utils.getChipColor (value);
            if (this.chipAnimation) {
                SB.game.tweens.remove(this.chipAnimation);
                this.chipAnimation = null;
            }
            this.chipAnimation = SB.game.add.tween(this.animationChip);
            this.animationChip.position.set (this.holder.worldPosition.x * SB.xScale + 110, this.holder.worldPosition.y * SB.yScale + 85);
            this.chipAnimation.to ({x: this.chipPositions[this.playerPositon - 1].x, y: this.chipPositions[this.playerPositon - 1].y}, 250, "Linear", true);
            this.chipAnimation.onComplete.add(function() {
                this.bet.visible = true;
                this.betValue.text = value;
                this.betChip.frameName = "chip" + SB.utils.getChipColor (value);
                SB.game.tweens.remove(this.chipAnimation);
                this.chipAnimation = null;
                this.chipAnimation = SB.game.add.tween(this.animationChip);
                this.chipAnimation.to ({x: 1065, y: 430}, 250, "Linear", true, 1250);
                this.chipAnimation.onComplete.add(function() {
                    this.bet.visible = false;
                    this.animationChip.visible = false;
                }.bind(this));
            }.bind(this));
            SB.SFX.bet.play();
        },
        mainToPlayer: function (value) {
            var chipValue = SB.utils.getChipColor (value);
            for (var i = 0; i < 20; i++) {
                var chip = SB.game.add.sprite(600, 420, "sprites", "chip" + chipValue);
                chip.scale.set(0.6, 0.6);
                var chipAnimation = SB.game.add.tween(chip);
                chipAnimation.to ({x: this.holder.worldPosition.x * SB.xScale + 110, y: this.holder.worldPosition.y * SB.yScale + 85}, 500, "Linear", true, i * 10);
                chipAnimation.onComplete.add(function() {
                    this.destroy();
                }.bind(chip));
            }

            SB.SFX.winchip.play();
        },
        sabaacToPlayer: function (value) {
            var chipValue = SB.utils.getChipColor (value);
            for (var i = 0; i < 20; i++) {
                var chip = SB.game.add.sprite(1065, 430, "sprites", "chip" + chipValue);
                chip.scale.set(0.6, 0.6);
                var chipAnimation = SB.game.add.tween(chip);
                chipAnimation.to ({x: this.holder.worldPosition.x * SB.xScale + 110, y: this.holder.worldPosition.y * SB.yScale + 85}, 500, "Linear", true, i * 10);
                chipAnimation.onComplete.add(function() {
                    this.destroy();
                }.bind(chip));
            }

            SB.SFX.winchip.play();
        },
        mainToSabaac: function(value) {
            var chipValue = SB.utils.getChipColor (value);
            for (var i = 0; i < 20; i++) {
                var chip = SB.game.add.sprite(600, 420, "sprites", "chip" + chipValue);
                chip.scale.set(0.6, 0.6);
                var chipAnimation = SB.game.add.tween(chip);
                chipAnimation.to ({x: 1065, y: 430}, 500, "Linear", true, i * 10);
                chipAnimation.onComplete.add(function() {
                    this.destroy();
                }.bind(chip));
            }

            SB.SFX.movechip.play();
        },
        show: function() {
            this.holder.visible = true;
        },
        hide: function() {
            this.holder.visible = false;
        },
        checkForSpecialCards: function() {
            var cards = this.player.cards.concat(this.player.interferenceCards);
            var cardList = {};
            var idiot = 0, two = 0, three = 0, queen = 0, end = 0, master = 0, mistress = 0, evil = 0, commander = 0;
            if (cards.length > 3) {
                return 0;
            }
            for (var i = 0; i < cards.length; i++) {
                switch (cards[i].value) {
                    case "I": idiot++; break;
                    case "2": two++; break;
                    case "3": three++; break;
                    case "Q": queen++; break;
                    case "E": end++; break;
                    case "MA": master++; break;
                    case "MI": mistress++; break;
                    case "Ev": evil++; break;
                    case "C": commander++; break;
                    default : return 0;
                }
            }

            if (idiot === 1 && two === 1 && three === 1) {
                return "Idiot's Array";
            } else if (queen === 2 && cards.length === 2) {
                return "Fairy Empress";
            } else if (end === 1 && master === 1 && mistress === 1) {
                return "Longing Hearts";
            } else if (commander === 1 && evil === 1 && mistress === 1) {
                return "Temptation";
            } else {
                return 0;
            }
        }
    };

    return SB.PlayerPanel;
});


