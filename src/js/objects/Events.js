define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.Events = function() {

    };

    SB.Events.prototype = {
        CLIENT_READY: "clientReady",
        PLAYER_CONNECTED: "playerConnected",
        PLAYER_DISCONNECTED: "playerDisconnected",
        START_GAME: "startGame",
        START_ROUND: "startRound",
        TAKE_ANTE: "takeAnte",
        TAKE_BET: "takeBet",
        ROUND_DEAL: "roundDeal",
        TAKE_TURN: "takeTurn",
        TAKE_ACTION: "takeAction",
        RECIEVE_ACTION: "recieveAction",
        COLLECT_BETS: "collectBets",
        SHOW_WINNER: "showWinner",
        RESET_PLAYERS: "resetPlayers",
        GAME_WAITING: "gameWaiting",
        SABAAC_SHIFT: "sabaacShift",
        FORCE_ALDERAAN: "forceAlderaan",
        ADD_IN_CREDITS: "addInCredits",
        UPDATE_PLAYERS: "updatePlayers"
    };

    return SB.Events;
});

