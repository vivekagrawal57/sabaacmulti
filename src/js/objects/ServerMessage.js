define([
    'phaser'
], function (
    Phaser
) {
    'use strict';

    SB.ServerMessage = function() {
        this.create();
    };

    SB.ServerMessage.prototype = {
        message: null,

        create: function() {
            this.container = SB.game.add.group();

            this.sprite = SB.game.add.sprite(0 , 0, "sprites", "server-message");
            this.sprite.anchor.set(0.5, 0.5);
            this.sprite.scale.set(1.33, 1.33);
            this.container.add(this.sprite);

            this.message = SB.game.add.text(0, 0, "Waiting for enough players to join.");
            this.message.fill = "black";
            this.message.wordWrap = true;
            this.message.wordWrapWidth = 600;
            this.message.fontSize = 50;
            this.message.stroke = "#00FFFF";
            this.message.strokeThickness = 6;
            this.message.anchor.set(0.5, 0.5);
            this.message.align = "center";
            this.container.add(this.message);

            this.container.position.set(960, 480);
        },

        show: function(msg) {
            this.container.visible = true;
            this.message.text = msg;
        },

        hide: function() {
            this.container.visible = false;
        }
    };

    return SB.ServerMessage;
});




